/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_EXE

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <neurocache/nsc_plug.h>
#include <neurocache/nsc_internal.h>
#include <libgen/post.h>

#include <assert.h>
#include <errno.h>

#define INTELI_MAX_ITEMS	10
#define INTELI_BUF_WINDOW	sizeof(NSC_MSG)
#define INTELI_BUF_SIZE		(3*INTELI_BUF_WINDOW)
#define INTELI_DBG_MSG

typedef enum {
	INTELI_PARSE_ERR,
	INTELI_PARSE_INCOMPLETE,
	INTELI_PARSE_COMPLETE
} inteli_parse_t;

typedef struct st_inteli_item {
	unsigned int uid;
	NEURAL_CONNECTION *client;
	NEURAL_CONNECTION *server;
	unsigned char buf_client[INTELI_BUF_WINDOW];
	unsigned int buf_client_used;
	unsigned char buf_server[INTELI_BUF_WINDOW];
	unsigned int buf_server_used;
} inteli_item;

typedef struct st_inteli_ctx {
	NEURAL_LISTENER *list;
	NEURAL_ADDRESS *addr;
	NEURAL_SELECTOR *sel;
	inteli_item items[INTELI_MAX_ITEMS];
	unsigned int items_used;
	unsigned int flags;
	NEURAL_CONNECTION *newclient;
} inteli_ctx;

#define INTELI_FLAG_IO		(unsigned int)0x0001
#define INTELI_FLAG_MSG		(unsigned int)0x0002
#define INTELI_FLAG_MSG_DETAIL	(unsigned int)0x0004

static const char *def_listen = NULL;
static const char *def_server = NULL;
static const unsigned int def_flags = 0;

static int nsc_inteli_version(void)
{
	SYS_fprintf(SYS_stderr,
		"NeuroCache Inteli version %s (%s / %s)\n\n"
		"Distributed as part of the NeuroCache source package\n"
		"Copyright (C) 2019-2020 Hilman P. Alisabana, Jakarta.\n\n",
		HAVE_PACKAGE_VERSION, HAVE_HYSCMTERM_UUID, HAVE_HYSCMTERM_DATE);
}

static const char *usage_msg[] = {
"",
"Usage: nsc_inteli [options]     where 'options' are from;",
"  -listen <addr>   (accept incoming connections on address 'addr')",
"  -server <addr>   (proxy incoming connections to server at address 'addr')",
"  -connect <addr>  (alias for '-server')",
"  -<h|help|?>      (display this usage message)",
"  -<v|version>     (display version information)",
"", NULL};

static void do_inteli(const char *addr_server, const char *addr_listen,
			unsigned int flags, int *finished);

static int usage(void)
{
	const char **u = usage_msg;
	while(*u)
		SYS_fprintf(SYS_stderr, "%s\n", *(u++));
	return 0;
}

static const char *CMD_VERS1 = "-v";
static const char *CMD_VERS2 = "-version";
static const char *CMD_HELP1 = "-h";
static const char *CMD_HELP2 = "-help";
static const char *CMD_HELP3 = "-?";
static const char *CMD_LISTEN = "-listen";
static const char *CMD_SERVER1 = "-server";
static const char *CMD_SERVER2 = "-connect";

static int err_noarg(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, -%s requires an argument\n", arg);
	usage();
	return 1;
}
#if 0
static int err_badrange(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, -%s given an invalid argument\n", arg);
	usage();
	return 1;
}
#endif
static int err_badswitch(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, \"%s\" not recognised\n", arg);
	usage();
	return 1;
}

#define ARG_INC {argc--;argv++;}
#define ARG_CHECK(a) \
	if(argc < 2) \
		return err_noarg(a); \
	ARG_INC

int main(int argc, char *argv[])
{
	int finished = 0;
	const char *addr_listen = def_listen;
	const char *addr_server = def_server;
	unsigned int flags = def_flags;

	ARG_INC;
	while(argc > 0) {
		if((strcmp(*argv, CMD_VERS1) == 0) ||
				(strcmp(*argv, CMD_VERS2) == 0))
			return nsc_inteli_version();
		if((strcmp(*argv, CMD_HELP1) == 0) ||
				(strcmp(*argv, CMD_HELP2) == 0) ||
				(strcmp(*argv, CMD_HELP3) == 0))
			return usage();
		else if(strcmp(*argv, CMD_SERVER1) == 0) {
			ARG_CHECK(CMD_SERVER1);
			addr_server = *argv;
		} else if(strcmp(*argv, CMD_SERVER2) == 0) {
			ARG_CHECK(CMD_SERVER2);
			addr_server = *argv;
		} else if(strcmp(*argv, CMD_LISTEN) == 0) {
			ARG_CHECK(CMD_LISTEN);
			addr_listen = *argv;
		} else
			return err_badswitch(*argv);
		ARG_INC;
	}

	if(!addr_server || !addr_listen) {
		SYS_fprintf(SYS_stderr, "Error, must provide -listen and -server\n");
		return 1;
	}

	if(!SYS_sigpipe_ignore()) {
#if SYS_DEBUG_LEVEL > 0
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGPIPE\n");
#endif
		return 1;
	}

	do_inteli(addr_server, addr_listen, flags, &finished);
	if(!finished)
		return 1;
	return 0;
}

static unsigned int uid_seed = 0;

static int inteli_item_init(inteli_item *item, NEURAL_CONNECTION *accepted,
			const NEURAL_ADDRESS *addr_server)
{
	int ret = 0;
	if((item->server = NEURAL_CONNECTION_new()) == NULL) goto err;
	if(!NEURAL_CONNECTION_create(item->server, addr_server)) goto err;
	item->uid = uid_seed++;
	item->client = accepted;
	item->buf_client_used = item->buf_server_used = 0;
	ret = 1;
err:
	if(!ret) {
		if(item->server) NEURAL_CONNECTION_free(item->server);
	}
	return ret;
}

static void inteli_item_finish(inteli_item *item)
{
	NEURAL_CONNECTION_free(item->client);
	NEURAL_CONNECTION_free(item->server);
}

static int inteli_item_to_sel(inteli_item *item, NEURAL_SELECTOR *sel)
{
	if((NEURAL_BUFFER_unused(NEURAL_CONNECTION_get_send(item->server)) >=
						INTELI_BUF_WINDOW) &&
			!NEURAL_CONNECTION_add_to_selector(item->client, sel))
		return 0;
	if((NEURAL_BUFFER_unused(NEURAL_CONNECTION_get_send(item->client)) >=
						INTELI_BUF_WINDOW) &&
			!NEURAL_CONNECTION_add_to_selector(item->server, sel))
		return 0;
	return 1;
}

#define BUF_HEADER_SIZE		(4+1+4+1+1+1+2)
#define BUF_HEADER_COMPLETE(n)	((n) < BUF_HEADER_SIZE)

static const char c2s_1[] = "client->server";
static const char c2s_2[] = "server->client";
#define INTELI_C2S(n) ((n) ? c2s_1 : c2s_2)

static inteli_parse_t inteli_data_arriving(inteli_item *item, int client_to_server)
{
	NEURAL_CONNECTION *src = (client_to_server ?
			item->client : item->server);
	NEURAL_CONNECTION *dest = (client_to_server ?
			item->server : item->client);
	unsigned char *buf = (client_to_server ?
			item->buf_client : item->buf_server);
	unsigned int *buf_used = (client_to_server ?
			&item->buf_client_used : &item->buf_server_used);
	NEURAL_BUFFER *buf_in = NEURAL_CONNECTION_get_read(src);
	NEURAL_BUFFER *buf_out = NEURAL_CONNECTION_get_send(dest);
	while((NEURAL_BUFFER_unused(buf_out) >= INTELI_BUF_WINDOW) &&
			NEURAL_BUFFER_notempty(buf_in)) {
		unsigned int moved;
		unsigned long m_proto_level;
		unsigned char m_is_response;
		unsigned long m_request_uid;
		unsigned char m_op_class;
		unsigned char m_operation;
		unsigned char m_complete;
		unsigned int m_data_len;
		assert(*buf_used < INTELI_BUF_WINDOW);
		moved = NEURAL_BUFFER_read(buf_in, buf + *buf_used,
					INTELI_BUF_WINDOW - *buf_used);
		assert(moved > 0);
		*buf_used += moved;
		if(*buf_used < BUF_HEADER_SIZE)
			return INTELI_PARSE_INCOMPLETE;
		{
		const unsigned char *foop = buf;
		unsigned int foolen = BUF_HEADER_SIZE;
		moved = NEURAL_decode_uint32(&foop, &foolen, &m_proto_level); assert(moved);
		moved = NEURAL_decode_char(&foop, &foolen, &m_is_response); assert(moved);
		moved = NEURAL_decode_uint32(&foop, &foolen, &m_request_uid); assert(moved);
		moved = NEURAL_decode_char(&foop, &foolen, &m_op_class); assert(moved);
		moved = NEURAL_decode_char(&foop, &foolen, &m_operation); assert(moved);
		moved = NEURAL_decode_char(&foop, &foolen, &m_complete); assert(moved);
		moved = NEURAL_decode_uint16(&foop, &foolen, &m_data_len); assert(moved);
		assert(foolen == 0);
		}
		if(m_data_len > NSC_MSG_MAX_DATA) {
#ifdef INTELI_DBG_MSG
			SYS_fprintf(SYS_stderr, "INTELI_DBG_MSG: connection %d, %s, "
				"message has illegal 'data_len' (%d)\n",
				item->uid, INTELI_C2S(client_to_server), m_data_len);
#endif
			return INTELI_PARSE_ERR;
		}
		moved = m_data_len + BUF_HEADER_SIZE;
		if(*buf_used < moved)
			return INTELI_PARSE_INCOMPLETE;
#ifdef INTELI_DBG_MSG
		SYS_fprintf(SYS_stderr, "INTELI_DBG_MSG: connection %d, %s, "
			"message completed (request_uid = %lu), total_len=%d\n",
			item->uid, INTELI_C2S(client_to_server), m_request_uid,
			moved);
#endif

#ifndef NDEBUG
		{
		unsigned int foo = NEURAL_BUFFER_write(buf_out, buf, moved);
		assert(foo == moved);
		}
#endif
		*buf_used -= moved;
		if(*buf_used)
			SYS_memmove_n(unsigned char, buf, buf + moved, *buf_used);
		return INTELI_PARSE_COMPLETE;
	}
	return INTELI_PARSE_INCOMPLETE;
}

static int inteli_item_io(inteli_item *item)
{
	inteli_parse_t res;
	if(!NEURAL_CONNECTION_io(item->client) || !NEURAL_CONNECTION_io(item->server))
		return 0;
	do {
		res = inteli_data_arriving(item, 1);
		if(res == INTELI_PARSE_ERR) {
			SYS_fprintf(SYS_stderr, "[TODO: change me] client->server error\n");
			return 0;
		}
	} while(res == INTELI_PARSE_COMPLETE);
	do {
		res = inteli_data_arriving(item, 0);
		if(res == INTELI_PARSE_ERR) {
			SYS_fprintf(SYS_stderr, "[TODO: change me] server->client error\n");
			return 0;
		}
	} while(res == INTELI_PARSE_COMPLETE);
	return 1;
}

static int inteli_ctx_init(inteli_ctx *ctx, const char *addr_listen,
			const char *addr_server, unsigned int flags)
{
	int ret = 0;
	NEURAL_ADDRESS *a;
	ctx->list = NULL;
	ctx->addr = NULL;
	ctx->sel = NULL;
	ctx->newclient = NULL;
	ctx->items_used = 0;
	ctx->flags = flags;
	if((a = NEURAL_ADDRESS_new()) == NULL) goto err;
	if(!NEURAL_ADDRESS_create(a, addr_listen, INTELI_BUF_SIZE)) goto err;
	if(!NEURAL_ADDRESS_can_listen(a)) goto err;
	if((ctx->addr = NEURAL_ADDRESS_new()) == NULL) goto err;
	if(!NEURAL_ADDRESS_create(ctx->addr, addr_server, INTELI_BUF_SIZE)) goto err;
	if(!NEURAL_ADDRESS_can_connect(ctx->addr)) goto err;
	if((ctx->list = NEURAL_LISTENER_new()) == NULL) goto err;
	if(!NEURAL_LISTENER_create(ctx->list, a)) goto err;
	if((ctx->sel = NEURAL_SELECTOR_new()) == NULL) goto err;
	if((ctx->newclient = NEURAL_CONNECTION_new()) == NULL) goto err;

	ret = 1;
err:
	if(a) NEURAL_ADDRESS_free(a);
	if(!ret) {
		if(ctx->list) NEURAL_LISTENER_free(ctx->list);
		if(ctx->addr) NEURAL_ADDRESS_free(ctx->addr);
		if(ctx->sel) NEURAL_SELECTOR_free(ctx->sel);
		if(ctx->newclient) NEURAL_CONNECTION_free(ctx->newclient);
	}
	return ret;
}

static void inteli_ctx_finish(inteli_ctx *ctx)
{
	unsigned int loop = 0;
	inteli_item *i = ctx->items;
	NEURAL_LISTENER_free(ctx->list);
	while(loop++ < ctx->items_used)
		inteli_item_finish(i++);
}

static int inteli_ctx_to_sel(inteli_ctx *ctx)
{
	unsigned int loop = 0;
	inteli_item *i = ctx->items;
	if(!NEURAL_LISTENER_finished(ctx->list) &&
			(ctx->items_used < INTELI_MAX_ITEMS) &&
			!NEURAL_LISTENER_add_to_selector(ctx->list, ctx->sel))
		return 0;
	while(loop++ < ctx->items_used)
		if(!inteli_item_to_sel(i++, ctx->sel))
			return 0;
	return 1;
}

static int inteli_ctx_io(inteli_ctx *ctx, int *finished)
{
	unsigned int loop = 0;
	inteli_item *i = ctx->items;
	if(!NEURAL_LISTENER_finished(ctx->list) && NEURAL_CONNECTION_accept(
				ctx->newclient, ctx->list)) {
		assert(ctx->items_used < INTELI_MAX_ITEMS);
		if(!inteli_item_init(ctx->items + ctx->items_used,
				ctx->newclient, ctx->addr)) {
#ifdef INTELI_DBG_CONNS
			SYS_fprintf(SYS_stderr, "INTELI_DBG_CONNS: failed "
					"incoming connection\n");
#endif
			NEURAL_CONNECTION_free(ctx->newclient);
		} else {
#ifdef INTELI_DBG_CONNS
			SYS_fprintf(SYS_stderr, "INTELI_DBG_CONNS: connection "
				"%d accepted\n", ctx->items[ctx->items_used].uid);
#endif
			ctx->items_used++;
		}
		ctx->newclient = NEURAL_CONNECTION_new();
		if(!ctx->newclient)
			return 0;
	}
	while(loop < ctx->items_used) {
		if(!inteli_item_io(i)) {
#ifdef INTELI_DBG_CONNS
			SYS_fprintf(SYS_stderr, "INTELI_DBG_CONNS: connection "
					"%d dropped\n", i->uid);
#endif
			inteli_item_finish(i);
			if(loop + 1 < ctx->items_used)
				SYS_memmove_n(inteli_item, i, i + 1,
						ctx->items_used - (loop + 1));
			ctx->items_used--;
		} else {
			loop++;
			i++;
		}
	}
	if(!ctx->items_used && NEURAL_LISTENER_finished(ctx->list))
		*finished = 1;
	return 1;
}

static int inteli_ctx_loop(inteli_ctx *ctx, int *finished)
{
	int sel_res;
	if(!inteli_ctx_to_sel(ctx))
		return 0;
#ifdef INTELI_DBG_SELECT
	SYS_fprintf(SYS_stderr, "INTELI_DBG_SELECT: selecting ...");
	fflush(SYS_stderr);
#endif
	sel_res = NEURAL_SELECTOR_select(ctx->sel, 0, 0);
#ifdef INTELI_DBG_SELECT
	SYS_fprintf(SYS_stderr, "returned %d\n", sel_res);
#endif
	if(sel_res < 0) {
		switch(errno) {
		case EINTR:
			return 1;
		case EBADF:
			SYS_fprintf(SYS_stderr, "Error: EBADF from select()\n");
			break;
		case ENOMEM:
			SYS_fprintf(SYS_stderr, "Error: ENOMEM from select()\n");
			break;
		default:
			SYS_fprintf(SYS_stderr, "Error: unknown problem in select()\n");
			break;
		}
		return 0;
	}
	if(sel_res == 0) {
		SYS_fprintf(SYS_stderr, "Error, select() returned zero?\n");
		return 0;
	}
	return inteli_ctx_io(ctx, finished);
}

static void do_inteli(const char *addr_server, const char *addr_listen,
			unsigned int flags, int *finished)
{
	inteli_ctx ctx;
	if(inteli_ctx_init(&ctx, addr_listen, addr_server, flags)) {
		while(inteli_ctx_loop(&ctx, finished) && !(*finished))
			;
		inteli_ctx_finish(&ctx);
	}
}

