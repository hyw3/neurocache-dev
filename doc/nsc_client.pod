=head1 NAME

nsc_client - Neuro-based session caching client proxy


=head1 SYNOPSIS

B<nsc_client> -server <address> [options]


=head1 DESCRIPTION

B<nsc_client> runs a client proxy to provide access to a remote cache server
(typically over TCP/IPv4) by providing a local service (typically over unix
domain sockets). It starts listening on a configurable network address for
connections and establishes a persistent connection to an instance of
B<nsc_server> for proxying cache operations to. Incoming connections are
expected to communicate using the L<neurocache(8)> protocol, and would typically
be applications using one of the neurocache APIs in I<libneurocache> to
encapsulate these communications.

=head1 OPTIONS

=over 4

=item B<-daemon>

After initialising, B<nsc_client> will detach from the parent process, close
standard file-descriptors, etc. If this flag is not set, B<nsc_client> will run in
the foreground.

=item B<-user> user

This switch will attempt to change user privileges of B<nsc_client> to the given
user ID after initialising its listening socket.

=item B<-listen> address

This flag configures the address on which B<nsc_client> should listen for incoming
connections.  The syntax is that defined by the I<libneural> API, e.g.

    # Listen on a unix domain socket in the /tmp directory
    nsc_client -listen UNIX:/tmp/cacheclient

The default value is: UNIX:/tmp/scache

=item B<-sockowner> user

It will attempt to change ownership of the created socket file when listening
(see B<-listen>) on unix domain sockets.

=item B<-sockgroup> group

It will attempt to change group ownership of the created socket file when listening
(see B<-listen>) on unix domain sockets.

=item B<-sockperms> perms

It will attempt to change file permissions on unix domain sockets for the created
socket file, and is specified in the standard octal notation used for unix file
permissions. To start nsc_client to run as the I<nobody> user, listening on a unix
domain socket that can only be connected to by the I<root> user or members of the
I<ssl> group;

    # nsc_client -listen UNIX:/tmp/cacheclient -user nobody \
          -sockgroup ssl -sockperms 440

=item B<-server> address

=item B<-connect> address

These flags are identical, and specify the address of the cache server
B<nsc_client> should connect to.

    # Connect to a remote cache server listening on port 9001
    nsc_client -listen UNIX:/tmp/cacheclient \
               -server IP:cacheserver.localnet:9001

=item B<-retry> msecs

This flag allows the retry period to be configured to any number of milliseconds.

=item B<-idle> msecs

This flag specifies the period of idle time after which client connections will
be dropped, and is in units of milliseconds and B<not> seconds. The default is
zero, and this means that client connections are never intentionally dropped.

=item B<-pidfile> path

When B<-pidfile> is specified B<nsc_client> will write its process ID to a file at the
specified path upon successful initialisation.

    kill `cat pidfile.pid`

=item B<-h>, B<-help>, B<-?>

These flags will cause B<nsc_client> to display a brief usage summary to the console
and exit cleanly. Any other flags are ignored.

=back


=head1 SEE ALSO

=over 4

=item L<nsc_server(1)>

Neuro-based cache server.

=item L<nsc_inteli(1)>

NeuroCache protocol analyser and debugging tool.

=item L<neurocache(8)>

Overview of the neurocache architecture.

=item F<http://clhy.hyang.org/archives/neurocache//>

NeuroCache home page.

=back


=head1 AUTHOR

This program was designed and implemented by Hilman P. Alisabana for
cLHy API (cAPI) capi_socache_neuro of cLHy server.

Home Page: F<http://clhy.hyang.org/archives/neurocache/>

