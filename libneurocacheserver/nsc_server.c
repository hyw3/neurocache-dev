/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <neurocache/nsc_server.h>
#include <neurocache/nsc_plug.h>
#include <neurocache/nsc_internal.h>
#include <libgen/post.h>

#include <assert.h>

#define NSC_SERVER_START_SIZE		256
#define NSC_CLIENT_STORE_START_SIZE	NSC_MSG_MAX_DATA
#define NSC_CLIENT_RESPONSE_START_SIZE	NSC_MSG_MAX_DATA

static const NSC_CACHE_cb *default_cache_implementation = NULL;

struct st_NSC_SERVER {
	const NSC_CACHE_cb *vt;
	NSC_CLIENT **clients;
	unsigned int clients_used, clients_size;
	NSC_CACHE *cache;
	unsigned long ops;
};

struct st_NSC_CLIENT {
	NSC_SERVER *server;
	NSC_PLUG *plug;
	unsigned int flags;
	unsigned char read_data[NSC_MAX_TOTAL_DATA];
	unsigned int read_data_len;
	unsigned char send_data[NSC_MAX_TOTAL_DATA];
	unsigned int send_data_len;
};

static void int_server_del_client(NSC_SERVER *ctx, unsigned int idx)
{
	NSC_CLIENT *clnt = ctx->clients[idx];
	NSC_PLUG_free(clnt->plug);
	SYS_free(NSC_CLIENT, clnt);
	if(idx + 1 < ctx->clients_used)
		SYS_memmove_n(NSC_CLIENT *, ctx->clients + idx,
			(const NSC_CLIENT **)ctx->clients + (idx + 1),
			ctx->clients_used - (idx + 1));
	ctx->clients_used--;
}

static void int_response_1byte(NSC_CLIENT *clnt, unsigned char val)
{
	clnt->send_data[0] = val;
	clnt->send_data_len = 1;
}

static int int_do_op_add(NSC_CLIENT *clnt, const struct timeval *now)
{
	int res;
	unsigned long msecs, id_len, data_len;
	unsigned char *p = clnt->read_data;
	unsigned int p_len = clnt->read_data_len;

	if(!NEURAL_decode_uint32((const unsigned char **)&p, &p_len, &msecs) ||
			!NEURAL_decode_uint32((const unsigned char **)&p,
				&p_len, &id_len))
		return 0;
	assert((p_len + 8) == clnt->read_data_len);
	assert(p == (clnt->read_data + 8));
	if(msecs > NSC_MAX_EXPIRY) {
		int_response_1byte(clnt, NSC_ADD_ERR_TIMEOUT_RANGE);
		return 1;
	}
	if(id_len >= p_len) {
		int_response_1byte(clnt, NSC_ADD_ERR_CORRUPT);
		return 1;
	}
	if(!id_len || (id_len > NSC_MAX_ID_LEN)) {
		int_response_1byte(clnt, NSC_ADD_ERR_ID_RANGE);
		return 1;
	}
	data_len = p_len - id_len;
	if(!data_len || (data_len > NSC_MAX_DATA_LEN)) {
		int_response_1byte(clnt, NSC_ADD_ERR_DATA_RANGE);
		return 1;
	}
	res = clnt->server->vt->cache_add(clnt->server->cache, now, msecs,
			p, id_len, p + id_len, data_len);
	if(res)
		int_response_1byte(clnt, NSC_ERR_OK);
	else
		int_response_1byte(clnt, NSC_ADD_ERR_MATCHING_SESSION);
	return 1;
}

static int int_do_op_get(NSC_CLIENT *clnt, const struct timeval *now)
{
	unsigned int len;
	len = clnt->server->vt->cache_get(clnt->server->cache, now,
			clnt->read_data, clnt->read_data_len, NULL, 0);
	if(!len) {
		int_response_1byte(clnt, NSC_ERR_NOTOK);
		return 1;
	}
	if(len > NSC_MAX_TOTAL_DATA)
		return 0;
	len = clnt->server->vt->cache_get(clnt->server->cache, now,
				clnt->read_data, clnt->read_data_len,
				clnt->send_data, NSC_MAX_TOTAL_DATA);
	assert(len && (len <= NSC_MAX_TOTAL_DATA));
	if(!len)
		return 0;
	clnt->send_data_len = len;
	return 1;
}

static int int_do_op_remove(NSC_CLIENT *clnt, const struct timeval *now)
{
	if(clnt->server->vt->cache_remove(clnt->server->cache, now,
				clnt->read_data, clnt->read_data_len))
		int_response_1byte(clnt, NSC_ERR_OK);
	else
		int_response_1byte(clnt, NSC_ERR_NOTOK);
	return 1;

}

static int int_do_op_have(NSC_CLIENT *clnt, const struct timeval *now)
{
	if(clnt->server->vt->cache_have(clnt->server->cache, now,
				clnt->read_data, clnt->read_data_len))
		int_response_1byte(clnt, NSC_ERR_OK);
	else
		int_response_1byte(clnt, NSC_ERR_NOTOK);
	return 1;

}

static int int_do_operation(NSC_CLIENT *clnt, const struct timeval *now)
{
	int toret = 1, plug_read = 1, plug_write = 0;
	unsigned long request_uid;
	NSC_CMD cmd;
	const unsigned char *payload_data;
	unsigned int payload_len;

	if(!NSC_PLUG_read(clnt->plug, 1, &request_uid, &cmd,
				&payload_data, &payload_len))
		goto err;
	plug_read = 1;
	if(!NSC_PLUG_write(clnt->plug, 0, request_uid, cmd, NULL, 0))
		goto err;
	plug_write = 1;
	assert(payload_len <= NSC_MAX_TOTAL_DATA);
	if(payload_len)
		SYS_memcpy_n(unsigned char, clnt->read_data,
				payload_data, payload_len);
	clnt->read_data_len = payload_len;
	switch(cmd) {
	case NSC_CMD_ADD:
		toret = int_do_op_add(clnt, now);
		break;
	case NSC_CMD_GET:
		toret = int_do_op_get(clnt, now);
		break;
	case NSC_CMD_REMOVE:
		toret = int_do_op_remove(clnt, now);
		break;
	case NSC_CMD_HAVE:
		toret = int_do_op_have(clnt, now);
		break;
	default:
		goto err;
	}
	if(!toret)
		goto err;
	if(!NSC_PLUG_write_more(clnt->plug, clnt->send_data,
				clnt->send_data_len) ||
			!NSC_PLUG_commit(clnt->plug))
		goto err;
	plug_write = 0;
	if(!NSC_PLUG_consume(clnt->plug))
		goto err;
	clnt->server->ops++;
	return toret;
err:
	if(plug_read)
		NSC_PLUG_consume(clnt->plug);
	if(plug_write)
		NSC_PLUG_rollback(clnt->plug);
	return 0;
}

int NSC_SERVER_set_cache(const NSC_CACHE_cb *impl)
{
	if(!impl || !impl->cache_new || !impl->cache_free ||
			!impl->cache_add || !impl->cache_get ||
			!impl->cache_remove || !impl->cache_have ||
			!impl->cache_num_items)
		return 0;
	default_cache_implementation = impl;
	return 1;
}

NSC_SERVER *NSC_SERVER_new(unsigned int max_sessions)
{
	NSC_SERVER *toret;
	if(!default_cache_implementation)
		return NULL;
	toret = SYS_malloc(NSC_SERVER, 1);
	if(!toret)
		return NULL;
	toret->clients = SYS_malloc(NSC_CLIENT *,
				NSC_SERVER_START_SIZE);
	if(!toret->clients) {
		SYS_free(NSC_SERVER, toret);
		return NULL;
	}
	toret->vt = default_cache_implementation;
	toret->cache = toret->vt->cache_new(max_sessions);
	if(!toret->cache) {
		SYS_free(NSC_CLIENT *, toret->clients);
		SYS_free(NSC_SERVER, toret);
		return NULL;
	}
	toret->clients_used = 0;
	toret->clients_size = NSC_SERVER_START_SIZE;
	toret->ops = 0;
	return toret;
}

void NSC_SERVER_free(NSC_SERVER *ctx)
{
	NSC_CLIENT *client;
	unsigned int idx = ctx->clients_used;
	ctx->vt->cache_free(ctx->cache);
	while(idx-- > 0) {
		client = ctx->clients[idx];
		if(client->flags & NSC_CLIENT_FLAG_IN_SERVER)
			int_server_del_client(ctx, idx);
	};
	assert(ctx->clients_used == 0);
	SYS_free(NSC_CLIENT *, ctx->clients);
	SYS_free(NSC_SERVER, ctx);
}

unsigned int NSC_SERVER_items_stored(NSC_SERVER *ctx,
			const struct timeval *now)
{
	return ctx->vt->cache_num_items(ctx->cache, now);
}

void NSC_SERVER_reset_operations(NSC_SERVER *ctx)
{
	ctx->ops = 0;
}

unsigned long NSC_SERVER_num_operations(NSC_SERVER *ctx)
{
	return ctx->ops;
}

NSC_CLIENT *NSC_SERVER_new_client(NSC_SERVER *ctx,
			NEURAL_CONNECTION *conn, unsigned int flags)
{
	NSC_CLIENT *c;
	NSC_PLUG *plug;
	unsigned int plug_flags = 0;
	if(ctx->clients_used == ctx->clients_size) {
		NSC_CLIENT **newitems;
		unsigned int newsize = ctx->clients_size * 3 / 2;
		newitems = SYS_malloc(NSC_CLIENT *, newsize);
		if(!newitems)
			return NULL;
		SYS_memcpy_n(NSC_CLIENT *, newitems,
				(const NSC_CLIENT **)ctx->clients,
				ctx->clients_used);
		SYS_free(NSC_CLIENT *, ctx->clients);
		ctx->clients = newitems;
		ctx->clients_size = newsize;
	}
	if(flags & NSC_CLIENT_FLAG_NOFREE_CONN)
		plug_flags |= NSC_PLUG_FLAG_NOFREE_CONN;
	if((plug = NSC_PLUG_new(conn, plug_flags)) == NULL)
		return NULL;
	c = SYS_malloc(NSC_CLIENT, 1);
	if(!c) {
		NSC_PLUG_free(plug);
		return NULL;
	}
	c->server = ctx;
	c->plug = plug;
	c->flags = flags;
	c->read_data_len = c->send_data_len = 0;
	ctx->clients[ctx->clients_used++] = c;
	return c;
}

int NSC_SERVER_del_client(NSC_CLIENT *clnt)
{
	NSC_SERVER *ctx = clnt->server;
	unsigned int idx = 0;
	while((idx < ctx->clients_used) && (ctx->clients[idx] != clnt))
		idx++;
	if(idx >= ctx->clients_used)
		return 0;
	int_server_del_client(ctx, idx);
	return 1;
}

int NSC_SERVER_process_client(NSC_CLIENT *clnt,
			const struct timeval *now)
{
	unsigned long request_uid;
	NSC_CMD cmd;
	const unsigned char *payload_data;
	unsigned int payload_len;
	if(!NSC_PLUG_read(clnt->plug, 0, &request_uid, &cmd,
				&payload_data, &payload_len))
		return 1;
	return int_do_operation(clnt, now);
}

int NSC_SERVER_clients_io(NSC_SERVER *ctx, const struct timeval *now)
{
	unsigned int idx = 0;
	NSC_CLIENT *client;
	while(idx < ctx->clients_used) {
		client = ctx->clients[idx];
		if((client->flags & NSC_CLIENT_FLAG_IN_SERVER) &&
				(!NSC_PLUG_io(client->plug) ||
				!NSC_SERVER_process_client(client, now)))
			int_server_del_client(ctx, idx);
		else
			idx++;
	}
	return 1;
}

int NSC_SERVER_clients_empty(const NSC_SERVER *ctx)
{
	return !ctx->clients_used;
}
