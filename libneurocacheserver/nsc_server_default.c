/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <neurocache/nsc_server.h>
#include <neurocache/nsc_plug.h>
#include <neurocache/nsc_internal.h>
#include <libgen/post.h>

#include <assert.h>

typedef struct st_NSC_ITEM {
	struct timeval expiry;
	unsigned int id_len, data_len;
	unsigned char *ptr;
} NSC_ITEM;

struct st_NSC_CACHE {
	NSC_ITEM *items;
	unsigned int items_used, items_size;
	unsigned int expire_delta;
	unsigned char cached_id[NSC_MAX_ID_LEN];
	unsigned int cached_id_len;
	int cached_idx;
};

#ifdef NSC_CACHE_DEBUG
#ifndef NSC_CACHE_DEBUG_INTERVAL
#error "Error, must define NSC_CACHE_DEBUG_INTERVAL when NSC_CACHE_DEBUG is defined"
#endif
static unsigned int cached_removes = 0;
static unsigned int cached_misses = 0;
static unsigned int cached_hits = 0;

#define CACHED_REMOVE {if((++cached_removes % NSC_CACHE_DEBUG_INTERVAL) == 0) \
		SYS_fprintf(SYS_stderr, "NSC_CACHE_DEBUG: cached_removes = %u\n", \
				cached_removes); }
#define CACHED_MISS {if((++cached_misses % NSC_CACHE_DEBUG_INTERVAL) == 0) \
		SYS_fprintf(SYS_stderr, "NSC_CACHE_DEBUG: cached_misses = %u\n", \
				cached_misses); }
#define CACHED_HIT {if((++cached_hits % NSC_CACHE_DEBUG_INTERVAL) == 0) \
		SYS_fprintf(SYS_stderr, "NSC_CACHE_DEBUG: cached_hits = %u\n", \
				cached_hits); }
#else
#define CACHED_REMOVE
#define CACHED_MISS
#define CACHED_HIT
#endif

static void int_lookup_expired(NSC_CACHE *cache, unsigned int num)
{
	cache->cached_idx -= num;
	if(cache->cached_idx < 0) {
		cache->cached_idx = -1;
		CACHED_REMOVE
	}
}

static void int_lookup_removed(NSC_CACHE *cache, unsigned int idx)
{
	assert(idx <= cache->items_used);
	if(cache->cached_idx == (int)idx) {
		cache->cached_idx = -1;
		CACHED_REMOVE
	} else if(cache->cached_idx > (int)idx)
		cache->cached_idx--;
}

static int int_lookup_check(NSC_CACHE *cache,
			const unsigned char *session_id,
			unsigned int session_id_len,
			unsigned int *idx)
{
	if((cache->cached_idx < 0) ||
			(session_id_len != cache->cached_id_len) ||
			(memcmp(session_id, cache->cached_id,
				session_id_len) != 0)) {
		CACHED_MISS
		return 0;
	}
	*idx = (unsigned int)cache->cached_idx;
	CACHED_HIT
	return 1;
}

static void int_lookup_set(NSC_CACHE *cache,
			const unsigned char *session_id,
			unsigned int session_id_len,
			int idx)
{
	cache->cached_id_len = session_id_len;
	if(session_id_len)
		SYS_memcpy_n(unsigned char, cache->cached_id,
				session_id, session_id_len);
	cache->cached_idx = idx;
}

static void int_pre_remove_NSC_ITEM(NSC_ITEM *item)
{
	SYS_free(unsigned char, item->ptr);
	item->ptr = NULL;
}

static void int_force_expire(NSC_CACHE *cache, unsigned int num)
{
	assert((num > 0) && (num <= cache->items_used));
	if(num < cache->items_used)
		SYS_memmove_n(NSC_ITEM, cache->items, cache->items + num,
				cache->items_used - num);
	cache->items_used -= num;
	int_lookup_expired(cache, num);
}

static void int_expire(NSC_CACHE *cache, const struct timeval *now)
{
	unsigned int idx = 0, toexpire = 0;
	NSC_ITEM *item = cache->items;
	while((idx < cache->items_used) && (SYS_timecmp(now,
				&(item->expiry)) > 0)) {
		int_pre_remove_NSC_ITEM(item);
		idx++;
		item++;
		toexpire++;
	}
	if(toexpire)
		int_force_expire(cache, toexpire);
}

static int int_find_NSC_ITEM(NSC_CACHE *cache, const unsigned char *ptr,
				unsigned int len, const struct timeval *now)
{
	unsigned int idx;
	NSC_ITEM *item = cache->items;
	int_expire(cache, now);
	if(int_lookup_check(cache, ptr, len, &idx))
		return idx;
	idx = 0;
	while(idx < cache->items_used) {
		if((item->id_len == len) && (memcmp(item->ptr, ptr, len) == 0)) {
			int_lookup_set(cache, ptr, len, idx);
			return (int)idx;
		}
		idx++;
		item++;
	}
	return -1;
}

static int int_add_NSC_ITEM(NSC_CACHE *cache, unsigned int idx,
		const struct timeval *expiry,
		const unsigned char *session_id, unsigned int session_id_len,
		const unsigned char *data, unsigned int data_len)
{
	unsigned char *ptr;
	NSC_ITEM *item;

	ptr = SYS_malloc(unsigned char, session_id_len + data_len);
	if(!ptr)
		return 0;
	item = cache->items + idx;
	if(idx < cache->items_used)
		SYS_memmove_n(NSC_ITEM, item + 1, item,
				cache->items_used - idx);
	SYS_timecpy(&item->expiry, expiry);
	item->ptr = ptr;
	item->id_len = session_id_len;
	item->data_len = data_len;
	SYS_memcpy_n(unsigned char, item->ptr, session_id, session_id_len);
	SYS_memcpy_n(unsigned char, item->ptr + item->id_len, data, data_len);
	cache->items_used++;
	int_lookup_set(cache, session_id, session_id_len, idx);
	return 1;
}

static void int_remove_NSC_ITEM(NSC_CACHE *cache, unsigned int idx)
{
	NSC_ITEM *item;
	assert(idx < cache->items_used);
	item = cache->items + idx;
	int_pre_remove_NSC_ITEM(item);
	cache->items_used--;
	if(idx < cache->items_used)
		SYS_memmove_n(NSC_ITEM, cache->items + idx,
				cache->items + (idx + 1),
				cache->items_used - idx);
	int_lookup_removed(cache, idx);
}

static NSC_CACHE *cache_new(unsigned int max_sessions)
{
	NSC_CACHE *toret;
	if((max_sessions < NSC_CACHE_MIN_SIZE) ||
			(max_sessions > NSC_CACHE_MAX_SIZE))
		return NULL;
	toret = SYS_malloc(NSC_CACHE, 1);
	if(!toret)
		return NULL;
	toret->items = SYS_malloc(NSC_ITEM, max_sessions);
	if(!toret->items) {
		SYS_free(NSC_CACHE, toret);
		return NULL;
	}
	toret->items_used = 0;
	toret->items_size = max_sessions;
	toret->expire_delta = max_sessions / 30;
	if(!toret->expire_delta)
		toret->expire_delta = 1;
	int_lookup_set(toret, NULL, 0, -1);
	return toret;
}

static void cache_free(NSC_CACHE *cache)
{
	while(cache->items_used)
		int_remove_NSC_ITEM(cache, cache->items_used - 1);
	SYS_free(NSC_ITEM, cache->items);
	SYS_free(NSC_CACHE, cache);
}

static int cache_add_session(NSC_CACHE *cache,
			const struct timeval *now,
			unsigned long timeout_msecs,
			const unsigned char *session_id,
			unsigned int session_id_len,
			const unsigned char *data,
			unsigned int data_len)
{
	NSC_ITEM *item;
	int idx;
	struct timeval expiry;

	assert(session_id_len && data_len &&
			(session_id_len <= NSC_MAX_ID_LEN) &&
			(data_len <= NSC_MAX_DATA_LEN));
	idx = int_find_NSC_ITEM(cache, session_id, session_id_len, now);
	if(idx >= 0)
		return 0;
	if(cache->items_used == cache->items_size)
		int_force_expire(cache, cache->expire_delta);
	SYS_timeadd(&expiry, now, timeout_msecs);
	idx = cache->items_used;
	item = cache->items + idx;
	while(idx > 0)  {
		idx--;
		item--;
		if(SYS_timecmp(&item->expiry, &expiry) <= 0) {
			idx++;
			item++;
			goto found;
		}
	}
found:
	return int_add_NSC_ITEM(cache, idx, &expiry, session_id,
					session_id_len, data, data_len);
}

static unsigned int cache_get_session(NSC_CACHE *cache,
			const struct timeval *now,
			const unsigned char *session_id,
			unsigned int session_id_len,
			unsigned char *store,
			unsigned int store_len)
{
	NSC_ITEM *item;
	int idx = int_find_NSC_ITEM(cache,
			session_id, session_id_len, now);
	if(idx < 0)
		return 0;
	item = cache->items + idx;
	if(store) {
		unsigned int towrite = item->data_len;
		assert(store_len > 0);
		if(towrite > store_len)
			towrite = store_len;
		SYS_memcpy_n(unsigned char, store,
			item->ptr + item->id_len, towrite);
	}
	return item->data_len;
}

static int cache_remove_session(NSC_CACHE *cache,
			const struct timeval *now,
			const unsigned char *session_id,
			unsigned int session_id_len)
{
	int idx = int_find_NSC_ITEM(cache, session_id, session_id_len, now);
	if(idx < 0)
		return 0;
	int_remove_NSC_ITEM(cache, idx);
	return 1;
}

static int cache_have_session(NSC_CACHE *cache,
			const struct timeval *now,
			const unsigned char *session_id,
			unsigned int session_id_len)
{
	return (int_find_NSC_ITEM(cache, session_id,
				session_id_len, now) < 0 ? 0 : 1);
}

static unsigned int cache_items_stored(NSC_CACHE *cache,
			const struct timeval *now)
{
	int_expire(cache, now);
	return cache->items_used;
}

static const NSC_CACHE_cb our_implementation = {
	cache_new,
	cache_free,
	cache_add_session,
	cache_get_session,
	cache_remove_session,
	cache_have_session,
	cache_items_stored
};

int NSC_SERVER_set_default_cache(void)
{
	return NSC_SERVER_set_cache(&our_implementation);
}
