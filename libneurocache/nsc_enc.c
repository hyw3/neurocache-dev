/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <neurocache/nsc_plug.h>
#include <neurocache/nsc_internal.h>
#include <libgen/post.h>

#include <assert.h>

static int proto_level_test(unsigned long pl)
{
	if((NEUROCACHE_GET_PROTO_VER(pl) != NEUROCACHE_PROTO_VER)
#if 0
			|| (NEUROCACHE_GET_PATCH_LEVEL(pl) < 0x0003)
			|| (NEUROCACHE_GET_PATCH_LEVEL(pl) == 0x00a3)
#endif
           ) {
#ifndef NEUROCACHE_NO_PROTOCOL_STDERR
		SYS_fprintf(SYS_stderr, "libneurocache(pid=%u) protocol "
			"incompatibility; my level is %08x, the peer's is %08x\n",
			(unsigned int)SYS_getpid(), NEUROCACHE_PROTO_LEVEL,
			(unsigned int)pl);
#endif
		abort();
	}
	return 1;
}

static int NSC_MSG_set_cmd(NSC_MSG *msg, NSC_CMD cmd)
{
	switch(cmd) {
	case NSC_CMD_ADD:
		msg->op_class = NSC_CLASS_USER;
		msg->operation = NSC_OP_ADD;
		return 1;
	case NSC_CMD_GET:
		msg->op_class = NSC_CLASS_USER;
		msg->operation = NSC_OP_GET;
		return 1;
	case NSC_CMD_REMOVE:
		msg->op_class = NSC_CLASS_USER;
		msg->operation = NSC_OP_REMOVE;
		return 1;
	case NSC_CMD_HAVE:
		msg->op_class = NSC_CLASS_USER;
		msg->operation = NSC_OP_HAVE;
		return 1;
	default:
		break;
	}
	return 0;
}

static NSC_CMD int_get_cmd(unsigned char op_class, unsigned char operation)
{
	switch(op_class) {
	case NSC_CLASS_USER:
		switch(operation) {
		case NSC_OP_ADD:
			return NSC_CMD_ADD;
		case NSC_OP_GET:
			return NSC_CMD_GET;
		case NSC_OP_REMOVE:
			return NSC_CMD_REMOVE;
		case NSC_OP_HAVE:
			return NSC_CMD_HAVE;
		default:
			goto err;
		}
	default:
		break;
	}
err:
	return NSC_CMD_ERROR;
}

static NSC_CMD NSC_MSG_get_cmd(const NSC_MSG *msg)
{
	return int_get_cmd(msg->op_class, msg->operation);
}

static unsigned int NSC_MSG_encoding_size(const NSC_MSG *msg)
{
	assert(msg->data_len <= NSC_MSG_MAX_DATA);
	return (14 + msg->data_len);
}

static NSC_DECODE_STATE NSC_MSG_pre_decode(const unsigned char *data,
					unsigned int data_len)
{
	unsigned char op_class, complete;
	unsigned short payload_len;
	unsigned long ver;
	if(data_len-- < 5)
		return NSC_DECODE_STATE_INCOMPLETE;
	{
		const unsigned char *data_1 = data;
		unsigned int len_1 = 4;
		if(!NEURAL_decode_uint32(&data_1, &len_1, &ver))
			return NSC_DECODE_STATE_CORRUPT;
		if(!proto_level_test(ver))
			return NSC_DECODE_STATE_CORRUPT;
	}
	data += 4;
	if(*(data++) > 1)
		return NSC_DECODE_STATE_CORRUPT;
	if(data_len < 5)
		return NSC_DECODE_STATE_INCOMPLETE;
	data_len -= 5;
	data += 4;
	op_class = *(data++);
	if(op_class > NSC_CLASS_LAST)
		return NSC_DECODE_STATE_CORRUPT;
	if(data_len-- < 1)
		return NSC_DECODE_STATE_INCOMPLETE;
	if(int_get_cmd(op_class, *(data++)) == NSC_CMD_ERROR)
		return NSC_DECODE_STATE_CORRUPT;
	if(data_len-- < 1)
		return NSC_DECODE_STATE_INCOMPLETE;
	complete = *(data++);
	if(complete > 1)
		return NSC_DECODE_STATE_CORRUPT;
	if(data_len < 2)
		return NSC_DECODE_STATE_INCOMPLETE;
	payload_len = ntohs(*((const unsigned short *)data));
	if(payload_len > NSC_MSG_MAX_DATA)
		return NSC_DECODE_STATE_CORRUPT;
	if(!complete && (payload_len < NSC_MSG_MAX_DATA))
		return NSC_DECODE_STATE_CORRUPT;
	if(data_len - 2 < payload_len)
		return NSC_DECODE_STATE_INCOMPLETE;
	return NSC_DECODE_STATE_OK;
}

#ifdef NSC_MSG_DEBUG
static const char *str_dump_class[] = { "NSC_CLASS_USER", NULL };
static const char *str_dump_op[] = { "NSC_OP_ADD", "NSC_OP_GET",
				"NSC_OP_REMOVE", "NSC_OP_HAVE", NULL };
static const char *dump_int_to_str(int val, const char **strs)
{
	while(val && *strs) {
		val--;
		strs++;
	}
	if(*strs)
		return *strs;
	return "<unrecognised value>";
}
#define debug_bytes_per_line 20
static void debug_dump_bin(FILE *f, const char *prefix,
		const unsigned char *data, unsigned int len)
{
	SYS_fprintf(f, "len=%u\n", len);
	while(len) {
		unsigned int to_print = ((len < debug_bytes_per_line) ?
				len : debug_bytes_per_line);
		len -= to_print;
		SYS_fprintf(f, "%s", prefix);
		while(to_print--)
			SYS_fprintf(f, "%02x ", *(data++));
		SYS_fprintf(f, "\n");
	}
}

static void dump_msg(const NSC_MSG *msg)
{
	SYS_fprintf(SYS_stderr, "NSC_MSG_DEBUG: dumping message...\n");
	SYS_fprintf(SYS_stderr, "   proto_level:  %08x\n",
		msg->proto_level);
	if(msg->proto_level != 0x00100000)
		abort();
	SYS_fprintf(SYS_stderr, "   is_response:  %u (%s)\n",
		msg->is_response, (msg->is_response ? "response" : "request"));
	SYS_fprintf(SYS_stderr, "   request_uid:  %u\n", msg->request_uid);
	SYS_fprintf(SYS_stderr, "   op_class:     %s\n",
		dump_int_to_str(msg->op_class, str_dump_class));
	SYS_fprintf(SYS_stderr, "   operation:    %s\n",
		dump_int_to_str(msg->operation, str_dump_op));
	SYS_fprintf(SYS_stderr, "   complete:     %u (%s)\n",
		msg->complete, (msg->complete ? "complete" : "incomplete"));
	SYS_fprintf(SYS_stderr, "   data_len:     %u\n", msg->data_len);
	SYS_fprintf(SYS_stderr, "   data:\n");
	debug_dump_bin(SYS_stderr, "       ", msg->data, msg->data_len);
}
#endif

static unsigned int NSC_MSG_encode(const NSC_MSG *msg, unsigned char *ptr,
				unsigned int data_len)
{
	unsigned int len = data_len;
#if 0
	msg->proto_level = NEUROCACHE_PROTO_LEVEL;
#endif
	if(!NEURAL_encode_uint32(&ptr, &len, msg->proto_level) ||
			!NEURAL_encode_char(&ptr, &len, msg->is_response) ||
			!NEURAL_encode_uint32(&ptr, &len, msg->request_uid) ||
			!NEURAL_encode_char(&ptr, &len, msg->op_class) ||
			!NEURAL_encode_char(&ptr, &len, msg->operation) ||
			!NEURAL_encode_char(&ptr, &len, msg->complete) ||
			!NEURAL_encode_uint16(&ptr, &len, msg->data_len) ||
			!NEURAL_encode_bin(&ptr, &len, msg->data,
				msg->data_len))
		return 0;
	assert(data_len >= len);
#ifdef NSC_MSG_DEBUG
	dump_msg(msg);
#endif
	return data_len - len;
}

static unsigned int NSC_MSG_decode(NSC_MSG *msg, const unsigned char *data,
				unsigned int data_len)
{
	unsigned char op_class, operation;
	unsigned int len = data_len;
	if(!NEURAL_decode_uint32(&data, &len, &msg->proto_level) ||
			!NEURAL_decode_char(&data, &len, &msg->is_response) ||
			!NEURAL_decode_uint32(&data, &len, &msg->request_uid) ||
			!NEURAL_decode_char(&data, &len, &op_class) ||
			!NEURAL_decode_char(&data, &len, &operation) ||
			!NEURAL_decode_char(&data, &len, &msg->complete) ||
			!NEURAL_decode_uint16(&data, &len, &msg->data_len) ||
			!NEURAL_decode_bin(&data, &len, msg->data,
				msg->data_len))
		return 0;
	msg->op_class = op_class;
	msg->operation = operation;
	assert(data_len >= len);
#ifdef NSC_MSG_DEBUG
	dump_msg(msg);
#endif
	assert((msg->complete == 1) || (msg->data_len >= NSC_MSG_MAX_DATA));
	return data_len - len;
}

typedef enum {
	PLUG_EMPTY,
	PLUG_IO,
	PLUG_USER,
	PLUG_FULL
} NSC_PLUG_STATE;

typedef struct st_NSC_PLUG_IO {
	NSC_PLUG_STATE state;
	NSC_MSG msg;
	unsigned long request_uid;
	NSC_CMD cmd;
	unsigned char *data;
	unsigned int data_used, data_size;
} NSC_PLUG_IO;

struct st_NSC_PLUG {
	NEURAL_CONNECTION *conn;
	unsigned int flags;
	NSC_PLUG_IO read;
	NSC_PLUG_IO write;
};


#define NSC_IO_START_SIZE	NSC_MSG_MAX_DATA

static int NSC_PLUG_IO_init(NSC_PLUG_IO *io)
{
	io->state = PLUG_EMPTY;
	io->data = SYS_malloc(unsigned char, NSC_IO_START_SIZE);
	if(!io->data)
		return 0;
	io->data_used = 0;
	io->data_size = NSC_IO_START_SIZE;
	return 1;
}

static void NSC_PLUG_IO_finish(NSC_PLUG_IO *io)
{
	SYS_free(unsigned char, io->data);
}

static int NSC_PLUG_IO_make_space(NSC_PLUG_IO *io, unsigned int needed)
{
	unsigned char *newdata;
	unsigned int newsize = io->data_size;

	if(io->data_used + needed <= io->data_size)
		return 1;
	do {
		newsize = newsize * 3 /  2;
	} while(io->data_used + needed > newsize);
	newdata = SYS_malloc(unsigned char, newsize);
	if(!newdata)
		return 0;
	if(io->data_used)
		SYS_memcpy_n(unsigned char, newdata, io->data, io->data_used);
	SYS_free(unsigned char, io->data);
	io->data = newdata;
	io->data_size = newsize;
	return 1;
}

static int NSC_PLUG_IO_read_flush(NSC_PLUG_IO *io, int to_server,
				NEURAL_BUFFER *buffer)
{
	const unsigned char *buf_ptr;
	unsigned int buf_len, tmp;
	NSC_CMD cmd;

start_over:
	switch(io->state) {
	case PLUG_FULL:
	case PLUG_USER:
		return 1;
	case PLUG_EMPTY:
	case PLUG_IO:
		break;
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	buf_ptr = NEURAL_BUFFER_data(buffer);
	buf_len = NEURAL_BUFFER_used(buffer);
	switch(NSC_MSG_pre_decode(buf_ptr, buf_len)) {
	case NSC_DECODE_STATE_INCOMPLETE:
		return 1;
	case NSC_DECODE_STATE_OK:
		break;
	case NSC_DECODE_STATE_CORRUPT:
		return 0;
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	tmp = NSC_MSG_decode(&io->msg, buf_ptr, buf_len);
	NEURAL_BUFFER_read(buffer, NULL, tmp);
	cmd = NSC_MSG_get_cmd(&io->msg);
	if((to_server && !io->msg.is_response) ||
			(!to_server && io->msg.is_response))
		return 0;
	if(io->state == PLUG_EMPTY) {
		io->data_used = 0;
		io->request_uid = io->msg.request_uid;
		io->cmd = cmd;
		io->state = PLUG_IO;
	} else {
		if((io->msg.request_uid != io->request_uid) ||
				(io->cmd != cmd))
			return 0;
		if(io->msg.data_len + io->data_used > NSC_MAX_TOTAL_DATA)
			return 0;
	}
	if(io->msg.data_len) {
		if(!NSC_PLUG_IO_make_space(io, io->msg.data_len))
			return 0;
		SYS_memcpy_n(unsigned char, io->data + io->data_used,
				io->msg.data, io->msg.data_len);
		io->data_used += io->msg.data_len;
	}
	if(io->msg.complete)
		io->state = PLUG_FULL;
	else
		goto start_over;
	return 1;
}

static int NSC_PLUG_IO_read(NSC_PLUG_IO *io, int resume,
			unsigned long *request_uid,
			NSC_CMD *cmd,
			const unsigned char **payload_data,
	                unsigned int *payload_len)
{
	switch(io->state) {
	case PLUG_EMPTY:
	case PLUG_IO:
		return 0;
	case PLUG_USER:
		if(!resume)
			return 0;
		break;
	case PLUG_FULL:
		io->state = PLUG_USER;
		break;
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	*request_uid = io->request_uid;
	*cmd = io->cmd;
	*payload_data = io->data;
	*payload_len = io->data_used;
	return 1;
}

static int NSC_PLUG_IO_consume(NSC_PLUG_IO *io, int to_server,
				NEURAL_BUFFER *buffer)
{
	switch(io->state) {
	case PLUG_EMPTY:
	case PLUG_IO:
	case PLUG_FULL:
		return 0;
	case PLUG_USER:
		break;
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	io->data_used = 0;
	io->state = PLUG_EMPTY;
	return NSC_PLUG_IO_read_flush(io, to_server, buffer);
}

static int NSC_PLUG_IO_write_flush(NSC_PLUG_IO *io, int to_server,
				NEURAL_BUFFER *buffer)
{
	unsigned char *buf_ptr;
	unsigned int buf_len, tmp;

	switch(io->state) {
	case PLUG_EMPTY:
	case PLUG_USER:
		return 1;
	case PLUG_IO:
		break;
	case PLUG_FULL:
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
start_over:
	buf_ptr = NEURAL_BUFFER_write_ptr(buffer);
	buf_len = NEURAL_BUFFER_unused(buffer);
	io->msg.is_response = (to_server ? 0 : 1);
	if(!NSC_MSG_set_cmd(&io->msg, io->cmd))
		return 0;
	io->msg.request_uid = io->request_uid;
	io->msg.data_len = (io->data_used > NSC_MSG_MAX_DATA ?
			NSC_MSG_MAX_DATA : io->data_used);
	io->msg.complete = ((io->msg.data_len == io->data_used) ? 1 : 0);
	SYS_memcpy_n(unsigned char, io->msg.data, io->data, io->msg.data_len);
	if(NSC_MSG_encoding_size(&io->msg) > buf_len)
		return 1;
	io->msg.proto_level = NEUROCACHE_PROTO_LEVEL;
	tmp = NSC_MSG_encode(&io->msg, buf_ptr, buf_len);
	if(!tmp)
		return 0;
	NEURAL_BUFFER_wrote(buffer, tmp);
	io->data_used -= io->msg.data_len;
	if(io->data_used) {
		SYS_memmove_n(unsigned char, io->data,
				io->data + io->msg.data_len,
				io->data_used);
		goto start_over;
	}
	io->state = PLUG_EMPTY;
	return 1;
}

static int NSC_PLUG_IO_write(NSC_PLUG_IO *io, int resume,
			unsigned long request_uid,
			NSC_CMD cmd,
			const unsigned char *payload_data,
			unsigned int payload_len)
{
	switch(io->state) {
	case PLUG_IO:
		return 0;
	case PLUG_USER:
		if(!resume)
			return 0;
	case PLUG_EMPTY:
		break;
	case PLUG_FULL:
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	if(payload_len > NSC_MAX_TOTAL_DATA)
		return 0;
	if(!NSC_PLUG_IO_make_space(io, payload_len))
		return 0;
	io->state = PLUG_USER;
	io->request_uid = request_uid;
	io->cmd = cmd;
	io->data_used = payload_len;
	if(payload_len)
		SYS_memcpy_n(unsigned char, io->data, payload_data, payload_len);
	return 1;
}

static int NSC_PLUG_IO_write_more(NSC_PLUG_IO *io,
			const unsigned char *data,
			unsigned int data_len)
{
	switch(io->state) {
	case PLUG_USER:
		break;
	case PLUG_IO:
	case PLUG_EMPTY:
		return 0;
	case PLUG_FULL:
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	if((io->data_used + data_len > NSC_MAX_TOTAL_DATA) ||
			!data || !data_len)
		return 0;
	if(!NSC_PLUG_IO_make_space(io, data_len))
		return 0;
	SYS_memcpy_n(unsigned char, io->data + io->data_used, data, data_len);
	io->data_used += data_len;
	return 1;
}

static int NSC_PLUG_IO_commit(NSC_PLUG_IO *io, int to_server,
			NEURAL_BUFFER *buffer)
{
	switch(io->state) {
	case PLUG_USER:
		break;
	case PLUG_IO:
	case PLUG_EMPTY:
		return 0;
	case PLUG_FULL:
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	io->state = PLUG_IO;
	return NSC_PLUG_IO_write_flush(io, to_server, buffer);
}

static int NSC_PLUG_IO_rollback(NSC_PLUG_IO *io)
{
	switch(io->state) {
	case PLUG_USER:
		break;
	case PLUG_IO:
	case PLUG_EMPTY:
		return 0;
	case PLUG_FULL:
	default:
		assert(NULL == "shouldn't be here");
		return 0;
	}
	io->state = PLUG_EMPTY;
	io->data_used = 0;
	return 1;
}

NSC_PLUG *NSC_PLUG_new(NEURAL_CONNECTION *conn, unsigned int flags)
{
	NSC_PLUG *toret = SYS_malloc(NSC_PLUG, 1);
	if(!toret)
		return NULL;
	toret->conn = conn;
	toret->flags = flags;
	if(NSC_PLUG_IO_init(&toret->read) && NSC_PLUG_IO_init(&toret->write))
		return toret;
	SYS_free(NSC_PLUG, toret);
	return NULL;
}

int NSC_PLUG_free(NSC_PLUG *plug)
{
	if(!(plug->flags & NSC_PLUG_FLAG_NOFREE_CONN))
		NEURAL_CONNECTION_free(plug->conn);
	NSC_PLUG_IO_finish(&plug->read);
	NSC_PLUG_IO_finish(&plug->write);
	SYS_free(NSC_PLUG, plug);
	return 1;
}

int NSC_PLUG_to_select(NSC_PLUG *plug, NEURAL_SELECTOR *sel)
{
	return NEURAL_CONNECTION_add_to_selector(plug->conn, sel);
}

void NSC_PLUG_from_select(NSC_PLUG *plug)
{
	NEURAL_CONNECTION_del_from_selector(plug->conn);
}

int NSC_PLUG_io(NSC_PLUG *plug)
{
	int to_server = plug->flags & NSC_PLUG_FLAG_TO_SERVER;
	if(!NEURAL_CONNECTION_io(plug->conn))
		return 0;
	if(!NSC_PLUG_IO_read_flush(&plug->read, to_server,
				NEURAL_CONNECTION_get_read(plug->conn)) ||
			!NSC_PLUG_IO_write_flush(&plug->write, to_server,
				NEURAL_CONNECTION_get_send(plug->conn)))
		return 0;
	return 1;
}

int NSC_PLUG_read(NSC_PLUG *plug, int resume,
			unsigned long *request_uid,
			NSC_CMD *cmd,
			const unsigned char **payload_data,
	                unsigned int *payload_len)
{
	return NSC_PLUG_IO_read(&plug->read, resume, request_uid, cmd,
			payload_data, payload_len);
}

int NSC_PLUG_consume(NSC_PLUG *plug)
{
	return NSC_PLUG_IO_consume(&plug->read,
			plug->flags & NSC_PLUG_FLAG_TO_SERVER,
			NEURAL_CONNECTION_get_read(plug->conn));
}

int NSC_PLUG_write(NSC_PLUG *plug, int resume,
			unsigned long request_uid,
			NSC_CMD cmd,
			const unsigned char *payload_data,
			unsigned int payload_len)
{
	return NSC_PLUG_IO_write(&plug->write, resume, request_uid, cmd,
			payload_data, payload_len);
}

int NSC_PLUG_write_more(NSC_PLUG *plug,
			const unsigned char *data,
			unsigned int data_len)
{
	return NSC_PLUG_IO_write_more(&plug->write, data, data_len);
}

int NSC_PLUG_commit(NSC_PLUG *plug)
{
	return NSC_PLUG_IO_commit(&plug->write,
			plug->flags & NSC_PLUG_FLAG_TO_SERVER,
			NEURAL_CONNECTION_get_send(plug->conn));
}

int NSC_PLUG_rollback(NSC_PLUG *plug)
{
	return NSC_PLUG_IO_rollback(&plug->write);
}
