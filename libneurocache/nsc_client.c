/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <neurocache/nsc_client.h>
#include <neurocache/nsc_plug.h>
#include <neurocache/nsc_internal.h>
#include <libgen/post.h>

#include <errno.h>
#include <assert.h>

#define NSC_CTX_BUFFER_SIZE		4096
#define NSC_RET_START_SIZE		2048

struct st_NSC_CTX {
	NEURAL_ADDRESS *address;
	NSC_PLUG *plug;
	unsigned int flags;
	pid_t current_pid;
	unsigned int last_op_was_get;
	unsigned char last_get_id[NSC_MAX_ID_LEN];
	unsigned int last_get_id_len;
	unsigned char read_data[NSC_MAX_TOTAL_DATA];
	unsigned int read_data_len;
	unsigned char send_data[NSC_MAX_TOTAL_DATA];
	unsigned int send_data_len;
};

static NSC_PLUG *int_temp_connect(NSC_CTX *ctx)
{
	NEURAL_CONNECTION *conn;
	NSC_PLUG *plug;
	if(((conn = NEURAL_CONNECTION_new()) != NULL) &&
			NEURAL_CONNECTION_create(conn, ctx->address) &&
			((plug = NSC_PLUG_new(conn,
				NSC_PLUG_FLAG_TO_SERVER)) != NULL))
		return plug;
	if(conn)
		NEURAL_CONNECTION_free(conn);
	return NULL;
}

static int int_connect(NSC_CTX *ctx)
{
	if(ctx->plug) {
		NSC_PLUG_free(ctx->plug);
		ctx->plug = NULL;
	}
	if((ctx->plug = int_temp_connect(ctx)) == NULL)
		return 0;
	return 1;
}

static int int_netloop(NSC_PLUG *plug, NEURAL_SELECTOR *sel)
{
	int ret;
reselect:
	ret = NEURAL_SELECTOR_select(sel, 0, 0);
	if((ret < 0) && (errno != EINTR))
		return 0;
	if(ret <= 0)
		goto reselect;
	if(!NSC_PLUG_io(plug))
		return 0;
	return 1;
}

static unsigned long global_uid = 1;

static int int_transact(NSC_CTX *ctx, NSC_CMD cmd)
{
	NSC_PLUG *plug;
	NEURAL_SELECTOR *sel;
	NSC_CMD check_cmd;
	const unsigned char *ret_data;
	unsigned int ret_len;
	pid_t pid;
	int toreturn = 0;
	int retried = 0;
	unsigned long check_uid, request_uid = global_uid++;
	if(cmd != NSC_CMD_GET)
		ctx->last_op_was_get = 0;
	ctx->read_data_len = 0;
	if(ctx->flags & NSC_CTX_FLAG_PERSISTENT) {
		if(((ctx->flags & NSC_CTX_FLAG_PERSISTENT_PIDCHECK) &&
				((pid = SYS_getpid()) != ctx->current_pid)) ||
				((ctx->flags & NSC_CTX_FLAG_PERSISTENT_LATE)
					&& !ctx->plug)) {
			if(!int_connect(ctx))
				return 0;
		}
		plug = ctx->plug;
	} else {
		if((plug = int_temp_connect(ctx)) == NULL)
			return 0;
	}
	if((sel = NEURAL_SELECTOR_new()) == NULL)
		goto err;
	if(!NSC_PLUG_to_select(plug, sel))
		goto err;
restart_after_net_err:
	if(ctx->send_data_len && (!NSC_PLUG_write(plug, 0, request_uid, cmd,
				ctx->send_data, ctx->send_data_len) ||
			!NSC_PLUG_commit(plug)))
		goto err;
reselect:
	if(!int_netloop(plug, sel))
		goto net_err;
	if(!NSC_PLUG_read(plug, 0, &check_uid, &check_cmd,
				&ret_data, &ret_len))
		goto reselect;
	if((check_uid != request_uid) || (check_cmd != cmd) || !ret_data ||
			!ret_len || (ret_len > NSC_MAX_TOTAL_DATA))
		goto err;
	ctx->read_data_len = ret_len;
	SYS_memcpy_n(unsigned char, ctx->read_data, ret_data, ret_len);
	NSC_PLUG_consume(plug);
	toreturn = 1;
err:
	ctx->send_data_len = 0;
	if(!(ctx->flags & NSC_CTX_FLAG_PERSISTENT) && plug)
		NSC_PLUG_free(plug);
	else if(plug)
		NSC_PLUG_from_select(plug);
	if(sel)
		NEURAL_SELECTOR_free(sel);
	return toreturn;
net_err:
	if(retried || !(ctx->flags & NSC_CTX_FLAG_PERSISTENT) ||
			!(ctx->flags & NSC_CTX_FLAG_PERSISTENT_RETRY))
		goto err;
	retried = 1;
	if(!int_connect(ctx))
		goto err;
	plug = ctx->plug;
	if(!NSC_PLUG_to_select(plug, sel))
		goto err;
	goto restart_after_net_err;
}

NSC_CTX *NSC_CTX_new(const char *target, unsigned int flags)
{
	NSC_CTX *ctx = SYS_malloc(NSC_CTX, 1);
	if(!ctx)
		goto err;
	ctx->flags = flags;
	ctx->current_pid = SYS_getpid();
	ctx->plug = NULL;
	ctx->last_op_was_get = ctx->last_get_id_len = 0;
	ctx->read_data_len = ctx->send_data_len = 0;
	if(((ctx->address = NEURAL_ADDRESS_new()) == NULL) ||
			!NEURAL_ADDRESS_create(ctx->address, target,
				NSC_CTX_BUFFER_SIZE) ||
			!NEURAL_ADDRESS_can_connect(ctx->address))
		goto err;
	if(((flags & NSC_CTX_FLAG_PERSISTENT) &&
			!(flags & NSC_CTX_FLAG_PERSISTENT_LATE)) &&
			!int_connect(ctx))
		goto err;
	return ctx;
err:
	if(ctx) {
		if(ctx->address)
			NEURAL_ADDRESS_free(ctx->address);
		if(ctx->plug)
			NSC_PLUG_free(ctx->plug);
		SYS_free(NSC_CTX, ctx);
	}
	return NULL;
}

void NSC_CTX_free(NSC_CTX *ctx)
{
	if(ctx->plug)
		NSC_PLUG_free(ctx->plug);
	NEURAL_ADDRESS_free(ctx->address);
	SYS_free(NSC_CTX, ctx);
}

int NSC_CTX_add_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len,
			const unsigned char *sess_data,
			unsigned int sess_len,
			unsigned long timeout_msecs)
{
	unsigned char *ptr;
	unsigned int check;
	assert(id_data && sess_data && id_len && sess_len &&
			(id_len <= NSC_MAX_TOTAL_DATA) &&
			(timeout_msecs > NSC_MIN_TIMEOUT));
	ctx->send_data_len = id_len + sess_len + 8;
	if(ctx->send_data_len > NSC_MAX_TOTAL_DATA)
		return 0;
	ptr = ctx->send_data;
	check = ctx->send_data_len;
	if(!NEURAL_encode_uint32(&ptr, &check, timeout_msecs) ||
			!NEURAL_encode_uint32(&ptr, &check, id_len))
		return 0;
	assert((check + 8) == ctx->send_data_len);
	assert((ctx->send_data + 8) == ptr);
	SYS_memcpy_n(unsigned char, ptr, id_data, id_len);
	ptr += id_len;
	SYS_memcpy_n(unsigned char, ptr, sess_data, sess_len);
	if(!int_transact(ctx, NSC_CMD_ADD))
		return 0;
	if((ctx->read_data_len != 1) || (ctx->read_data[0] !=
					NSC_ERR_OK))
		return 0;
	return 1;
}

int NSC_CTX_remove_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len)
{
	assert(id_data && id_len && (id_len <= NSC_MAX_TOTAL_DATA));
	ctx->send_data_len = id_len;
	SYS_memcpy_n(unsigned char, ctx->send_data, id_data, id_len);
	if(!int_transact(ctx, NSC_CMD_REMOVE))
		return 0;
	if((ctx->read_data_len != 1) || (ctx->read_data[0] !=
					NSC_ERR_OK))
		return 0;
	return 1;
}

static void get_helper(NSC_CTX *ctx, unsigned char *result_storage,
			unsigned int result_size,
			unsigned int *result_used)
{
	*result_used = ctx->read_data_len;
	if(result_storage) {
		unsigned int tocopy = ctx->read_data_len;
		if(tocopy > result_size)
			tocopy = result_size;
		if(tocopy)
			SYS_memcpy_n(unsigned char, result_storage,
					ctx->read_data, tocopy);
	}
}

int NSC_CTX_get_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len,
			unsigned char *result_storage,
			unsigned int result_size,
			unsigned int *result_used)
{
	assert(id_data && id_len && (id_len <= NSC_MAX_TOTAL_DATA));
	ctx->send_data_len = id_len;
	SYS_memcpy_n(unsigned char, ctx->send_data, id_data, id_len);
	if(!int_transact(ctx, NSC_CMD_GET))
		return 0;
	if(ctx->read_data_len < 5)
		return 0;
	ctx->last_op_was_get = 1;
	ctx->last_get_id_len = id_len;
	SYS_memcpy_n(unsigned char, ctx->last_get_id, id_data, id_len);
	get_helper(ctx, result_storage, result_size, result_used);
	return 1;
}

int NSC_CTX_reget_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len,
			unsigned char *result_storage,
			unsigned int result_size,
			unsigned int *result_used)
{
	if(!ctx->last_op_was_get)
		return 0;
	if((ctx->last_get_id_len != id_len) ||
			(memcmp(ctx->last_get_id, id_data, id_len) != 0))
		return 0;
	get_helper(ctx, result_storage, result_size, result_used);
	return 1;
}

int NSC_CTX_has_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len)
{
	assert(id_data && id_len && (id_len <= NSC_MAX_TOTAL_DATA));
	ctx->send_data_len = id_len;
	SYS_memcpy_n(unsigned char, ctx->send_data, id_data, id_len);
	if(!int_transact(ctx, NSC_CMD_HAVE))
		return -1;
	if(ctx->read_data_len != 1)
		return 0;
	switch(ctx->read_data[0]) {
	case NSC_ERR_OK:
		return 1;
	case NSC_ERR_NOTOK:
		return 0;
	default:
		break;
	}
	return -1;
}
