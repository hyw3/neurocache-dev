/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_NEUROCACHE_NSC_CLIENT_H
#define HEADER_NEUROCACHE_NSC_CLIENT_H
#define NEUROCACHE_CLIENT_API     0x0001
#define NEUROCACHE_CLIENT_BINARY  0x0001

typedef struct st_NSC_CTX NSC_CTX;

#define NSC_CTX_FLAG_PERSISTENT		(unsigned int)0x0001
#define NSC_CTX_FLAG_PERSISTENT_PIDCHECK	(unsigned int)0x0002
#define NSC_CTX_FLAG_PERSISTENT_RETRY	(unsigned int)0x0004
#define NSC_CTX_FLAG_PERSISTENT_LATE	(unsigned int)0x0008

#define NSC_MIN_TIMEOUT			500

NSC_CTX *NSC_CTX_new(const char *target, unsigned int flags);

void NSC_CTX_free(NSC_CTX *ctx);
int NSC_CTX_add_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len,
			const unsigned char *sess_data,
			unsigned int sess_len,
			unsigned long timeout_msecs);

int NSC_CTX_remove_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len);

int NSC_CTX_get_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len,
			unsigned char *result_storage,
			unsigned int result_size,
			unsigned int *result_used);

int NSC_CTX_reget_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len,
			unsigned char *result_storage,
			unsigned int result_size,
			unsigned int *result_used);

int NSC_CTX_has_session(NSC_CTX *ctx,
			const unsigned char *id_data,
			unsigned int id_len);

#endif /* !defined(HEADER_NEUROCACHE_NSC_CLIENT_H) */
