/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_NEUROCACHE_NSC_SERVER_H
#define HEADER_NEUROCACHE_NSC_SERVER_H

#define NSC_CACHE_MIN_SIZE		64
#define NSC_CACHE_MAX_SIZE		60000
#define NSC_MAX_EXPIRY			(unsigned long)604800000
#define NSC_MAX_DATA_LEN			32768

typedef struct st_NSC_SERVER NSC_SERVER;
typedef struct st_NSC_CLIENT NSC_CLIENT;
typedef struct st_NSC_CACHE  NSC_CACHE;
typedef struct st_NSC_CACHE_cb {
	NSC_CACHE *	(*cache_new)(unsigned int max_sessions);
	void		(*cache_free)(NSC_CACHE *cache);
	int		(*cache_add)(NSC_CACHE *cache,
				const struct timeval *now,
				unsigned long timeout_msecs,
				const unsigned char *session_id,
				unsigned int session_id_len,
				const unsigned char *data,
				unsigned int data_len);
	unsigned int	(*cache_get)(NSC_CACHE *cache,
				const struct timeval *now,
				const unsigned char *session_id,
				unsigned int session_id_len,
				unsigned char *store,
				unsigned int store_size);
	int		(*cache_remove)(NSC_CACHE *cache,
				const struct timeval *now,
				const unsigned char *session_id,
				unsigned int session_id_len);
	int		(*cache_have)(NSC_CACHE *cache,
				const struct timeval *now,
				const unsigned char *session_id,
				unsigned int session_id_len);
	unsigned int	(*cache_num_items)(NSC_CACHE *cache,
				const struct timeval *now);
} NSC_CACHE_cb;

#define NSC_CLIENT_FLAG_NOFREE_CONN		(unsigned int)0x0001
#define NSC_CLIENT_FLAG_IN_SERVER		(unsigned int)0x0002

NSC_SERVER *NSC_SERVER_new(unsigned int max_sessions);

void NSC_SERVER_free(NSC_SERVER *ctx);

int NSC_SERVER_set_default_cache(void);

int NSC_SERVER_set_cache(const NSC_CACHE_cb *impl);

unsigned int NSC_SERVER_items_stored(NSC_SERVER *ctx,
				const struct timeval *now);

void NSC_SERVER_reset_operations(NSC_SERVER *ctx);

unsigned long NSC_SERVER_num_operations(NSC_SERVER *ctx);

NSC_CLIENT *NSC_SERVER_new_client(NSC_SERVER *ctx,
				NEURAL_CONNECTION *conn,
				unsigned int flags);

int NSC_SERVER_del_client(NSC_CLIENT *clnt);

int NSC_SERVER_process_client(NSC_CLIENT *clnt,
				const struct timeval *now);

int NSC_SERVER_clients_io(NSC_SERVER *ctx, const struct timeval *now);

int NSC_SERVER_clients_empty(const NSC_SERVER *ctx);

#endif /* !defined(HEADER_NEUROCACHE_NSC_SERVER_H) */
