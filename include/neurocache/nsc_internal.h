/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_NEUROCACHE_NSC_INTERNAL_H
#define HEADER_NEUROCACHE_NSC_INTERNAL_H

#ifndef HEADER_NEUROCACHE_NSC_PLUG_H
#error "Must include nsc_plug.h prior to nsc_internal.h"
#endif

typedef enum {
	NSC_CLASS_USER = 0,
	NSC_CLASS_LAST = NSC_CLASS_USER
} NSC_CLASS;

typedef enum {
	NSC_OP_ADD = 0,
	NSC_OP_GET,
	NSC_OP_REMOVE,
	NSC_OP_HAVE
} NSC_OP;

typedef enum {
	NSC_ERR_OK = 0,
	NSC_ERR_NOTOK,
	NSC_ERR_DISCONNECTED
} NSC_ERR;

typedef enum {
	NSC_ADD_ERR_CORRUPT = 100,
	NSC_ADD_ERR_MATCHING_SESSION,
	NSC_ADD_ERR_TIMEOUT_RANGE,
	NSC_ADD_ERR_ID_RANGE,
	NSC_ADD_ERR_DATA_RANGE
} NSC_ADD_ERR;

typedef enum {
	NSC_DECODE_STATE_CORRUPT,
	NSC_DECODE_STATE_INCOMPLETE,
	NSC_DECODE_STATE_OK
} NSC_DECODE_STATE;

typedef struct st_NSC_MSG {
	unsigned long	proto_level;
	unsigned char	is_response;
	unsigned long	request_uid;
	NSC_CLASS	op_class;
	NSC_OP		operation;
	unsigned char	complete;
	unsigned int	data_len;
	unsigned char	data[NSC_MSG_MAX_DATA];
} NSC_MSG;

#if 0

int NSC_MSG_set_cmd(NSC_MSG *msg, NSC_CMD cmd);
NSC_CMD NSC_MSG_get_cmd(const NSC_MSG *msg);
int NSC_MSG_start_response(const NSC_MSG *request,
				NSC_MSG *response);
unsigned int NSC_MSG_encoding_size(const NSC_MSG *msg);
unsigned int NSC_MSG_encode(const NSC_MSG *msg, unsigned char *ptr,
				unsigned int data_len);
NSC_DECODE_STATE NSC_MSG_pre_decode(const unsigned char *data,
				unsigned int data_len);
unsigned int NSC_MSG_decode(NSC_MSG *msg, const unsigned char *data,
				unsigned int data_len);
#endif

#endif /* !defined(HEADER_NEUROCACHE_NSC_INTERNAL_H) */
