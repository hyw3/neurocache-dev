/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_NEUROCACHE_NSC_PLUG_H
#define HEADER_NEUROCACHE_NSC_PLUG_H

#define NEUROCACHE_GET_PROTO_VER(a)	((a) >> 16)
#define NEUROCACHE_GET_PATCH_LEVEL(a)	((a) & 0x0000FFFF)
#define NEUROCACHE_MAKE_PROTO_LEVEL(a,b)	(((a) << 16) + (b))
#define NEUROCACHE_PROTO_VER	0x11
#define NEUROCACHE_PATCH_LEVEL	0x00
#define NEUROCACHE_PROTO_LEVEL	NEUROCACHE_MAKE_PROTO_LEVEL(\
					NEUROCACHE_PROTO_VER,NEUROCACHE_PATCH_LEVEL)

typedef enum {
	NSC_CMD_ERROR,
	NSC_CMD_ADD,
	NSC_CMD_GET,
	NSC_CMD_REMOVE,
	NSC_CMD_HAVE
} NSC_CMD;

#define NSC_MSG_MAX_DATA		2048
#define NSC_MAX_MSG		16
#define NSC_MAX_TOTAL_DATA	(NSC_MSG_MAX_DATA * NSC_MAX_MSG)
#define NSC_MAX_ID_LEN		64

typedef struct st_NSC_PLUG NSC_PLUG;

#define NSC_PLUG_FLAG_TO_SERVER		(unsigned int)0x0001
#define NSC_PLUG_FLAG_NOFREE_CONN	(unsigned int)0x0002

NSC_PLUG *NSC_PLUG_new(NEURAL_CONNECTION *conn, unsigned int flags);

int NSC_PLUG_free(NSC_PLUG *plug);
int NSC_PLUG_to_select(NSC_PLUG *plug, NEURAL_SELECTOR *sel);
void NSC_PLUG_from_select(NSC_PLUG *plug);
int NSC_PLUG_io(NSC_PLUG *plug);

int NSC_PLUG_read(NSC_PLUG *plug, int resume,
		unsigned long *request_uid,
		NSC_CMD *cmd,
		const unsigned char **payload_data,
		unsigned int *payload_len);
int NSC_PLUG_consume(NSC_PLUG *plug);
int NSC_PLUG_write(NSC_PLUG *plug, int resume,
		unsigned long request_uid,
		NSC_CMD cmd,
		const unsigned char *payload_data,
		unsigned int payload_len);
int NSC_PLUG_write_more(NSC_PLUG *plug,
		const unsigned char *data, unsigned int data_len);
int NSC_PLUG_commit(NSC_PLUG *plug);
int NSC_PLUG_rollback(NSC_PLUG *plug);

#endif /* !defined(HEADER_NEUROCACHE_NSC_PLUG_H) */
