/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_LIBNEURAL_NEURAL_BUILD_H
#define HEADER_LIBNEURAL_NEURAL_BUILD_H

#ifndef HEADER_LIBNEURAL_NEURAL_H
	#error "Must include libneural/neural.h prior to libneural/neural_build.h"
#endif

typedef struct st_NEURAL_ADDRESS_vtable NEURAL_ADDRESS_vtable;
typedef struct st_NEURAL_LISTENER_vtable NEURAL_LISTENER_vtable;
typedef struct st_NEURAL_CONNECTION_vtable NEURAL_CONNECTION_vtable;
typedef struct st_NEURAL_SELECTOR_vtable NEURAL_SELECTOR_vtable;
typedef struct { int foo; } *NEURAL_SELECTOR_TOKEN;
#define NEURAL_SELECTOR_TOKEN_NULL	(NEURAL_SELECTOR_TOKEN)NULL

struct st_NEURAL_ADDRESS_vtable {
	const char *unique_name;
	size_t vtdata_size;
	const char **prefixes;
	int (*on_create)(NEURAL_ADDRESS *addr);
	void (*on_destroy)(NEURAL_ADDRESS *addr);
	void (*on_reset)(NEURAL_ADDRESS *addr);
	void (*pre_close)(NEURAL_ADDRESS *addr);
	int (*parse)(NEURAL_ADDRESS *addr, const char *addr_string);
	int (*can_connect)(const NEURAL_ADDRESS *addr);
	int (*can_listen)(const NEURAL_ADDRESS *addr);
	const NEURAL_LISTENER_vtable *(*create_listener)(const NEURAL_ADDRESS *addr);
	const NEURAL_CONNECTION_vtable *(*create_connection)(const NEURAL_ADDRESS *addr);
	struct st_NEURAL_ADDRESS_vtable *next;
};

int neural_address_set_vtable(NEURAL_ADDRESS *addr, const NEURAL_ADDRESS_vtable *vt);
const NEURAL_ADDRESS_vtable *neural_address_get_vtable(const NEURAL_ADDRESS *addr);
void *neural_address_get_vtdata(const NEURAL_ADDRESS *addr);
const NEURAL_LISTENER_vtable *neural_address_get_listener(const NEURAL_ADDRESS *addr);
const NEURAL_CONNECTION_vtable *neural_address_get_connection(const NEURAL_ADDRESS *addr);

struct st_NEURAL_LISTENER_vtable {
	size_t vtdata_size;
	int (*on_create)(NEURAL_LISTENER *);
	void (*on_destroy)(NEURAL_LISTENER *);
	void (*on_reset)(NEURAL_LISTENER *);
	void (*pre_close)(NEURAL_LISTENER *);
	int (*listen)(NEURAL_LISTENER *, const NEURAL_ADDRESS *);
	const NEURAL_CONNECTION_vtable *(*pre_accept)(NEURAL_LISTENER *);
	int (*finished)(const NEURAL_LISTENER *);
	int (*pre_selector_add)(NEURAL_LISTENER *, NEURAL_SELECTOR *);
	int (*post_selector_add)(NEURAL_LISTENER *, NEURAL_SELECTOR *,
				NEURAL_SELECTOR_TOKEN);
	void (*pre_selector_del)(NEURAL_LISTENER *, NEURAL_SELECTOR *,
				NEURAL_SELECTOR_TOKEN);
	void (*post_selector_del)(NEURAL_LISTENER *, NEURAL_SELECTOR *);
	void (*pre_select)(NEURAL_LISTENER *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
	void (*post_select)(NEURAL_LISTENER *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
	int (*set_fs_owner)(NEURAL_LISTENER *, const char *ownername,
				const char *groupname);
	int (*set_fs_perms)(NEURAL_LISTENER *, const char *octal_string);
};

int neural_listener_set_vtable(NEURAL_LISTENER *, const NEURAL_LISTENER_vtable *);
const NEURAL_LISTENER_vtable *neural_listener_get_vtable(const NEURAL_LISTENER *);
void *neural_listener_get_vtdata(const NEURAL_LISTENER *);
const NEURAL_CONNECTION_vtable *neural_listener_pre_accept(NEURAL_LISTENER *);
void neural_listener_pre_select(NEURAL_LISTENER *);
void neural_listener_post_select(NEURAL_LISTENER *);
NEURAL_SELECTOR *neural_listener_get_selector(const NEURAL_LISTENER *,
					NEURAL_SELECTOR_TOKEN *);
void neural_listener_set_selector_raw(NEURAL_LISTENER *, NEURAL_SELECTOR *,
					NEURAL_SELECTOR_TOKEN);
struct st_NEURAL_CONNECTION_vtable {
	size_t vtdata_size;
	int (*on_create)(NEURAL_CONNECTION *);
	void (*on_destroy)(NEURAL_CONNECTION *);
	void (*on_reset)(NEURAL_CONNECTION *);
	void (*pre_close)(NEURAL_CONNECTION *);
	int (*connect)(NEURAL_CONNECTION *, const NEURAL_ADDRESS *);
	int (*accept)(NEURAL_CONNECTION *, const NEURAL_LISTENER *);
	int (*set_size)(NEURAL_CONNECTION *, unsigned int);
	NEURAL_BUFFER *(*get_read)(const NEURAL_CONNECTION *);
	NEURAL_BUFFER *(*get_send)(const NEURAL_CONNECTION *);
	int (*is_established)(const NEURAL_CONNECTION *);
	int (*pre_selector_add)(NEURAL_CONNECTION *, NEURAL_SELECTOR *);
	int (*post_selector_add)(NEURAL_CONNECTION *, NEURAL_SELECTOR *,
				NEURAL_SELECTOR_TOKEN);
	void (*pre_selector_del)(NEURAL_CONNECTION *, NEURAL_SELECTOR *,
				NEURAL_SELECTOR_TOKEN);
	void (*post_selector_del)(NEURAL_CONNECTION *, NEURAL_SELECTOR *);
	void (*pre_select)(NEURAL_CONNECTION *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
	void (*post_select)(NEURAL_CONNECTION *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
	int (*do_io)(NEURAL_CONNECTION *);
};

int neural_connection_set_vtable(NEURAL_CONNECTION *, const NEURAL_CONNECTION_vtable *);
const NEURAL_CONNECTION_vtable *neural_connection_get_vtable(const NEURAL_CONNECTION *);
void *neural_connection_get_vtdata(const NEURAL_CONNECTION *);
void neural_connection_pre_select(NEURAL_CONNECTION *);
void neural_connection_post_select(NEURAL_CONNECTION *);
NEURAL_SELECTOR *neural_connection_get_selector(const NEURAL_CONNECTION *,
					NEURAL_SELECTOR_TOKEN *);
void neural_connection_set_selector_raw(NEURAL_CONNECTION *, NEURAL_SELECTOR *,
					NEURAL_SELECTOR_TOKEN);

const NEURAL_ADDRESS_vtable *NEURAL_ADDRESS_vtable_builtins(void);
void NEURAL_ADDRESS_vtable_link(NEURAL_ADDRESS_vtable *vt);

const NEURAL_SELECTOR_vtable *sel_fdselect(void);
const NEURAL_SELECTOR_vtable *sel_fdpoll(void);

typedef enum {
	NEURAL_SELECTOR_TYPE_ERROR = 0,
	NEURAL_SELECTOR_TYPE_DYNAMIC,
	NEURAL_SELECTOR_TYPE_FDSELECT,
	NEURAL_SELECTOR_TYPE_FDPOLL,
	NEURAL_SELECTOR_TYPE_CUSTOM = 100
} NEURAL_SELECTOR_TYPE;

typedef enum {
	NEURAL_SELECTOR_CTRL_FD = 0x0100,
	NEURAL_SELECTOR_CTRL_CUSTOM = 0x0800
} NEURAL_SELECTOR_CTRL_TYPE;

struct st_NEURAL_SELECTOR_vtable {
	size_t vtdata_size;
	int (*on_create)(NEURAL_SELECTOR *);
	void (*on_destroy)(NEURAL_SELECTOR *);
	void (*on_reset)(NEURAL_SELECTOR *);
	void (*pre_close)(NEURAL_SELECTOR *);
	NEURAL_SELECTOR_TYPE (*get_type)(const NEURAL_SELECTOR *);
	int (*select)(NEURAL_SELECTOR *, unsigned long usec_timeout, int use_timeout);
	unsigned int (*num_objects)(const NEURAL_SELECTOR *);
	NEURAL_SELECTOR_TOKEN (*add_listener)(NEURAL_SELECTOR *, NEURAL_LISTENER *);
	NEURAL_SELECTOR_TOKEN (*add_connection)(NEURAL_SELECTOR *, NEURAL_CONNECTION *);
	void (*del_listener)(NEURAL_SELECTOR *, NEURAL_LISTENER *, NEURAL_SELECTOR_TOKEN);
	void (*del_connection)(NEURAL_SELECTOR *, NEURAL_CONNECTION *, NEURAL_SELECTOR_TOKEN);
	int (*ctrl)(NEURAL_SELECTOR *, int, void *);
};

NEURAL_SELECTOR *neural_selector_new(const NEURAL_SELECTOR_vtable *);
const NEURAL_SELECTOR_vtable *neural_selector_get_vtable(const NEURAL_SELECTOR *);
void *neural_selector_get_vtdata(const NEURAL_SELECTOR *);
NEURAL_SELECTOR_TYPE neural_selector_get_type(const NEURAL_SELECTOR *);
int neural_selector_ctrl(NEURAL_SELECTOR *, int, void *);
int neural_selector_dynamic_set(NEURAL_SELECTOR *, const NEURAL_SELECTOR_vtable *);

#endif /* !defined(HEADER_LIBNEURAL_NEURAL_BUILD_H) */
