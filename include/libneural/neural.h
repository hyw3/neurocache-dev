/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_LIBNEURAL_NEURAL_H
#define HEADER_LIBNEURAL_NEURAL_H

typedef struct st_NEURAL_ADDRESS NEURAL_ADDRESS;
typedef struct st_NEURAL_LISTENER NEURAL_LISTENER;
typedef struct st_NEURAL_CONNECTION NEURAL_CONNECTION;
typedef struct st_NEURAL_SELECTOR NEURAL_SELECTOR;
typedef struct st_NEURAL_BUFFER NEURAL_BUFFER;

#define NEURAL_SELECT_FLAG_READ	(unsigned int)0x0001
#define NEURAL_SELECT_FLAG_SEND	(unsigned int)0x0002
#define NEURAL_SELECT_FLAG_RW	(NEURAL_SELECT_FLAG_READ | NEURAL_SELECT_FLAG_SEND)

void		NEURAL_config_set_nagle(int enabled);

NEURAL_ADDRESS *	NEURAL_ADDRESS_new(void);
void		NEURAL_ADDRESS_free(NEURAL_ADDRESS *addr);
void		NEURAL_ADDRESS_reset(NEURAL_ADDRESS *addr);
int		NEURAL_ADDRESS_create(NEURAL_ADDRESS *addr, const char *addr_string,
				      unsigned int def_buffer_size);
unsigned int	NEURAL_ADDRESS_get_def_buffer_size(const NEURAL_ADDRESS *addr);
int		NEURAL_ADDRESS_set_def_buffer_size(NEURAL_ADDRESS *addr,
				                   unsigned int def_buffer_size);
int		NEURAL_ADDRESS_can_connect(const NEURAL_ADDRESS *addr);
int		NEURAL_ADDRESS_can_listen(const NEURAL_ADDRESS *addr);

NEURAL_SELECTOR *	NEURAL_SELECTOR_new(void);
void		NEURAL_SELECTOR_free(NEURAL_SELECTOR *sel);
void		NEURAL_SELECTOR_reset(NEURAL_SELECTOR *sel);
int		NEURAL_SELECTOR_select(NEURAL_SELECTOR *sel,
				       unsigned long usec_timeout,
				       int use_timeout);
unsigned int	NEURAL_SELECTOR_num_objects(const NEURAL_SELECTOR *sel);

NEURAL_SELECTOR *	NEURAL_SELECTOR_new_fdselect(void);
NEURAL_SELECTOR *	NEURAL_SELECTOR_new_fdpoll(void);
NEURAL_LISTENER *	NEURAL_LISTENER_new(void);
void		NEURAL_LISTENER_free(NEURAL_LISTENER *list);
void		NEURAL_LISTENER_reset(NEURAL_LISTENER *list);
int		NEURAL_LISTENER_create(NEURAL_LISTENER *list,
				const NEURAL_ADDRESS *addr);
int		NEURAL_LISTENER_add_to_selector(NEURAL_LISTENER *list,
				                NEURAL_SELECTOR *sel);
void		NEURAL_LISTENER_del_from_selector(NEURAL_LISTENER *list);
int		NEURAL_LISTENER_finished(const NEURAL_LISTENER *list);
int		NEURAL_LISTENER_set_fs_owner(NEURAL_LISTENER *list,
				             const char *ownername,
				             const char *groupname);
int		NEURAL_LISTENER_set_fs_perms(NEURAL_LISTENER *list,
				             const char *octal_string);
NEURAL_CONNECTION *NEURAL_CONNECTION_new(void);
void		NEURAL_CONNECTION_free(NEURAL_CONNECTION *conn);
void		NEURAL_CONNECTION_reset(NEURAL_CONNECTION *conn);
int		NEURAL_CONNECTION_create(NEURAL_CONNECTION *conn,
				         const NEURAL_ADDRESS *addr);
int		NEURAL_CONNECTION_accept(NEURAL_CONNECTION *conn,
				         NEURAL_LISTENER *list);
int		NEURAL_CONNECTION_set_size(NEURAL_CONNECTION *conn,
				           unsigned int size);
NEURAL_BUFFER *	NEURAL_CONNECTION_get_read(NEURAL_CONNECTION *conn);
NEURAL_BUFFER *	NEURAL_CONNECTION_get_send(NEURAL_CONNECTION *conn);
const NEURAL_BUFFER *NEURAL_CONNECTION_get_read_c(const NEURAL_CONNECTION *conn);
const NEURAL_BUFFER *NEURAL_CONNECTION_get_send_c(const NEURAL_CONNECTION *conn);
int		NEURAL_CONNECTION_io(NEURAL_CONNECTION *conn);
int		NEURAL_CONNECTION_is_established(const NEURAL_CONNECTION *conn);
int		NEURAL_CONNECTION_add_to_selector(NEURAL_CONNECTION *conn,
				                  NEURAL_SELECTOR *sel);
void		NEURAL_CONNECTION_del_from_selector(NEURAL_CONNECTION *conn);
int		NEURAL_CONNECTION_create_pair(NEURAL_CONNECTION *conn1,
				              NEURAL_CONNECTION *conn2,
				              unsigned int def_buffer_size);
#if 0
int		NEURAL_CONNECTION_create_dummy(NEURAL_CONNECTION *conn,
				               unsigned int def_buffer_size);
#endif

NEURAL_BUFFER *	NEURAL_BUFFER_new(void);
void		NEURAL_BUFFER_free(NEURAL_BUFFER *buf);
void		NEURAL_BUFFER_reset(NEURAL_BUFFER *buf);
int		NEURAL_BUFFER_set_size(NEURAL_BUFFER *buf,
				       unsigned int size);
int		NEURAL_BUFFER_empty(const NEURAL_BUFFER *buf);
int		NEURAL_BUFFER_full(const NEURAL_BUFFER *buf);
int		NEURAL_BUFFER_notempty(const NEURAL_BUFFER *buf);
int		NEURAL_BUFFER_notfull(const NEURAL_BUFFER *buf);
unsigned int	NEURAL_BUFFER_used(const NEURAL_BUFFER *buf);
unsigned int	NEURAL_BUFFER_unused(const NEURAL_BUFFER *buf);
unsigned int	NEURAL_BUFFER_size(const NEURAL_BUFFER *buf);
const unsigned char *NEURAL_BUFFER_data(const NEURAL_BUFFER *buf);
unsigned int 	NEURAL_BUFFER_write(NEURAL_BUFFER *buf,
				    const unsigned char *ptr,
				    unsigned int size);
unsigned int 	NEURAL_BUFFER_read(NEURAL_BUFFER *buf,
				   unsigned char *ptr,
				   unsigned int size);
unsigned int	NEURAL_BUFFER_transfer(NEURAL_BUFFER *dest, NEURAL_BUFFER *src,
				       unsigned int max);
unsigned char *	NEURAL_BUFFER_write_ptr(NEURAL_BUFFER *buf);
void		NEURAL_BUFFER_wrote(NEURAL_BUFFER *buf,
				    unsigned int size);

int NEURAL_decode_uint32(const unsigned char **bin, unsigned int *bin_len,
			 unsigned long *val);
int NEURAL_decode_uint16(const unsigned char **bin, unsigned int *bin_len,
			 unsigned int *val);
int NEURAL_decode_char(const unsigned char **bin, unsigned int *bin_len,
		       unsigned char *val);
int NEURAL_decode_bin(const unsigned char **bin, unsigned int *bin_len,
			unsigned char *val, unsigned int val_len);

int NEURAL_encode_uint32(unsigned char **bin, unsigned int *bin_len,
			const unsigned long val);
int NEURAL_encode_uint16(unsigned char **bin, unsigned int *bin_len,
			const unsigned int val);
int NEURAL_encode_char(unsigned char **bin, unsigned int *bin_len,
		       const unsigned char val);
int NEURAL_encode_bin(unsigned char **bin, unsigned int *bin_len,
		      const unsigned char *val, const unsigned int val_len);

#endif /* !defined(HEADER_LIBNEURAL_NEURAL_H) */

