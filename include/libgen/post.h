/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_LIBGEN_POST_H
#define HEADER_LIBGEN_POST_H

#ifndef HEADER_LIBGEN_PRE_H
#error "must include libgen/pre.h before other headers"
#endif

#ifndef SYS_DEBUG_LEVEL
#define SYS_DEBUG_LEVEL 1
#endif

#include <stdio.h>
#include <stdlib.h>

#define SYS_stdin	stdin
#define SYS_stdout	stdout
#define SYS_stderr	stderr
#define SYS_fprintf	fprintf

#ifndef LEAVE_PROCESSES_ALONE

#ifndef IN_SYS_C
#define daemon dont_use_daemon_but_use_SYS_daemon_instead
#endif

#endif

#define sys_malloc	malloc
#define sys_realloc	realloc
#define sys_free	free
#define sys_memset	memset
#define sys_memcpy	memcpy
#define sys_memmove	memmove

#if 0
#define SYS_malloc(t,n)		(t *)malloc((n) * sizeof(t))
#define SYS_realloc(t,p,n)	(t *)realloc((p), (n) * sizeof(t))
#define SYS_free(t,p)		free((p))
#define SYS_cover(c,t,p)	memset((p), (c), sizeof(t))
#define SYS_cover_n(c,t,p,n)	memset((p), (c), (n) * sizeof(t))
#define SYS_memcpy(t,d,s)	memcpy((d), (s), sizeof(t))
#define SYS_memcpy_n(t,d,s,n)	memcpy((d), (s), (n) * sizeof(t))
#define SYS_memmove(t,d,s)	memmove((d), (s), sizeof(t))
#define SYS_memmove_n(t,d,s,n)	memmove((d), (s), (n) * sizeof(t))
#else

#define SYS_malloc(t,n)		(t *)sys_malloc((n) * sizeof(t))
#define SYS_realloc(t,p,n)	(t *)sys_realloc((p), (n) * sizeof(t))
#define SYS_free(t,p)		do { \
				t *tmp_sys_free_4765 = (p); \
				sys_free(tmp_sys_free_4765); \
				} while(0)
#define SYS_cover(c,t,p)	do { \
				t *temp_SYS_cover_ptr = (p); \
				sys_memset(temp_SYS_cover_ptr, (c), \
						sizeof(t)); \
				} while(0)
#define SYS_cover_n(c,t,p,n)	do { \
				t *temp_SYS_cover_n_ptr = (p); \
				sys_memset(temp_SYS_cover_n_ptr, (c), \
						(n) * sizeof(t)); \
				} while(0)
#define SYS_memcpy(t,d,s)	do { \
				t *temp_SYS_memcpy_ptr1 = (d); \
				const t *temp_SYS_memcpy_ptr2 = (s); \
				sys_memcpy(temp_SYS_memcpy_ptr1, \
					temp_SYS_memcpy_ptr2, \
					sizeof(t)); \
				} while(0)
#define SYS_memcpy_n(t,d,s,n)	do { \
				t *temp_SYS_memcpy_ptr1 = (d); \
				const t *temp_SYS_memcpy_ptr2 = (s); \
				sys_memcpy(temp_SYS_memcpy_ptr1, \
					temp_SYS_memcpy_ptr2, \
					(n) * sizeof(t)); \
				} while(0)
#define SYS_memmove(t,d,s)	do { \
				t *temp_SYS_memmove_ptr1 = (d); \
				const t *temp_SYS_memmove_ptr2 = (s); \
				sys_memmove(temp_SYS_memmove_ptr1, \
					temp_SYS_memmove_ptr2, \
					sizeof(t)); \
				} while(0)
#define SYS_memmove_n(t,d,s,n)	do { \
				t *temp_SYS_memmove_ptr1 = (d); \
				const t *temp_SYS_memmove_ptr2 = (s); \
				sys_memmove(temp_SYS_memmove_ptr1, \
					temp_SYS_memmove_ptr2, \
					(n) * sizeof(t)); \
				} while(0)
#endif

#define SYS_zero(t,p)		SYS_cover(0,t,(p))
#define SYS_zero_n(t,p,n)	SYS_cover_n(0,t,(p),(n))
#define SYS_strncpy(d,s,n)	do { \
				char *tmp_SYS_strncpy1 = (d); \
				const char *tmp_SYS_strncpy2 = (s); \
				size_t tmp_SYS_strncpy3 = strlen(tmp_SYS_strncpy2), \
					tmp_SYS_strncpy4 = (n); \
				if(tmp_SYS_strncpy3 < tmp_SYS_strncpy4) \
					SYS_memcpy_n(char, (d), (s), tmp_SYS_strncpy3 + 1); \
				else { \
					SYS_memcpy_n(char, (d), (s), tmp_SYS_strncpy4); \
					tmp_SYS_strncpy1[tmp_SYS_strncpy4 - 1] = '\0'; \
				} \
				} while(0)
#define SYS_strdup(d,s)		do { \
				char **tmp_SYS_strdup1 = (d); \
				const char *tmp_SYS_strdup2 = (s); \
				size_t tmp_SYS_strdup3 = strlen(tmp_SYS_strdup2) + 1; \
				*tmp_SYS_strdup1 = SYS_malloc(char, tmp_SYS_strdup3); \
				if(*tmp_SYS_strdup1) \
					SYS_memcpy_n(char, *tmp_SYS_strdup1, \
						tmp_SYS_strdup2, tmp_SYS_strdup3); \
				} while(0)

#define SYS_cover_s(c,s)	memset(&(s), (c), sizeof(s))
#define SYS_zero_s(s)		SYS_cover_s(0,(s))
#define SYS_getpid	getpid
#define SYS_timecmp(a,b) \
		(((a)->tv_sec < (b)->tv_sec) ? -1 : \
			(((a)->tv_sec > (b)->tv_sec) ? 1 : \
			(((a)->tv_usec < (b)->tv_usec) ? -1 : \
			(((a)->tv_usec > (b)->tv_usec) ? 1 : 0))))
#define SYS_timecpy(d,s) SYS_memcpy(struct timeval, (d), (s))
#define SYS_timeadd(res,I,msecs) \
do { \
	struct timeval *_tmp_res = (res); \
	const struct timeval *_tmp_I = (I); \
	unsigned long _tmp_carry = _tmp_I->tv_usec + ((msecs) * 1000); \
	_tmp_res->tv_usec = _tmp_carry % 1000000; \
	_tmp_carry /= 1000000; \
	_tmp_res->tv_sec = _tmp_I->tv_sec + _tmp_carry; \
} while(0)
#ifdef WIN32
#define SYS_gettime(tv) \
do { \
	FILETIME decimillisecs; \
	unsigned __int64 crud; \
	GetSystemTimeAsFileTime(&decimillisecs); \
	crud = ((unsigned __int64)decimillisecs.dwHighDateTime << 32) + \
		(unsigned __int64)decimillisecs.dwLowDateTime; \
	crud /= 10; \
	crud -= (unsigned __int64)12614400000 * (unsigned __int64)1000000; \
	tv->tv_sec = (long)(crud / 1000000); \
	tv->tv_usec = (long)(crud % 1000000); \
} while(0)
#else
#define SYS_gettime(tv) \
do { \
	if(gettimeofday(tv, NULL) != 0)	abort(); \
} while(0)
#endif

#if defined(SYS_GENERATING_EXE) || defined(SYS_LOCAL)

#ifdef WIN32
int SYS_sockets_init(void);
#else
int SYS_sigpipe_ignore(void);
int SYS_sigusr_interrupt(int *ptr);
int SYS_daemon(int nochdir);
int SYS_setuid(const char *username);
#endif
void SYS_timesub(struct timeval *res, const struct timeval *I,
		unsigned long msecs);
int SYS_expirycheck(const struct timeval *timeitem, unsigned long msec_expiry,
		const struct timeval *timenow);
unsigned long SYS_msecs_between(const struct timeval *a, const struct timeval *b);

#if 0
pid_t SYS_getpid(void);
void SYS_gettime(struct timeval *tv);
int SYS_timecmp(const struct timeval *a, const struct timeval *b);
void SYS_timecpy(struct timeval *dest, const struct timeval *src);
void SYS_timeadd(struct timeval *res, const struct timeval *I,
		unsigned long msecs);
#endif

#endif /* defined(SYS_GENERATING_EXE) || defined(SYS_LOCAL) */

#endif /* !defined(HEADER_LIBGEN_POST_H) */

