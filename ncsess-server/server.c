/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_EXE

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <neurocache/nsc_server.h>
#include <neurocache/nsc_plug.h>
#include <neurocache/nsc_internal.h>
#include <libgen/post.h>

#include <assert.h>
#include <errno.h>

static const char *def_server = NULL;
static const unsigned int def_sessions = 512;
static const unsigned long def_progress = 0;
#ifndef WIN32
static const char *def_pidfile = NULL;
static const char *def_user = NULL;
static const char *def_sockowner = NULL;
static const char *def_sockgroup = NULL;
static const char *def_sockperms = NULL;
#endif

static int nsc_server_version(void)
{
	SYS_fprintf(SYS_stderr,
		"NeuroCache Server version %s (%s / %s)\n\n"
		"Distributed as part of the NeuroCache source package\n"
		"Copyright (C) 2019-2020 Hilman P. Alisabana, Jakarta.\n\n",
		HAVE_PACKAGE_VERSION, HAVE_HYSCMTERM_UUID, HAVE_HYSCMTERM_DATE);
}

static const char *usage_msg[] = {
"",
"Usage: nsc_server [options]     where 'options' are from;",
#ifndef WIN32
"  -daemon            (detach and run in the background)",
#endif
"  -listen <addr>     (act as a server listening on address 'addr')",
"  -sessions <num>    (make the cache hold a maximum of 'num' sessions)",
"  -progress <num>    (report cache progress at least every 'num' operations)",
#ifndef WIN32
"  -user <user>       (run daemon as given user)",
"  -sockowner <user>  (controls ownership of unix domain listening socket)",
"  -sockgroup <group> (controls ownership of unix domain listening socket)",
"  -sockperms <oct>   (set permissions of unix domain listening socket)",
"  -pidfile <path>    (a file to store the process ID in)",
"  -killable          (exit cleanly on a SIGUSR1 or SIGUSR2 signal)",
#endif
"  -<h|help|?>        (display this usage message)",
"  -<v|version>       (display version information)",
"\n",
"Eg. nsc_server -listen IP:9001",
"  will start a session cache server listening on port 9001 for all TCP/IP",
"  interfaces.",
"", NULL};

#define MAX_SESSIONS		NSC_CACHE_MAX_SIZE
#define MAX_PROGRESS		(unsigned long)1000000
#define SERVER_BUFFER_SIZE	4096

static int do_server(const char *address, unsigned int max_sessions,
			unsigned long progress, int daemon_mode,
			const char *pidfile, int killable, const char *user,
			const char *sockowner, const char *sockgroup,
			const char *sockperms);

static int usage(void)
{
	const char **u = usage_msg;
	while(*u)
		SYS_fprintf(SYS_stderr, "%s\n", *(u++));
	return 0;
}

static const char *CMD_VERS1 = "-v";
static const char *CMD_VERS2 = "-version";
static const char *CMD_HELP1 = "-h";
static const char *CMD_HELP2 = "-help";
static const char *CMD_HELP3 = "-?";
#ifndef WIN32
static const char *CMD_DAEMON = "-daemon";
static const char *CMD_USER = "-user";
static const char *CMD_SOCKOWNER = "-sockowner";
static const char *CMD_SOCKGROUP = "-sockgroup";
static const char *CMD_SOCKPERMS = "-sockperms";
static const char *CMD_PIDFILE = "-pidfile";
static const char *CMD_KILLABLE = "-killable";
#endif
static const char *CMD_SERVER = "-listen";
static const char *CMD_SESSIONS = "-sessions";
static const char *CMD_PROGRESS = "-progress";

static int err_noarg(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, -%s requires an argument\n", arg);
	usage();
	return 1;
}
static int err_badrange(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, -%s given an invalid argument\n", arg);
	usage();
	return 1;
}
static int err_badswitch(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, \"%s\" not recognised\n", arg);
	usage();
	return 1;
}

#define ARG_INC {argc--;argv++;}
#define ARG_CHECK(a) \
	if(argc < 2) \
		return err_noarg(a); \
	ARG_INC

static int got_signal = 0;

int main(int argc, char *argv[])
{
	int sessions_set = 0;
	unsigned int sessions = 0;
	const char *server = def_server;
	unsigned long progress = def_progress;
#ifndef WIN32
	int daemon_mode = 0;
	int killable = 0;
	const char *pidfile = def_pidfile;
	const char *user = def_user;
	const char *sockowner = def_sockowner;
	const char *sockgroup = def_sockgroup;
	const char *sockperms = def_sockperms;
#endif

	ARG_INC;
	while(argc > 0) {
		if((strcmp(*argv, CMD_VERS1) == 0) ||
				(strcmp(*argv, CMD_VERS2) == 0))
			return nsc_server_version();
		if((strcmp(*argv, CMD_HELP1) == 0) ||
				(strcmp(*argv, CMD_HELP2) == 0) ||
				(strcmp(*argv, CMD_HELP3) == 0))
			return usage();
#ifndef WIN32
		if(strcmp(*argv, CMD_DAEMON) == 0)
			daemon_mode = 1;
		else if(strcmp(*argv, CMD_KILLABLE) == 0) {
			killable = 1;
		} else if(strcmp(*argv, CMD_PIDFILE) == 0) {
			ARG_CHECK(CMD_PIDFILE);
			pidfile = *argv;
		} else if(strcmp(*argv, CMD_USER) == 0) {
			ARG_CHECK(CMD_USER);
			user = *argv;
		} else if(strcmp(*argv, CMD_SOCKOWNER) == 0) {
			ARG_CHECK(CMD_SOCKOWNER);
			sockowner = *argv;
		} else if(strcmp(*argv, CMD_SOCKGROUP) == 0) {
			ARG_CHECK(CMD_SOCKGROUP);
			sockgroup = *argv;
		} else if(strcmp(*argv, CMD_SOCKPERMS) == 0) {
			ARG_CHECK(CMD_SOCKPERMS);
			sockperms = *argv;
		} else
#endif
		if(strcmp(*argv, CMD_SERVER) == 0) {
			ARG_CHECK(CMD_SERVER);
			server = *argv;
		} else if(strcmp(*argv, CMD_SESSIONS) == 0) {
			ARG_CHECK(CMD_SESSIONS);
			sessions = (unsigned int)atoi(*argv);
			sessions_set = 1;
		} else if(strcmp(*argv, CMD_PROGRESS) == 0) {
			ARG_CHECK(CMD_PROGRESS);
			progress = (unsigned long)atoi(*argv);
			if(progress > MAX_PROGRESS)
				return err_badrange(CMD_PROGRESS);
		} else
			return err_badswitch(*argv);
		ARG_INC;
	}

	if(!server) {
		SYS_fprintf(SYS_stderr, "Error, must provide -listen\n");
		return 1;
	}
	if(!sessions_set)
		sessions = def_sessions;
	if((sessions < 1) || (sessions > MAX_SESSIONS))
		return err_badrange(CMD_SESSIONS);
	if(!SYS_sigpipe_ignore()) {
#if SYS_DEBUG_LEVEL > 0
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGPIPE\n");
#endif
		return 1;
	}
	if(!SYS_sigusr_interrupt(&got_signal)) {
#if SYS_DEBUG_LEVEL > 0
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGUSR[1|2]\n");
#endif
		return 1;
	}
	return do_server(server, sessions, progress, daemon_mode, pidfile,
			killable, user, sockowner, sockgroup, sockperms);
}

static int do_server(const char *address, unsigned int max_sessions,
			unsigned long progress, int daemon_mode,
			const char *pidfile, int killable, const char *user,
			const char *sockowner, const char *sockgroup,
			const char *sockperms)
{
	int res, ret = 1;
	struct timeval now, last_now;
	unsigned int total = 0, tmp_total;
	unsigned long ops = 0, tmp_ops;
	NEURAL_CONNECTION *conn = NEURAL_CONNECTION_new();
	NEURAL_ADDRESS *addr = NEURAL_ADDRESS_new();
	NEURAL_SELECTOR *sel = NEURAL_SELECTOR_new();
	NEURAL_LISTENER *listener = NEURAL_LISTENER_new();
	NSC_SERVER *server = NULL;

	if(!NSC_SERVER_set_default_cache() ||
			((server = NSC_SERVER_new(max_sessions)) == NULL) ||
			!conn || !addr || !sel || !listener) {
		SYS_fprintf(SYS_stderr, "Error, malloc/initialisation failure\n");
		goto err;
	}
	if(!NEURAL_ADDRESS_create(addr, address, SERVER_BUFFER_SIZE) ||
			!NEURAL_ADDRESS_can_listen(addr) ||
			!NEURAL_LISTENER_create(listener, addr)) {
		SYS_fprintf(SYS_stderr, "Error, can't listen on '%s'\n",
				address);
		goto err;
	}
#ifndef WIN32
	if((sockowner || sockgroup) && !NEURAL_LISTENER_set_fs_owner(listener,
						sockowner, sockgroup))
		SYS_fprintf(SYS_stderr, "Warning, can't set socket ownership "
			"to user '%s' and group '%s', continuing anyway\n",
			sockowner ? sockowner : "(null)",
			sockgroup ? sockgroup : "(null)");
	if(sockperms && !NEURAL_LISTENER_set_fs_perms(listener, sockperms))
		SYS_fprintf(SYS_stderr, "Warning, can't set socket permissions "
				"to '%s', continuing anyway\n", sockperms);
	if(daemon_mode) {
		if(!SYS_daemon(0)) {
			SYS_fprintf(SYS_stderr, "Error, couldn't detach!\n");
			return 1;
		}
	}
	if(pidfile) {
		FILE *fp = fopen(pidfile, "w");
		if(!fp) {
			SYS_fprintf(SYS_stderr, "Error, couldn't open 'pidfile' "
					"at '%s'.\n", pidfile);
			return 1;
		}
		SYS_fprintf(fp, "%lu", (unsigned long)SYS_getpid());
		fclose(fp);
	}
	if(user) {
		if(!SYS_setuid(user)) {
			SYS_fprintf(SYS_stderr, "Error, couldn't become user "
				    "'%s'.\n", user);
			return 1;
		}
	}
#endif
	if(!NEURAL_LISTENER_add_to_selector(listener, sel)) {
		SYS_fprintf(SYS_stderr, "Error, selector problem\n");
		return 1;
	}
	SYS_gettime(&last_now);
network_loop:
	if(NEURAL_LISTENER_finished(listener)) {
		if(NSC_SERVER_clients_empty(server)) {
			ret = 0;
			goto err;
		}
	}
	if(!killable || !got_signal)
		res = NEURAL_SELECTOR_select(sel, 500000, 1);
	else
		res = -1;
	if(res < 0) {
		if(!killable)
			goto network_loop;
		if(got_signal)
			ret = 0;
		else if(errno == EINTR) {
			SYS_fprintf(SYS_stderr, "Error, select interrupted for unknown "
					"signal, continuing\n");
			goto network_loop;
		} else
			SYS_fprintf(SYS_stderr, "Error, select() failed\n");
		goto err;
	}
	SYS_gettime(&now);
	tmp_ops = NSC_SERVER_num_operations(server);
	if(SYS_msecs_between(&last_now, &now) < 1000) {
		if(!progress || ((tmp_ops / progress) == (ops / progress)))
			goto skip_totals;
	}
	tmp_total = NSC_SERVER_items_stored(server, &now);
	if((tmp_total == total) && (!progress ||
			((tmp_ops / progress) == (ops / progress)))) {
		if(res <= 0)
			goto network_loop;
		goto skip_totals;
	}
	SYS_fprintf(SYS_stderr, "Info, total operations = %7lu  (+ %5lu), "
		"total sessions = %5u  (%c%3u)\n", tmp_ops, tmp_ops - ops,
		tmp_total, (tmp_total > total ? '+' :
			(tmp_total == total ? '=' : '-')),
		(tmp_total > total ? tmp_total - total : total - tmp_total));
	SYS_timecpy(&last_now, &now);
	total = tmp_total;
	ops = tmp_ops;
skip_totals:
	if(!NSC_SERVER_clients_io(server, &now)) {
		SYS_fprintf(SYS_stderr, "Error, I/O failed\n");
		goto err;
	}
	while(!NEURAL_LISTENER_finished(listener) &&
			NEURAL_CONNECTION_accept(conn, listener)) {
		if(!NEURAL_CONNECTION_add_to_selector(conn, sel) ||
				!NSC_SERVER_new_client(server, conn,
					NSC_CLIENT_FLAG_IN_SERVER)) {
			SYS_fprintf(SYS_stderr, "Error, accept couldn't be handled\n");
			goto err;
		}
		if((conn = NEURAL_CONNECTION_new()) == NULL) goto err;
	}
	goto network_loop;
err:
	if(addr) NEURAL_ADDRESS_free(addr);
	if(conn) NEURAL_CONNECTION_free(conn);
	if(listener) NEURAL_LISTENER_free(listener);
	if(server)
		NSC_SERVER_free(server);
	if(sel) NEURAL_SELECTOR_free(sel);
	return killable;
}

