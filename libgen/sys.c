/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_LOCAL

#include <libgen/pre.h>
#define IN_SYS_C
#include <libgen/post.h>

#ifdef WIN32

int sockets_init(void)
{
	WORD wVersionRequested;
	WSADATA wsaData;

	wVersionRequested = MAKEWORD(2, 2);
	if(WSAStartup(wVersionRequested, &wsaData) != 0)
		return 0;
	return 1;
}

#else

#if 0
pid_t SYS_getpid(void)
{
	return getpid();
}
#endif

int SYS_daemon(int nochdir)
{
#ifdef HAVE_DAEMON
       if(daemon(nochdir, 0) == -1)
	       return 0;
       return 1;
#else
	pid_t pid;

	if ( (pid = fork()) < 0)
		return 0;
	else if (pid != 0)
		exit(0);
	setsid();

	if (!nochdir)
		chdir("/");

	umask(0);

	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	return 1;
#endif
}

int SYS_setuid(const char *username)
{
#if defined(HAVE_GETPWNAM) && defined(HAVE_SETUID)
	struct passwd *p = getpwnam(username);
	if(!p || (setuid(p->pw_uid) != 0))
		return 0;
	return 1;
#else
	return 0;
#endif
}

static int *gb_ptr = NULL;

static void empty_handler(int foo)
{
	if(gb_ptr) *gb_ptr = 1;
}

static int int_sig_set(int signum, void (*handler)(int))
{
	struct sigaction sig;

	sig.sa_handler = handler;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags = 0;
	if(sigaction(signum, &sig, NULL) != 0)
		return 0;
	return 1;
}

int SYS_sigpipe_ignore(void)
{
	if(!int_sig_set(SIGPIPE, SIG_IGN)) {
#if SYS_DEBUG_LEVEL > 0
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGPIPE\n\n");
#endif
		return 0;
	}
	return 1;
}

int SYS_sigusr_interrupt(int *ptr)
{
	gb_ptr = ptr;
	if(!int_sig_set(SIGUSR1, empty_handler) ||
			!int_sig_set(SIGUSR2, empty_handler)) {
#if SYS_DEBUG_LEVEL > 0
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGUSR1 or SIGUSR2\n\n");
#endif
		return 0;
	}
	return 1;
}

#endif /* !defined(WIN32) */

#if 0
int SYS_timecmp(const struct timeval *a, const struct timeval *b)
{
	if(a->tv_sec < b->tv_sec)
		return -1;
	else if(a->tv_sec > b->tv_sec)
		return 1;
	if(a->tv_usec < b->tv_usec)
		return -1;
	else if(a->tv_usec > b->tv_usec)
		return 1;
	return 0;
}

void SYS_gettime(struct timeval *tv)
{
#ifdef WIN32
	FILETIME decimillisecs;
	unsigned __int64 crud;
	GetSystemTimeAsFileTime(&decimillisecs);
	crud = ((unsigned __int64)decimillisecs.dwHighDateTime << 32) +
		(unsigned __int64)decimillisecs.dwLowDateTime;
	crud /= 10;
	crud -= (unsigned __int64)12614400000 * (unsigned __int64)1000000;
	tv->tv_sec = (long)(crud / 1000000);
	tv->tv_usec = (long)(crud % 1000000);
#else
	if(gettimeofday(tv, NULL) != 0)
		abort();
#endif
}

void SYS_timecpy(struct timeval *dest, const struct timeval *src)
{
	SYS_memcpy(struct timeval, dest, src);
}

void SYS_timeadd(struct timeval *res, const struct timeval *I,
		unsigned long msecs)
{
	unsigned long carry = I->tv_usec + (msecs * 1000);
	res->tv_usec = carry % 1000000;
	carry /= 1000000;
	res->tv_sec = I->tv_sec + carry;
}

#endif

int SYS_expirycheck(const struct timeval *timeitem, unsigned long msec_expiry,
		const struct timeval *timenow)
{
	struct timeval threshold;
	unsigned long usec_expiry = msec_expiry * 1000;
	SYS_memcpy(struct timeval, &threshold, timeitem);
	threshold.tv_sec = threshold.tv_sec + (usec_expiry / 1000000L);
	threshold.tv_usec += (usec_expiry % 1000000);
	if(threshold.tv_usec > 1000000) {
		threshold.tv_usec -= 1000000;
		threshold.tv_sec++;
	}
	if(timercmp(timenow, &threshold, <))
		return 0;
	return 1;
}

void SYS_timesub(struct timeval *res, const struct timeval *I,
		unsigned long msecs)
{
	unsigned long sub_low = (msecs % 1000) * 1000;
	unsigned long sub_high = msecs / 1000;
	if((unsigned long)I->tv_usec < sub_low) {
		sub_high++;
		res->tv_usec = (I->tv_usec + 1000000) - sub_low;
	} else
		res->tv_usec = I->tv_usec - sub_low;
	res->tv_sec = I->tv_sec - sub_high;
}

unsigned long SYS_msecs_between(const struct timeval *a, const struct timeval *b)
{
	unsigned long toret;
	const struct timeval *tmp;

	if(SYS_timecmp(a, b) > 0) {
		tmp = a;
		a = b;
		b = tmp;
	}
	toret = (unsigned long)1000000 * (b->tv_sec - a->tv_sec);
	if(b->tv_usec > a->tv_usec)
		toret += b->tv_usec - a->tv_usec;
	else
		toret -= a->tv_usec - b->tv_usec;
	return (toret / 1000);
}

