/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "private.h"

#include <errno.h>

#define CLIENT_BUFFER_SIZE	(sizeof(NSC_MSG) * 2)
#define MAX_RETRY_PERIOD	3600000
#define MIN_RETRY_PERIOD	1
#define MAX_IDLE_PERIOD		3600000

static const char *def_listen_addr = "UNIX:/tmp/scache";
static const unsigned long def_retry_period = 5000;
static const unsigned long def_idle_timeout = 0;
#ifndef WIN32
static const char *def_pidfile = NULL;
static const char *def_user = NULL;
static const char *def_sockowner = NULL;
static const char *def_sockgroup = NULL;
static const char *def_sockperms = NULL;
#endif

static int nsc_client_version(void)
{
	SYS_fprintf(SYS_stderr,
		"NeuroCache Client version %s (%s / %s)\n\n"
		"Distributed as part of the NeuroCache source package\n"
		"Copyright (C) 2019-2020 Hilman P. Alisabana, Jakarta.\n\n",
		HAVE_PACKAGE_VERSION, HAVE_HYSCMTERM_UUID, HAVE_HYSCMTERM_DATE);
}

static const char *usage_msg[] = {
"",
"Usage: nsc_client [options]      where 'options' are from;",
#ifndef WIN32
"  -daemon            (detach and run in the background)",
#endif
"  -listen <addr>     (listen on address 'addr', def: UNIX:/tmp/scache)",
"  -server <addr>     (connects to a cache server at 'addr')",
"  -connect <addr>    (alias for '-server')",
"  -retry <num>       (retry period (msecs) for cache servers, def: 5000)",
"  -idle <num>        (idle timeout (msecs) for client connections, def: 0)",
#ifndef WIN32
"  -user <user>       (run daemon as given user)",
"  -sockowner <user>  (controls ownership of unix domain listening socket)",
"  -sockgroup <group> (controls ownership of unix domain listening socket)",
"  -sockperms <oct>   (set permissions of unix domain listening socket)",
"  -pidfile <path>    (a file to store the process ID in)",
"  -killable          (exit cleanly on a SIGUSR1 or SIGUSR2 signal)",
#endif
"  -<h|help|?>        (display this usage message)",
"  -<v|version>       (display version information)",
"",
" Eg. nsc_client -listen UNIX:/tmp/scache -server IP:192.168.2.5:9003",
" will listen on a unix domain socket at /tmp/scache and will manage",
" forwarding requests and responses to and from the cache server.",
"", NULL};

static const char *CMD_VERS1 = "-v";
static const char *CMD_VERS2 = "-version";
static const char *CMD_HELP1 = "-h";
static const char *CMD_HELP2 = "-help";
static const char *CMD_HELP3 = "-?";
#ifndef WIN32
static const char *CMD_DAEMON = "-daemon";
static const char *CMD_USER = "-user";
static const char *CMD_SOCKOWNER = "-sockowner";
static const char *CMD_SOCKGROUP = "-sockgroup";
static const char *CMD_SOCKPERMS = "-sockperms";
static const char *CMD_PIDFILE = "-pidfile";
static const char *CMD_KILLABLE = "-killable";
#endif
static const char *CMD_LISTEN = "-listen";
static const char *CMD_SERVER1 = "-server";
static const char *CMD_SERVER2 = "-connect";
static const char *CMD_RETRY = "-retry";
static const char *CMD_IDLE = "-idle";

static int usage(void) {
	const char **u = usage_msg;
	while(*u)
		SYS_fprintf(SYS_stderr, "%s\n", *(u++));
	return 0;
}
static int err_noarg(const char *arg) {
	SYS_fprintf(SYS_stderr, "Error, %s requires an argument\n", arg);
	usage();
	return 1;
}
static int err_badarg(const char *arg) {
	SYS_fprintf(SYS_stderr, "Error, %s given an invalid argument\n", arg);
	usage();
	return 1;
}
static int err_badswitch(const char *arg) {
	SYS_fprintf(SYS_stderr, "Error, \"%s\" not recognised\n", arg);
	usage();
	return 1;
}

#define ARG_INC {argc--;argv++;}
#define ARG_CHECK(a) \
	if(argc < 2) \
		return err_noarg(a); \
	ARG_INC

static int got_signal = 0;

int main(int argc, char *argv[])
{
	int tmp_res, res = 1;
	NEURAL_ADDRESS *addr;
	NEURAL_SELECTOR *sel;
	NEURAL_LISTENER *listener;
	NEURAL_CONNECTION *conn = NULL;
	unsigned long timeout;
	struct timeval now;
	server_t *server;
	clients_t *clients;
	multiplexer_t *multiplexer;
	const char *server_address = NULL;
#ifndef WIN32
	int daemon_mode = 0;
	int killable = 0;
	const char *pidfile = def_pidfile;
	const char *user = def_user;
	const char *sockowner = def_sockowner;
	const char *sockgroup = def_sockgroup;
	const char *sockperms = def_sockperms;
#endif
	const char *listen_addr = def_listen_addr;
	unsigned long retry_period = def_retry_period;
	unsigned long idle_timeout = def_idle_timeout;

	ARG_INC;
	while(argc > 0) {
		if((strcmp(*argv, CMD_VERS1) == 0) ||
				(strcmp(*argv, CMD_VERS2) == 0))
			return nsc_client_version();
		if((strcmp(*argv, CMD_HELP1) == 0) ||
				(strcmp(*argv, CMD_HELP2) == 0) ||
				(strcmp(*argv, CMD_HELP3) == 0))
			return usage();
#ifndef WIN32
		if(strcmp(*argv, CMD_DAEMON) == 0)
			daemon_mode = 1;
		else if(strcmp(*argv, CMD_KILLABLE) == 0) {
			killable = 1;
		} else if(strcmp(*argv, CMD_PIDFILE) == 0) {
			ARG_CHECK(CMD_PIDFILE);
			pidfile = *argv;
		} else if(strcmp(*argv, CMD_USER) == 0) {
			ARG_CHECK(CMD_USER);
			user = *argv;
		} else if(strcmp(*argv, CMD_SOCKOWNER) == 0) {
			ARG_CHECK(CMD_SOCKOWNER);
			sockowner = *argv;
		} else if(strcmp(*argv, CMD_SOCKGROUP) == 0) {
			ARG_CHECK(CMD_SOCKGROUP);
			sockgroup = *argv;
		} else if(strcmp(*argv, CMD_SOCKPERMS) == 0) {
			ARG_CHECK(CMD_SOCKPERMS);
			sockperms = *argv;
		} else
#endif
		if(strcmp(*argv, CMD_LISTEN) == 0) {
			ARG_CHECK(CMD_LISTEN);
			listen_addr = *argv;
		} else if((strcmp(*argv, CMD_SERVER1) == 0) ||
				(strcmp(*argv, CMD_SERVER2) == 0)) {
			ARG_CHECK(*argv);
			if(server_address) {
				SYS_fprintf(SYS_stderr, "Error, too many servers\n");
				return err_badarg(*(argv - 1));
			}
			server_address = *argv;
		} else if(strcmp(*argv, CMD_RETRY) == 0) {
			char *tmp_ptr;
			ARG_CHECK(*argv);
			retry_period = strtoul(*argv, &tmp_ptr, 10);
			if((tmp_ptr == *argv) || (*tmp_ptr != '\0') ||
					(retry_period < MIN_RETRY_PERIOD) ||
					(retry_period > MAX_RETRY_PERIOD)) {
				return err_badarg(*(argv - 1));
			}
		} else if(strcmp(*argv, CMD_IDLE) == 0) {
			char *tmp_ptr;
			ARG_CHECK(*argv);
			idle_timeout = strtoul(*argv, &tmp_ptr, 10);
			if((tmp_ptr == *argv) || (*tmp_ptr != '\0') ||
					(idle_timeout > MAX_IDLE_PERIOD)) {
				return err_badarg(*(argv - 1));
			}
		} else
			return err_badswitch(*argv);
		ARG_INC;
	}

	if(!server_address) {
		SYS_fprintf(SYS_stderr, "Error, no server specified!\n");
		return 1;
	}
#ifdef WIN32
	if(!sockets_init()) {
		SYS_fprintf(SYS_stderr, "Error, couldn't initialise socket layer\n");
		return 1;
	}
#else
	if(!SYS_sigpipe_ignore()) {
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGPIPE\n");
		return 1;
	}
	if(!SYS_sigusr_interrupt(&got_signal)) {
#if SYS_DEBUG_LEVEL > 0
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGUSR[1|2]\n");
#endif
		return 1;
	}
#endif

	if(((addr = NEURAL_ADDRESS_new()) == NULL) ||
			!NEURAL_ADDRESS_create(addr, listen_addr,
				CLIENT_BUFFER_SIZE) ||
			!NEURAL_ADDRESS_can_listen(addr) ||
			((listener = NEURAL_LISTENER_new()) == NULL) ||
			!NEURAL_LISTENER_create(listener, addr)) {
		SYS_fprintf(SYS_stderr, "Error, bad listen address\n");
		return 1;
	}
	NEURAL_ADDRESS_free(addr);
	if((sel = NEURAL_SELECTOR_new()) == NULL) {
		SYS_fprintf(SYS_stderr, "Error, malloc problem\n");
		return 1;
	}

#ifndef WIN32
	if((sockowner || sockgroup) && !NEURAL_LISTENER_set_fs_owner(listener,
						sockowner, sockgroup))
		SYS_fprintf(SYS_stderr, "Warning, can't set socket ownership "
			"to user '%s' and group '%s', continuing anyway\n",
			sockowner ? sockowner : "(null)",
			sockgroup ? sockgroup : "(null)");
	if(sockperms && !NEURAL_LISTENER_set_fs_perms(listener, sockperms))
		SYS_fprintf(SYS_stderr, "Warning, can't set socket permissions "
				"to '%s', continuing anyway\n", sockperms);
#endif
	SYS_gettime(&now);
	if(((server = server_new(server_address, retry_period, &now)) == NULL) ||
			((clients = clients_new()) == NULL) ||
			((multiplexer = multiplexer_new()) == NULL)) {
		SYS_fprintf(SYS_stderr, "Error, internal initialisation problems\n");
		return 1;
	}

#ifndef WIN32
	if(daemon_mode) {
		if(!SYS_daemon(0)) {
			SYS_fprintf(SYS_stderr, "Error, couldn't detach!\n");
			return 1;
		}
	}
	if(pidfile) {
		FILE *fp = fopen(pidfile, "w");
		if(!fp) {
			SYS_fprintf(SYS_stderr, "Error, couldn't open 'pidfile' "
					"at '%s'.\n", pidfile);
			return 1;
		}
		SYS_fprintf(fp, "%lu", (unsigned long)SYS_getpid());
		fclose(fp);
	}
	if(user) {
		if(!SYS_setuid(user)) {
			SYS_fprintf(SYS_stderr, "Error, couldn't become user "
				    "'%s'.\n", user);
			return 1;
		}
	}
#endif

	timeout = retry_period * 333;
	if(timeout < 20000)
		timeout = 20000;

	if((conn = NEURAL_CONNECTION_new()) == NULL) {
		SYS_fprintf(SYS_stderr, "Error, connection couldn't be created!!\n");
		goto err;
	}
	if(!NEURAL_LISTENER_add_to_selector(listener, sel))
		goto err;
main_loop:
	if(!server_selector_hook(server, sel, &now)) {
		SYS_fprintf(SYS_stderr, "Error, selector problem\n");
		goto err;
	}
	if(NEURAL_LISTENER_finished(listener)) {
		NEURAL_LISTENER_del_from_selector(listener);
		if(clients_empty(clients))
			goto end;
	}
	if(!killable || !got_signal)
		tmp_res = NEURAL_SELECTOR_select(sel, timeout, 1);
	else
		tmp_res = -1;
	if(tmp_res < 0) {
		if(!killable)
			goto main_loop;
		if(got_signal)
			goto end;
		if(errno != EINTR) {
			SYS_fprintf(SYS_stderr, "Error, select interrupted for unknown "
					"signal, continuing\n");
			goto main_loop;
		}
		SYS_fprintf(SYS_stderr, "Error, select() failed\n");
		goto err;
	}
	SYS_gettime(&now);
	while(!NEURAL_LISTENER_finished(listener) &&
			NEURAL_CONNECTION_accept(conn, listener)) {
		if(!NEURAL_CONNECTION_add_to_selector(conn, sel) ||
				!clients_new_client(clients, conn, &now)) {
			SYS_fprintf(SYS_stderr, "Error, couldn't add in new "
				"client connection - dropping it.\n");
			NEURAL_CONNECTION_free(conn);
		}
		if((conn = NEURAL_CONNECTION_new()) == NULL) {
			SYS_fprintf(SYS_stderr, "Error, connection couldn't be created!!\n");
			goto err;
		}
	}
	if(!clients_io(clients, multiplexer, &now, idle_timeout) ||
			!server_io(server, multiplexer, clients, &now)) {
		SYS_fprintf(SYS_stderr, "Error, a fatal problem with the "
			"client or server code occured. Closing.\n");
		goto err;
	}
	if(!multiplexer_run(multiplexer, clients, server, &now)) {
		SYS_fprintf(SYS_stderr, "Error, a fatal problem with the "
			"multiplexer has occured. Closing.\n");
		goto err;
	}
	goto main_loop;
end:
	res = 0;
err:
	if(conn)
		NEURAL_CONNECTION_free(conn);
	NEURAL_LISTENER_free(listener);
	clients_free(clients);
	server_free(server);
	multiplexer_free(multiplexer);
	NEURAL_SELECTOR_free(sel);
	return res;
}

