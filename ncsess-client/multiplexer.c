/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "private.h"

#include <assert.h>

#define MULTIPLEXER_MAX_ITEMS 512

typedef struct st_item_t {
	unsigned long m_uid;
	unsigned long c_uid;
	unsigned long s_uid;
	enum {
		ITEM_NORMAL,
		ITEM_CLIENT_DEAD,
		ITEM_SERVER_DEAD
	} state;
} item_t;

struct st_multiplexer_t {
	item_t items[MULTIPLEXER_MAX_ITEMS];
	unsigned int used;
	unsigned long uid_seed;
};

static void int_remove(multiplexer_t *m, unsigned int idx)
{
	assert(idx < m->used);
	if(idx + 1 < m->used)
		SYS_memmove_n(item_t, m->items + idx, m->items + (idx + 1),
				m->used - (idx + 1));
	m->used--;
}

multiplexer_t *multiplexer_new(void)
{
	multiplexer_t *m = SYS_malloc(multiplexer_t, 1);
	if(!m)
		return NULL;
	m->used = 0;
	m->uid_seed = 1;
	return m;
}

void multiplexer_free(multiplexer_t *m)
{
	SYS_free(multiplexer_t, m);
}

int multiplexer_run(multiplexer_t *m, clients_t *c, server_t *s,
			const struct timeval *now)
{
	if(server_is_active(s) && !server_to_clients(s, c, m, now))
		return 0;
	if(!clients_to_server(c, s, m, now))
		return 0;
	return 1;
}

void multiplexer_mark_dead_client(multiplexer_t *m, unsigned long client_uid)
{
	unsigned int loop = 0;
	item_t *item = m->items;
	while(loop < m->used) {
		if(item->c_uid == client_uid) {
			if(item->state == ITEM_SERVER_DEAD)
				int_remove(m, loop);
			else {
				abort();
				item->state = ITEM_CLIENT_DEAD;
				loop++;
				item++;
			}
		} else {
			loop++;
			item++;
		}
	}
}

void multiplexer_mark_dead_server(multiplexer_t *m, unsigned long server_uid,
			clients_t *c)
{
	unsigned int loop = 0;
	item_t *item = m->items;
	while(loop < m->used) {
		if(item->s_uid == server_uid) {
			if(item->state != ITEM_CLIENT_DEAD)
				clients_digest_error(c, item->c_uid);
			int_remove(m, loop);
		} else {
			loop++;
			item++;
		}
	}
}

int multiplexer_has_space(multiplexer_t *m)
{
	return (m->used < MULTIPLEXER_MAX_ITEMS);
}

unsigned long multiplexer_add(multiplexer_t *m, unsigned long client_uid,
			unsigned long server_uid)
{
	item_t *item = m->items + m->used;

	assert(m->used < MULTIPLEXER_MAX_ITEMS);
	item->m_uid = m->uid_seed++;
	item->c_uid = client_uid;
	item->s_uid = server_uid;
	item->state = ITEM_NORMAL;
	m->used++;
	return item->m_uid;
}

void multiplexer_delete_item(multiplexer_t *m, unsigned long m_uid)
{
	unsigned int loop = m->used;
	item_t *item = m->items + loop;
	while(loop--) {
		item--;
		if(item->m_uid == m_uid) {
			int_remove(m, loop);
			return;
		}
	}
	assert(NULL == "shouldn't happen!");
}

void multiplexer_finish(multiplexer_t *m, clients_t *c, unsigned long uid,
			NSC_CMD cmd, const unsigned char *data,
			unsigned int data_len)
{
	unsigned int loop = 0;
	item_t *item = m->items;
	while(loop < m->used) {
		if(item->m_uid == uid)
			goto found;
		loop++;
		item++;
	}
	assert(NULL == "shouldn't happen!");
	return;
found:
	if(item->state != ITEM_CLIENT_DEAD)
		clients_digest_response(c, item->c_uid, cmd, data, data_len);
	int_remove(m, loop);
}

