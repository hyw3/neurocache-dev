/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "private.h"

#include <assert.h>

#define CLIENTS_MAX_ITEMS	1024
#define CLIENTS_MAX_RETRIES	5

typedef struct st_client_ctx {
	unsigned long uid;
	NSC_PLUG *plug;
	int request_open;
	unsigned long request_uid;
	NSC_CMD request_cmd;
	const unsigned char *request_data;
	unsigned int request_len;
	int response_done;
	unsigned long multiplex_id;
	struct timeval timestamp;
} client_ctx;

struct st_clients_t {
	client_ctx *items[CLIENTS_MAX_ITEMS];
	unsigned int used;
	unsigned long uid_seed;
	unsigned int priorities[CLIENTS_MAX_ITEMS];
};

static void client_ctx_flush(client_ctx *ctx)
{
	assert(ctx->plug != NULL);
restart:
	if(!ctx->request_open) {
		ctx->request_open = NSC_PLUG_read(ctx->plug, 0,
				&ctx->request_uid,
				&ctx->request_cmd,
				&ctx->request_data,
				&ctx->request_len);
		if(!ctx->request_open)
			return;
		ctx->multiplex_id = 0;
		ctx->response_done = 0;
		if(!NSC_PLUG_write(ctx->plug, 0, ctx->request_uid,
					ctx->request_cmd, NULL, 0)) {
			assert(NULL == "shouldn't happen");
			NSC_PLUG_consume(ctx->plug);
			ctx->request_open = 0;
		}
		return;
	}
	if(!ctx->response_done)
		return;
	if(!NSC_PLUG_commit(ctx->plug))
		return;
	if(!NSC_PLUG_consume(ctx->plug)) {
		assert(NULL == "shouldn't happen");
		return;
	}
	ctx->response_done = 0;
	ctx->request_open = 0;
	goto restart;
}

static client_ctx *client_ctx_new(unsigned long uid, NEURAL_CONNECTION *conn,
				const struct timeval *now)
{
	client_ctx *c = SYS_malloc(client_ctx, 1);
	if(!c)
		return NULL;
	c->uid = uid;
	c->request_open = 0;
	c->response_done = 0;
	c->multiplex_id = 0;
	SYS_timecpy(&c->timestamp, now);
	c->plug = NSC_PLUG_new(conn, 0);
	if(!c->plug) {
		SYS_free(client_ctx, c);
		return NULL;
	}
	return c;
}

static void client_ctx_free(client_ctx *c)
{
	NSC_PLUG_free(c->plug);
	SYS_free(client_ctx, c);
}

static int client_ctx_io(client_ctx *c)
{
	assert(c->plug != NULL);
	if(!NSC_PLUG_io(c->plug))
		return 0;
	client_ctx_flush(c);
	return 1;
}

static void client_ctx_digest_response(client_ctx *ctx,
			NSC_CMD cmd,
			const unsigned char *data,
			unsigned int data_len)
{
	assert(ctx->response_done == 0);
	assert(ctx->request_open != 0);
	assert(ctx->plug != NULL);
	assert(ctx->request_cmd == cmd);
	if(data_len)
		NSC_PLUG_write_more(ctx->plug, data, data_len);
	ctx->response_done = 1;
	client_ctx_flush(ctx);
}

static int client_ctx_should_timeout(client_ctx *c, unsigned long idle_timeout,
			const struct timeval *now)
{
	if(c->request_open)
		return 0;
	return SYS_expirycheck(&c->timestamp, idle_timeout, now);
}

static void priority_removed(clients_t *c, unsigned int idx)
{
	unsigned int *p_iterate = c->priorities;
	unsigned int iterate = 0;
	int convertedfound = 0;
	unsigned int converted = c->priorities[idx];

	assert(c->used >= idx);
	while(iterate < c->used) {
		if(convertedfound) {
just_found:
			p_iterate[0] = p_iterate[1];
			assert(p_iterate[0] != converted);
			if(p_iterate[0] > converted)
				p_iterate[0]--;
		} else {
			if(p_iterate[0] == converted) {
				convertedfound = 1;
				goto just_found;
			} else if(p_iterate[0] > converted)
				p_iterate[0]--;
		}
		iterate++;
		p_iterate++;
	}
	assert(convertedfound || (*p_iterate == converted));
}

static void priority_totail(clients_t *c, unsigned int pre_index)
{
	if(pre_index + 1 < c->used) {
		unsigned int tmp = c->priorities[pre_index];
		SYS_memmove_n(unsigned int, c->priorities + pre_index,
				c->priorities + (pre_index + 1),
				c->used - (pre_index + 1));
		c->priorities[c->used - 1] = tmp;
	}
}

static int int_find(clients_t *c, unsigned long client_uid, unsigned int *pos)
{
	unsigned int idx = 0;
	client_ctx **item = c->items;
	while(idx < c->used) {
		if((*item)->uid == client_uid) {
			*pos = idx;
			return 1;
		}
		if((*item)->uid > client_uid)
			return 0;
		idx++;
		item++;
	}
	return 0;
}

clients_t *clients_new(void)
{
	clients_t *c = SYS_malloc(clients_t, 1);
	if(!c)
		return NULL;
	c->used = 0;
	c->uid_seed = 1;
	return c;
}

void clients_free(clients_t *c)
{
	while(c->used > 0)
		client_ctx_free(c->items[--c->used]);
	SYS_free(clients_t, c);
}

int clients_empty(const clients_t *c)
{
	return !c->used;
}

static void clients_delete(clients_t *c, unsigned int idx, multiplexer_t *m)
{
	client_ctx **ctx = c->items + idx;

#ifdef CLIENTS_PRINT_CONNECTS
	SYS_fprintf(SYS_stderr, "Info: dead client connection (%u)\n", idx);
#endif
	multiplexer_mark_dead_client(m, (*ctx)->uid);
	client_ctx_free(*ctx);
	if(idx + 1 < c->used)
		SYS_memmove_n(client_ctx *, ctx, (const client_ctx **)ctx + 1,
				c->used - (idx + 1));
	c->used--;
	priority_removed(c, idx);
}

int clients_io(clients_t *c, multiplexer_t *m, const struct timeval *now,
			unsigned long idle_timeout)
{
	unsigned int pos = 0;
	while(pos < c->used) {
		if(!client_ctx_io(c->items[pos]) || (idle_timeout &&
				client_ctx_should_timeout(c->items[pos],
					idle_timeout, now)))
			clients_delete(c, pos, m);
		else
			pos++;
	}
	return 1;
}

int clients_new_client(clients_t *c, NEURAL_CONNECTION *conn,
			const struct timeval *now)
{
	client_ctx **item = c->items + c->used;

	if(c->used >= CLIENTS_MAX_ITEMS) {
		SYS_fprintf(SYS_stderr, "Error, rejected new client connection "
				"already at maximum (%u)\n", CLIENTS_MAX_ITEMS);
		return 0;
	}
	*item = client_ctx_new(c->uid_seed, conn, now);
	if(*item == NULL) {
		SYS_fprintf(SYS_stderr, "Error, initialisation of new client "
				"connection failed\n");
		return 0;
	}
	c->uid_seed++;
	if(c->uid_seed == 0)
		c->uid_seed = 1;
	c->priorities[c->used] = c->used;
	c->used++;
#ifdef CLIENTS_PRINT_CONNECTS
	SYS_fprintf(SYS_stderr, "Info: new client connection (%u)\n", c->used);
#endif
	return 1;
}

int clients_to_server(clients_t *c, server_t *s, multiplexer_t *m,
			const struct timeval *now)
{
	unsigned int edge_l = 0;
	while(edge_l < c->used) {
		unsigned long m_uid;
		unsigned int client_idx;
		client_ctx *ctx;
restart_loop:
		client_idx = c->priorities[edge_l];
		ctx = c->items[client_idx];
		if(!multiplexer_has_space(m))
			return 1;
		if(!ctx->request_open || ctx->multiplex_id)
			goto skip;
		if(!server_is_active(s)) {
			client_ctx_digest_response(ctx, ctx->request_cmd, NULL, 0);
			goto responded_locally;
		}
		m_uid = multiplexer_add(m, ctx->uid, server_get_uid(s));
		if(!server_place_request(s, m_uid, ctx->request_cmd,
				ctx->request_data, ctx->request_len)) {
			multiplexer_delete_item(m, m_uid);
			return 1;
		}
		ctx->multiplex_id = m_uid;
		SYS_timecpy(&ctx->timestamp, now);
responded_locally:
		priority_totail(c, edge_l);
		goto restart_loop;
skip:
		edge_l++;
	}
	return 1;
}

void clients_digest_response(clients_t *c, unsigned long client_uid,
				NSC_CMD cmd,
				const unsigned char *data,
				unsigned int data_len)
{
	unsigned int idx;
	if(!int_find(c, client_uid, &idx)) {
		assert(NULL == "shouldn't happen!");
		return;
	}
	client_ctx_digest_response(c->items[idx], cmd, data, data_len);
}

void clients_digest_error(clients_t *c, unsigned long client_uid)
{
	static const unsigned char errbyte = NSC_ERR_DISCONNECTED;
	client_ctx *ctx;
	unsigned int idx;
	if(!int_find(c, client_uid, &idx)) {
		assert(NULL == "shouldn't happen!");
		return;
	}
	ctx = c->items[idx];
	client_ctx_digest_response(ctx, ctx->request_cmd, &errbyte, 1);
}
