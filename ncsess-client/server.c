/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "private.h"

#include <assert.h>

#define SERVER_BUFFER_SIZE	(sizeof(NSC_MSG) * 8)

static unsigned long uid_seed = 1;

struct st_server_t {
	unsigned long uid;
	NSC_PLUG *plug;
	NEURAL_ADDRESS *address;
	struct timeval last_fail;
	unsigned long retry_msecs;
};

static int server_retry_util(server_t *s, const struct timeval *now)
{
	NEURAL_CONNECTION *conn;
	if(s->plug) return 0;
	if(!SYS_expirycheck(&s->last_fail, s->retry_msecs, now))
		return 0;
	conn = NEURAL_CONNECTION_new();
	if(!conn) return 0;
	SYS_timecpy(&s->last_fail, now);
	if(!NEURAL_CONNECTION_create(conn, s->address) ||
			((s->plug = NSC_PLUG_new(conn,
				NSC_PLUG_FLAG_TO_SERVER)) == NULL)) {
		NEURAL_CONNECTION_free(conn);
		return 0;
	}
	s->uid = uid_seed++;
	return 1;
}

static void server_dead_util(server_t *s, multiplexer_t *m, clients_t *c,
			const struct timeval *now)
{
	NSC_PLUG_free(s->plug);
	s->plug = NULL;
	SYS_timecpy(&s->last_fail, now);
	multiplexer_mark_dead_server(m, s->uid, c);
}

int server_is_active(server_t *s)
{
	return (s->plug ? 1 : 0);
}

unsigned long server_get_uid(server_t *s)
{
	return s->uid;
}

server_t *server_new(const char *address, unsigned long retry_msecs,
			const struct timeval *now)
{
	server_t *s = NULL;
	NEURAL_ADDRESS *a = NEURAL_ADDRESS_new();

	if(!a || !NEURAL_ADDRESS_create(a, address, SERVER_BUFFER_SIZE) ||
			!NEURAL_ADDRESS_can_connect(a))
		goto err;
	s = SYS_malloc(server_t, 1);
	if(!s)
		goto err;
	s->plug = NULL;
	s->address = a;
	s->retry_msecs = retry_msecs;
	SYS_timesub(&s->last_fail, now, retry_msecs + 1);
	return s;
err:
	if(a)
		NEURAL_ADDRESS_free(a);
	return NULL;
}

void server_free(server_t *s)
{
	NEURAL_ADDRESS_free(s->address);
	if(s->plug)
		NSC_PLUG_free(s->plug);
	SYS_free(server_t, s);
}

int server_selector_hook(server_t *s, NEURAL_SELECTOR *sel, const struct timeval *now)
{
	if(server_retry_util(s, now))
		return NSC_PLUG_to_select(s->plug, sel);
	return 1;
}

int server_io(server_t *s, multiplexer_t *m, clients_t *c,
			const struct timeval *now)
{
	if(server_is_active(s) && !NSC_PLUG_io(s->plug))
		server_dead_util(s, m, c, now);
	return 1;
}

int server_to_clients(server_t *s, clients_t *c, multiplexer_t *m,
			const struct timeval *now)
{
	unsigned long uid;
	NSC_CMD cmd;
	const unsigned char *data;
	unsigned int len;
	assert(server_is_active(s));
	while(NSC_PLUG_read(s->plug, 0, &uid, &cmd, &data, &len)) {
		multiplexer_finish(m, c, uid, cmd, data, len);
		NSC_PLUG_consume(s->plug);
	}
	return 1;
}

int server_place_request(server_t *s, unsigned long uid, NSC_CMD cmd,
			const unsigned char *data, unsigned int data_len)
{
	assert(server_is_active(s));
	if(!NSC_PLUG_write(s->plug, 0, uid, cmd, data, data_len))
		return 0;
	if(!NSC_PLUG_commit(s->plug)) {
		assert(NULL == "shouldn't happen!");
		NSC_PLUG_rollback(s->plug);
		return 0;
	}
	return 1;
}
