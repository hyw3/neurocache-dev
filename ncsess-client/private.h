/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_PRIVATE_SESSCLIENT_H
#define HEADER_PRIVATE_SESSCLIENT_H

#define SYS_GENERATING_EXE

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <neurocache/nsc_plug.h>
#include <neurocache/nsc_internal.h>
#include <libgen/post.h>

typedef struct st_clients_t	clients_t;
typedef struct st_server_t	server_t;
typedef struct st_multiplexer_t	multiplexer_t;

clients_t *clients_new(void);
void clients_free(clients_t *c);
int clients_empty(const clients_t *c);
int clients_io(clients_t *c, multiplexer_t *m, const struct timeval *now,
			unsigned long idle_timeout);
int clients_new_client(clients_t *c, NEURAL_CONNECTION *conn,
			const struct timeval *now);
int clients_to_server(clients_t *c, server_t *s, multiplexer_t *m,
			const struct timeval *now);
void clients_digest_response(clients_t *c, unsigned long client_uid,
				NSC_CMD cmd,
				const unsigned char *data,
				unsigned int data_len);
void clients_digest_error(clients_t *c, unsigned long client_uid);

server_t *server_new(const char *address, unsigned long retry_msecs,
			const struct timeval *now);
void server_free(server_t *s);
int server_selector_hook(server_t *s, NEURAL_SELECTOR *sel, const struct timeval *now);
int server_io(server_t *s, multiplexer_t *m, clients_t *c,
			const struct timeval *now);
int server_to_clients(server_t *s, clients_t *c, multiplexer_t *m,
			const struct timeval *now);
int server_place_request(server_t *s, unsigned long uid, NSC_CMD cmd,
			const unsigned char *data, unsigned int data_len);
int server_is_active(server_t *s);
unsigned long server_get_uid(server_t *s);

multiplexer_t *multiplexer_new(void);
void multiplexer_free(multiplexer_t *m);
int multiplexer_run(multiplexer_t *m, clients_t *c, server_t *s,
			const struct timeval *now);
void multiplexer_mark_dead_client(multiplexer_t *m, unsigned long client_uid);
void multiplexer_mark_dead_server(multiplexer_t *m, unsigned long server_uid,
			clients_t *c);
int multiplexer_has_space(multiplexer_t *m);
unsigned long multiplexer_add(multiplexer_t *m, unsigned long client_uid,
			unsigned long server_uid);
void multiplexer_delete_item(multiplexer_t *m, unsigned long m_uid);
void multiplexer_finish(multiplexer_t *m, clients_t *c, unsigned long uid,
			NSC_CMD cmd, const unsigned char *data,
			unsigned int data_len);

#endif /* !defined(HEADER_PRIVATE_SESSCLIENT_H) */
