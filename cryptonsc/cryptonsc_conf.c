/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "cryptonsc.h"
#include <assert.h>

static char *def_cacert = NULL;
static char *def_cert = NULL;
static cryptonsc_sslmeth def_sslmeth = CRYPTONSC_SSLMETH_NORMAL;
static unsigned int def_list_size = 5;
static unsigned int def_total_max = 0;
static unsigned int def_time_max = 0;
static unsigned int def_response_size = 8192;
static unsigned int def_response_expected = 20;
static const char *def_request_string = "GET /\r\n";
static const char *def_session_string = "s";
static unsigned int def_period_update = 0;
static unsigned int def_nologo = 0;
static const char *def_cipher_string = NULL;
static unsigned int def_output_sessions = 0;
#ifdef HAVE_ENGINE
static const char *def_engine_id = NULL;
#endif

IMPLEMENT_CMDS;

static int session_string_set(cryptonsc_config *sc, const char *val);
static int command_line_next_token(int *argc, const char ***argv,
			cmd_id_t *cmd_id, const char **cmd_val);

static int session_string_set(cryptonsc_config *sc, const char *val)
{
	unsigned int loop = 0, has_resumes = 0, val_len = strlen(val);

	for(loop = 0; loop < val_len; loop++) {
		switch(val[loop]) {
		case('r'):
			has_resumes = 1;
		case('s'):
			break;
		default:
			return 0;
		}
	}
	if(has_resumes) {
		sc->session_string = val;
		sc->session_string_length = val_len;
	} else
		sc->session_string = NULL;
	return 1;
}

static int command_line_next_token(int *argc, const char ***argv,
			cmd_id_t *cmd_id, const char **cmd_val)
{
	const cmd_defn *iterator = cmds;
	if(*argc <= 0)
		return 0;
	while(iterator->cmd_name && strcmp(iterator->cmd_name, **argv))
		iterator++;
	if(!iterator->cmd_name)
		return unknown_switch(**argv);
	(*argc)--;
	(*argv)++;
	assert(iterator->cmd_args < 2);
	if(*argc < (int)iterator->cmd_args) {
		SYS_fprintf(SYS_stderr,
			"Error, '%s' requires %u arguments (only %u "
			"supplied)\n", **argv, iterator->cmd_args, *argc);
		return 0;
	}
	*cmd_id = iterator->cmd_id;
	if(iterator->cmd_args) {
		*cmd_val = **argv;
		(*argv)++;
		*argc -= iterator->cmd_args;
	} else
		*cmd_val = NULL;
	return 1;
}

void cryptonsc_config_init(cryptonsc_config *sc)
{
	sc->cacert = def_cacert;
	sc->cert = def_cert;
	sc->sslmeth = def_sslmeth;
	sc->list_size = def_list_size;
	sc->total_max = def_total_max;
	sc->time_max = def_time_max;
	sc->response_size = def_response_size;
	sc->response_expected = def_response_expected;
	sc->request_string = def_request_string;
	sc->period_update = def_period_update;
	sc->nologo = def_nologo;
	sc->cipher_string = def_cipher_string;
	sc->output_sessions = def_output_sessions;
	sc->csv_output = NULL;
#ifdef HAVE_ENGINE
	sc->engine_id = def_engine_id;
#endif
	session_string_set(sc, def_session_string);
	sc->distribution = neuraldist_new();
	assert(sc->distribution);
}

void cryptonsc_config_finish(cryptonsc_config *sc)
{
	neuraldist_free(sc->distribution);
	if(sc->csv_output)
		fclose(sc->csv_output);
}

int cryptonsc_config_process_command_line(cryptonsc_config *sc,
			int argc, const char **argv)
{
	neuraldist_error_t err;
	unsigned int num_servers;
	cmd_id_t cmd;
	const char *val;
	const char *neuraldist_str = NULL;
	const char *sess_pattern_str = NULL;
	const char *csv_path = NULL;

cmd_loop:
	if(!argc)
		goto post_process;
	if(!command_line_next_token(&argc, &argv, &cmd, &val))
		return 0;
	switch(cmd) {
	case CMD_NUM(SESSION_IDS):
		sc->output_sessions = 1; break;
	case CMD_NUM(NOLOGO):
		sc->nologo = 1; break;
        case CMD_NUM(VERS1):
        case CMD_NUM(VERS2):
                copyright(sc->nologo);
                return 0;
	case CMD_NUM(HELP1):
	case CMD_NUM(HELP2):
	case CMD_NUM(HELP3):
		copyright(sc->nologo);
		main_usage();
		return 0;
	case CMD_NUM(CONNECT):
		if(!neuraldist_push_address(sc->distribution, val)) {
			SYS_fprintf(SYS_stderr, "invalid syntax [%s]\n", val);
			return 0;
		}
		break;
	case CMD_NUM(CAFILE):
		sc->cacert = val; break;
	case CMD_NUM(CERT):
		sc->cert = val; break;
	case CMD_NUM(SSLMETH):
		if(!util_parse_sslmeth(val, &sc->sslmeth)) {
			SYS_fprintf(SYS_stderr, "invalid ssl/tls method\n");
			return 0;
		}
		break;
	case CMD_NUM(NUM):
		if(!int_strtoul(val, &sc->list_size) || !sc->list_size ||
					(sc->list_size > MAX_LIST_SIZE)) {
			SYS_fprintf(SYS_stderr, "invalid number of connections\n");
			return 0;
		}
		break;
	case CMD_NUM(COUNT):
		if(!int_strtoul(val, &sc->total_max) ||
				(sc->total_max > MAX_TOTAL_MAX)) {
			SYS_fprintf(SYS_stderr, "invalid number of requests\n");
			return 0;
		}
		break;
	case CMD_NUM(TIME):
		if(!int_strtoul(val, &sc->time_max) ||
				(sc->time_max > MAX_TIME_MAX)) {
			SYS_fprintf(SYS_stderr, "invalid time limit\n");
			return 0;
		}
		break;
	case CMD_NUM(EXPECT):
		{
		long tmp_long;
		if(!int_strtol(val, &tmp_long) || (tmp_long > MAX_RESPONSE_SIZE)) {
			SYS_fprintf(SYS_stderr, "invalid expected response size\n");
			return 0;
		}
		sc->response_expected = (tmp_long < 0 ?
				EXPECT_SERVER_CLOSE : (unsigned long)tmp_long);
		if(sc->response_expected > sc->response_size)
			sc->response_size = sc->response_expected;
		}
		break;
	case CMD_NUM(REQUEST):
		sc->request_string = util_parse_escaped_string(val); break;
	case CMD_NUM(SESSION):
		if(sess_pattern_str) {
			SYS_fprintf(SYS_stderr, "Only one -sessions argument can be "
					"specified\n");
			return 0;
		}
		sess_pattern_str = val;
		break;
	case CMD_NUM(UPDATE):
		if(!int_strtoul(val, &sc->period_update) ||
				(sc->period_update > MAX_PERIOD_UPDATE)) {
			SYS_fprintf(SYS_stderr, "invalid update period\n");
			return 0;
		}
		break;
	case CMD_NUM(CIPHER):
		sc->cipher_string = val; break;
	case CMD_NUM(CSV):
		if(csv_path) {
			SYS_fprintf(SYS_stderr, "Only one -csv argument can be "
					"specified\n");
			return 0;
		}
		csv_path = val;
		break;
	case CMD_NUM(DISTRIBUTE):
		if(neuraldist_str) {
			SYS_fprintf(SYS_stderr, "Only one -distribute argument can be "
					"specified\n");
			return 0;
		}
		neuraldist_str = val;
		break;
#ifdef HAVE_ENGINE
	case CMD_NUM(ENGINE):
		sc->engine_id = val; break;
#endif
	default:
		assert(NULL == "shouldn't happen!!!");
		return 0;
	}
	goto cmd_loop;

post_process:
	num_servers = neuraldist_num(sc->distribution);
	if(num_servers == 0) {
		SYS_fprintf(SYS_stderr, "Error, no servers specified. See 'sslcryptonsc -h' for usage on '-connect'\n");
		return 0;
	}

	err = neuraldist_parse(sc->distribution, neuraldist_str);
	if (err != ERR_DIST_PAT_OKAY) {
		if(neuraldist_str)
			SYS_fprintf(SYS_stderr, "Error, '%s' is an invalid distribute "
				"pattern: %s\n", neuraldist_str,
				neuraldist_error_string(err));
		else
			SYS_fprintf(SYS_stderr, "Error, 'neuraldist' failed: %s\n",
				neuraldist_error_string(err));
		if (err == ERR_DIST_PAT_VALUE_OUT_OF_RANGE)
			SYS_fprintf(SYS_stderr, "Specify values between 1 and "
					"%d.\n", num_servers);
		return 0;
	}

	if(sess_pattern_str && !session_string_set(sc, sess_pattern_str)) {
		SYS_fprintf(SYS_stderr, "Error, '%s' is an invalid session string\n",
				sess_pattern_str);
		return 0;
	}
	if(csv_path && ((sc->csv_output = fopen(val, "w")) == NULL)) {
		SYS_fprintf(SYS_stderr, "Error, '%s' is invalid csv path\n",
				csv_path);
		return 0;
	}
	return 1;
}


