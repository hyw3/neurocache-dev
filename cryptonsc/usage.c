/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "cryptonsc.h"

#include <assert.h>
#include <errno.h>

static unsigned int verify_fail_reported = 0;

void copyright(int nologo)
{
	if(!nologo)
		SYS_fprintf(SYS_stderr,
			"\nSSLCryptoNSC version %s (%s / %s)\n\n"
                        "This is an implementation of SSL/TLS of NeuroCache based on\n"
			"OpenSSL that can be used for enhancing the load-balancers.\n"
			"Copyright (C) 2020 Hilman P. Alisabana, Jakarta, Indonesia.\n\n",
			HAVE_PACKAGE_VERSION, HAVE_HYSCMTERM_UUID, HAVE_HYSCMTERM_DATE);
}

void main_usage(void)
{
	SYS_fprintf(SYS_stdout,
		"usage: sslcryptonsc [options ...]    where the options are\n");
	SYS_fprintf(SYS_stdout,
		"    -connect [ IP:<host>:<port> | UNIX:<path> ]\n");
	SYS_fprintf(SYS_stdout,
		"                      - (REQUIRED) a network address to connect to\n");
	SYS_fprintf(SYS_stdout,
		"    -CAfile <path>    - a PEM file containing trusted CA certificates\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = a list of common global paths)\n");
	SYS_fprintf(SYS_stdout,
		"    -cert <path>      - a PEM file containing the client cert and key\n");
	SYS_fprintf(SYS_stdout,
		"                        (default, no client certificate)\n");
	SYS_fprintf(SYS_stdout,
		"    -sslmeth <meth>   - specifies an SSL/TLS handshake version\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = normal, alternatives: sslv2, sslv3, tlsv1)\n");
	SYS_fprintf(SYS_stdout,
		"    -num <n>          - the number of simultaneous connections to use\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = 5)\n");
	SYS_fprintf(SYS_stdout,
		"    -count <n>        - the maximum number of requests to count\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = 0, keep going indefinitely)\n");
	SYS_fprintf(SYS_stdout,
		"    -time <n>         - the maximum number of seconds to run for\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = 0, keep going indefinitely)\n");
	SYS_fprintf(SYS_stdout,
		"    -request <string> - the string to send to the server\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = \"GET /\\r\\n\")\n");
	SYS_fprintf(SYS_stdout,
		"    -expect <n>       - the amount of data to expect in a response\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = 20 bytes)\n");
	SYS_fprintf(SYS_stdout,
		"    -session <string> - a string of 's' (new session) and 'r' (resume)\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = \"s\")\n");
	SYS_fprintf(SYS_stdout,
		"    -update <n>       - the number of seconds between printed updates\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = 0, no printed updates)\n");
	SYS_fprintf(SYS_stdout,
		"    -cipher <string>  - a string specifying the cipher suites\n");
	SYS_fprintf(SYS_stdout,
		"                        (default = 0, assume OpenSSL defaults)\n");
	SYS_fprintf(SYS_stdout,
		"    -csv <path>       - output per-second summaries to a CSV file\n");
	SYS_fprintf(SYS_stdout,
		"                        (default, no CSV output)\n");
	SYS_fprintf(SYS_stdout,
		"    -session_ids      - display all SSL session IDs negotiated\n");
#ifdef HAVE_ENGINE
	SYS_fprintf(SYS_stdout,
		"    -engine <id>      - Initialise and use the specified engine\n");
#endif
	SYS_fprintf(SYS_stdout,
		"    -distribute <str> - a pattern of server indexes for distribution\n");
	SYS_fprintf(SYS_stdout,
		"    -nologo           - supresses the output of the program name and author\n");
	SYS_fprintf(SYS_stdout,
		"                        (useful when trying to parse output automatically)\n");
	SYS_fprintf(SYS_stdout,
		"    -h, -?, --help    - display usage information\n");
	SYS_fprintf(SYS_stdout,
		"    -v, --version     - display version number information\n");
}

int unknown_switch(const char *str)
{
	SYS_fprintf(SYS_stderr,
		"invalid switch \"%s\" (or invalid value). See 'sslcryptonsc -h' for usage\n",
		str);
	return 0;
}

void verify_result_warning(void)
{
	if(verify_fail_reported)
		return;
	SYS_fprintf(SYS_stderr,
"Certificate verification failed, probably a self-signed server cert *or*\n"
"the signing CA cert is not trusted by us (hint: use '-CAfile').\n"
"This message will only be printed once\n");
	verify_fail_reported = 1;
}

int openssl_err(void)
{
	SYS_fprintf(SYS_stderr, " (OpenSSL errors follow)\n");
	ERR_print_errors_fp(SYS_stderr);
	return 1;
}

