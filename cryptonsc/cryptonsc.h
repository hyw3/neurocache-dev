/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_CRYPTONSC_H
#define HEADER_CRYPTONSC_H

#define SYS_GENERATING_EXE

#include <libgen/pre.h>
#include <libneural/neural.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/x509.h>
#ifdef HAVE_ENGINE
#include <openssl/engine.h>
#endif

#include <libgen/post.h>

typedef struct st_cryptonsc_thread_ctx cryptonsc_thread_ctx;
typedef struct st_learniterator learniterator;
typedef struct st_cryptonsc_config cryptonsc_config;
typedef struct st_neuraldist neuraldist;

typedef struct st_cryptonsc_item {
	const cryptonsc_thread_ctx *parent;
	SSL *ssl;
	SSL_SESSION *ssl_sess;
	BIO *bio_in;
	BIO *bio_out;
	const unsigned char *request;
	unsigned int request_size;
	unsigned int request_sent;
	unsigned char *response;
	unsigned int response_size;
	unsigned int response_received;
	unsigned int response_expected;
	NEURAL_CONNECTION *conn;
	learniterator *learniterator;
	unsigned int total_completed;
	unsigned int total_failed;
	unsigned int handshake_complete;
	unsigned int resumes_hit;
	unsigned int resumes_missed;
} cryptonsc_item;

struct st_cryptonsc_thread_ctx {
	const cryptonsc_config *config;
	SSL_CTX *ssl_ctx;
	cryptonsc_item *items;
	unsigned int size;
	NEURAL_SELECTOR *sel;
	unsigned int total_completed;
	unsigned int total_failed;
	unsigned int total_max;
	unsigned int resumes_hit;
	unsigned int resumes_missed;
};

void copyright(int nologo);
void main_usage(void);
int unknown_switch(const char *str);
void verify_result_warning(void);
int openssl_err(void);

typedef struct {
	const char *string;
	const char *delimiters;
	const char *position;
	char *token;
} tokeniser_t;
int init_tokeniser(tokeniser_t *t, const char *string, const char *delimiters);
void free_tokeniser(tokeniser_t *t);
char *do_tokenising(tokeniser_t *t);

int int_strtol(const char *str, long *val);
int int_strtoul(const char *str, unsigned long *val);

int int_substrtol(const char *str, long *val, const char *valid_terms);
int int_substrtoul(const char *str, unsigned long *val, const char *valid_terms);

char *util_parse_escaped_string(const char *str_toconvert);

typedef enum st_cryptonsc_sslmeth {
	CRYPTONSC_SSLMETH_NORMAL,
	CRYPTONSC_SSLMETH_SSLv2,
	CRYPTONSC_SSLMETH_SSLv3,
	CRYPTONSC_SSLMETH_TLSv1
} cryptonsc_sslmeth;

int util_parse_sslmeth(const char *str_toconvert, cryptonsc_sslmeth *val);

/* #define DATE_OUTPUT */

#define MAX_LIST_SIZE 100
#define MAX_TOTAL_MAX 2000000
#define MAX_TIME_MAX 86400 /* one day */
#define MAX_RESPONSE_SIZE 65536
#define EXPECT_SERVER_CLOSE ((unsigned int)-1)
#define MAX_PERIOD_UPDATE 300
#define CRYPTONSC_BUFFER_SIZE 2048

struct st_cryptonsc_config {
	const char *cacert;
	const char *cert;
	const char *cipher_string;
	cryptonsc_sslmeth sslmeth;
#ifdef HAVE_ENGINE
	const char *engine_id;
#endif
	unsigned long list_size;
	unsigned long total_max;
	unsigned long time_max;
	unsigned long response_size;
	unsigned long response_expected;
	const char *request_string;
	const char *session_string;
	unsigned int session_string_length;
	unsigned long period_update;
	unsigned int nologo;
	unsigned int output_sessions;
	FILE *csv_output;
	neuraldist *distribution;
};

void cryptonsc_config_init(cryptonsc_config *sc);
void cryptonsc_config_finish(cryptonsc_config *sc);
int cryptonsc_config_process_command_line(cryptonsc_config *sc,
			int argc, const char **argv);

#define CMD_STR(s,v)	static const char CMD_STR_##s[] = v;
#define CMD_NUM(s)	CMD_NUM_##s
#define IMPLEMENT_CMDS_STRINGS_RAW \
	CMD_STR(SESSION_IDS, "-session_ids") \
	CMD_STR(NOLOGO, "-nologo") \
	CMD_STR(VERS1, "-v") \
	CMD_STR(VERS2, "--version") \
	CMD_STR(HELP1, "-h") \
	CMD_STR(HELP2, "-?") \
	CMD_STR(HELP3, "--help") \
	CMD_STR(CONNECT, "-connect") \
	CMD_STR(CAFILE, "-CAfile") \
	CMD_STR(CERT, "-cert") \
	CMD_STR(SSLMETH, "-sslmeth") \
	CMD_STR(NUM, "-num") \
	CMD_STR(COUNT, "-count") \
	CMD_STR(TIME, "-time") \
	CMD_STR(EXPECT, "-expect") \
	CMD_STR(REQUEST, "-request") \
	CMD_STR(SESSION, "-session") \
	CMD_STR(UPDATE, "-update") \
	CMD_STR(CIPHER, "-cipher") \
	CMD_STR(CSV, "-csv") \
	CMD_STR(DISTRIBUTE, "-distribute")
#ifndef HAVE_ENGINE
#define IMPLEMENT_CMDS_STRINGS IMPLEMENT_CMDS_STRINGS_RAW
#else
#define IMPLEMENT_CMDS_STRINGS \
	IMPLEMENT_CMDS_STRINGS_RAW \
	CMD_STR(ENGINE, "-engine")
#endif

typedef enum {
	CMD_NUM(SESSION_IDS),
	CMD_NUM(NOLOGO),
	CMD_NUM(VERS1),
	CMD_NUM(VERS2),
	CMD_NUM(HELP1),
	CMD_NUM(HELP2),
	CMD_NUM(HELP3),
	CMD_NUM(CONNECT),
	CMD_NUM(CAFILE),
	CMD_NUM(CERT),
	CMD_NUM(SSLMETH),
	CMD_NUM(NUM),
	CMD_NUM(COUNT),
	CMD_NUM(TIME),
	CMD_NUM(EXPECT),
	CMD_NUM(REQUEST),
	CMD_NUM(SESSION),
	CMD_NUM(UPDATE),
	CMD_NUM(CIPHER),
	CMD_NUM(CSV),
	CMD_NUM(DISTRIBUTE)
#ifdef HAVE_ENGINE
	,CMD_NUM(ENGINE)
#endif
} cmd_id_t;

typedef struct st_cmd_defn {
	const char *	cmd_name;
	cmd_id_t	cmd_id;
	unsigned int	cmd_args;
} cmd_defn;

#define CMD0(s)		{CMD_STR_##s, CMD_NUM_##s, 0}
#define CMD1(s)		{CMD_STR_##s, CMD_NUM_##s, 1}
#define IMPLEMENT_CMDS_RAW \
	IMPLEMENT_CMDS_STRINGS \
	static const cmd_defn cmds[] = { \
	CMD0(SESSION_IDS), CMD0(NOLOGO),  CMD0(VERS1),  CMD0(VERS2), CMD0(HELP1), CMD0(HELP2), CMD0(HELP3), \
	CMD1(CONNECT), CMD1(CAFILE), CMD1(CERT), CMD1(SSLMETH), CMD1(NUM), CMD1(COUNT), \
	CMD1(TIME), CMD1(EXPECT), CMD1(REQUEST), CMD1(SESSION), CMD1(UPDATE), \
	CMD1(CIPHER), CMD1(CSV), CMD1(DISTRIBUTE),
#ifndef HAVE_ENGINE
#define IMPLEMENT_CMDS \
	IMPLEMENT_CMDS_RAW \
	{NULL,0,0} }
#else
#define IMPLEMENT_CMDS \
	IMPLEMENT_CMDS_RAW \
	CMD1(ENGINE), \
	{NULL,0,0} }
#endif

typedef enum {
	ERR_DIST_PAT_OKAY = 0,
	ERR_DIST_PAT_VALUE_OUT_OF_RANGE,
	ERR_DIST_PAT_INVALID_SYNTAX,
	ERR_DIST_PAT_ARRAY_FULL,
	ERR_DIST_PAT_INTERNAL_PROBLEM
} neuraldist_error_t;

void neuraldist_up(neuraldist *p);
neuraldist *neuraldist_new(void);
void neuraldist_free(neuraldist *dist);
unsigned int neuraldist_get_start_idx(neuraldist *p);
unsigned int neuraldist_period(neuraldist *dist);
unsigned int neuraldist_num(neuraldist *dist);
const NEURAL_ADDRESS *neuraldist_get(const neuraldist *dist,
				unsigned int idx);
int neuraldist_push_address(neuraldist *dist,
				const char *address);
neuraldist_error_t neuraldist_parse(neuraldist *dist,
				const char *dist_str);
const char *neuraldist_error_string(neuraldist_error_t err);

learniterator *learniterator_new(neuraldist *p);
void learniterator_free(learniterator *c);
const NEURAL_ADDRESS *learniterator_next(learniterator *c);

#endif /* !defined(HEADER_CRYPTONSC_H) */

