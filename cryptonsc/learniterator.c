/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */


#include "cryptonsc.h"

struct st_learniterator {
	neuraldist *p;
	unsigned int idx;
};

learniterator *learniterator_new(neuraldist *p)
{
	learniterator *c = SYS_malloc(learniterator, 1);
	if(!c)
		return NULL;

	neuraldist_up(p);

	c->p = p;
	c->idx = neuraldist_get_start_idx(p);

	return c;
}

void learniterator_free(learniterator *c)
{
	neuraldist_free(c->p);
	SYS_free(learniterator, c);
}

const NEURAL_ADDRESS *learniterator_next(learniterator *c)
{
	unsigned int idx = c->idx;

	if(++(c->idx) >= neuraldist_period(c->p))
		c->idx = 0;

	return neuraldist_get(c->p, idx);
}



