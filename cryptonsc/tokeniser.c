/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "cryptonsc.h"

#include <ctype.h>
#include <assert.h>
#include <errno.h>

int init_tokeniser(tokeniser_t *t, const char *string, const char *delimiters)
{
    if (t) {
        t->string     = string;
        t->delimiters = delimiters;
        t->position   = string;
        t->token = SYS_malloc(char, strlen(string) + 1);
        if (!t->token) {
            free_tokeniser(t);
            return 1;
        }
	return 0;
    }
    return 1;
}

void free_tokeniser(tokeniser_t *t)
{
    if (t) {
        t->string     = NULL;
        t->delimiters = NULL;
        t->position   = NULL;
        if (t->token)
            SYS_free(char, t->token);
        t->token = NULL;
    }
}

char *do_tokenising(tokeniser_t *t)
{
    if (t) {
        const char *p;
        int found_token = 0;
        int i = 0;

        if (t->position == NULL)
            return NULL;

        p = t->position;
        while ( (strchr(t->delimiters, *p)) != NULL && *p != '\0')
            p++;

        for (; *p != '\0'; p++) {
            while ( (strchr(t->delimiters, *p)) != NULL && *p != '\0') {
                found_token = 1;
                p++;
            }

            if (found_token)
                break;

            t->token[i++] = *p;
        }

        if (i == 0)
            return NULL;

        t->token[i] = '\0';

        if (found_token)
            t->position = p;
        else
            t->position = NULL;

        return t->token;
    }
    return NULL;
}

#ifndef HAVE_STRTOL
extern long int strtol(const char *nptr, char **endptr, int base)
{
	int negate = -1;
	long toret = 0;
	assert(base == 10);
keep_scanning:
	if(isdigit(*nptr)) {
		toret += ((*nptr) - '0');
		if((LONG_MAX / 10) <= toret) {
			toret = LONG_MAX;
			goto end;
		}
		toret *= 10;
	} else if((*nptr == '-') || (*nptr == '+')) {
		if(negate != -1)
			goto end;
		negate = ((*nptr == '-') ? 1 : 0);
	} else if(!isspace(*nptr)) {
		goto end;
	}
	nptr++;
	if(*nptr != '\0')
		goto keep_scanning;
end:
	return ((negate == 1) ? -toret : toret);
}
#endif

int int_strtol(const char *str, long *val)
{
	char *ptr;
	long tmp = strtol(str, &ptr, 10);
	if((ptr == str) || (*ptr != '\0'))
		return 0;
	*val = tmp;
	return 1;
}

int int_strtoul(const char *str, unsigned long *val)
{
	long tmp;
	if(!int_strtol(str, &tmp) || (tmp < 0))
		return 0;
	*val = tmp;
	return 1;
}

int int_substrtol(const char *str, long *val, const char *valid_terms)
{
	char *ptr;
	long tmp = strtol(str, &ptr, 10);
	if((ptr == str) || ((*str == '-') && (str + 1 == ptr)))
		return 0;
	*val = tmp;
	if(!valid_terms || (*ptr == '\0') || strchr(valid_terms, *ptr))
		return 1;
	return 0;
}

int int_substrtoul(const char *str, unsigned long *val, const char *valid_terms)
{
	long tmp;
	if(!int_substrtol(str, &tmp, valid_terms) || (tmp < 0))
		return 0;
	*val = tmp;
	return 1;
}

char *util_parse_escaped_string(const char *str_toconvert)
{
	char *toreturn, *dest;
	int ctrl = 0;

	SYS_strdup(&toreturn, str_toconvert);
	if(!toreturn) return NULL;
	dest = toreturn;
	while(*str_toconvert) {
		if(!ctrl) {
			if(*str_toconvert != '\\')
				*(dest++) = *str_toconvert;
			else
				ctrl = 1;
		} else {
			switch(*str_toconvert) {
			case 'r':
				*(dest++) = '\r'; break;
			case 'n':
				*(dest++) = '\n'; break;
			default:
				*(dest++) = *str_toconvert;
			}
			ctrl = 0;
		}
		str_toconvert++;
	}
	*dest = 0;
	return toreturn;
}

int util_parse_sslmeth(const char *str_toconvert, cryptonsc_sslmeth *val)
{
	if(!strcmp(str_toconvert, "normal"))
		*val = CRYPTONSC_SSLMETH_NORMAL;
	else if(!strcmp(str_toconvert, "sslv2"))
		*val = CRYPTONSC_SSLMETH_SSLv2;
	else if(!strcmp(str_toconvert, "sslv3"))
		*val = CRYPTONSC_SSLMETH_SSLv3;
	else if(!strcmp(str_toconvert, "tlsv1"))
		*val = CRYPTONSC_SSLMETH_TLSv1;
	else
		return 0;
	return 1;
}

