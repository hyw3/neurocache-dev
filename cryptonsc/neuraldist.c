/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019-2020 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */


#include "cryptonsc.h"

#include <assert.h>

/* #define REF_DEBUG */

#define MAX_SERVERS 1000

#define MAX_NEURAL_DISTRIBUTION 1000

struct st_neuraldist {
	NEURAL_ADDRESS *items[MAX_SERVERS];
	unsigned int num;
	unsigned int idx[MAX_NEURAL_DISTRIBUTION];
	unsigned int period;
	unsigned int references;
	unsigned int start_idx;
};

void neuraldist_up(neuraldist *p)
{
	(p->references)++;
#ifdef REF_DEBUG
	printf("REF_DEBUG: neuraldist_up(%08x), ref count now %u\n",
			(unsigned int)p, p->references);
#endif
}

neuraldist *neuraldist_new(void)
{
	neuraldist *p = SYS_malloc(neuraldist, 1);
	if(!p)
		return NULL;
	p->num = 0;
	p->period = 0;
	p->references = 1;
	p->start_idx = 0;
#ifdef REF_DEBUG
	printf("REF_DEBUG: neuraldist_new(%08x), ref count now %u\n",
			(unsigned int)p, p->references);
#endif
	return p;
}

void neuraldist_free(neuraldist *p)
{
	(p->references)--;
#ifdef REF_DEBUG
	printf("REF_DEBUG: neuraldist_free(%08x), ref count now %u\n",
			(unsigned int)p, p->references);
#endif
	if(p->references < 0) {
		assert(NULL == "shouldn't happen!");
		abort();
	} else if(p->references == 0)
		SYS_free(neuraldist, p);
}

unsigned int neuraldist_get_start_idx(neuraldist *p)
{
	unsigned int toret = p->start_idx++;
	if(p->start_idx >= p->period)
		p->start_idx = 0;
	return toret;
}

unsigned int neuraldist_period(neuraldist *dist)
{
	return dist->period;
}

unsigned int neuraldist_num(neuraldist *dist)
{
	return dist->num;
}

const NEURAL_ADDRESS *neuraldist_get(const neuraldist *dist,
					unsigned int idx)
{
	if(idx >= dist->period)
		return NULL;
	return dist->items[dist->idx[idx]];
}

int neuraldist_push_address(neuraldist *dist, const char *address)
{
	NEURAL_ADDRESS *sa;

	if((dist->num >= MAX_SERVERS) || ((sa = NEURAL_ADDRESS_new()) == NULL))
		return 0;
	if(!NEURAL_ADDRESS_create(sa, address, CRYPTONSC_BUFFER_SIZE)) {
		NEURAL_ADDRESS_free(sa);
		return 0;
	}
	dist->items[dist->num++] = sa;
	return 1;
}

static neuraldist_error_t neuraldist_push(neuraldist *dist,
					unsigned int val)
{
	if(dist->period >= MAX_NEURAL_DISTRIBUTION)
		return ERR_DIST_PAT_ARRAY_FULL;

	if(!val || (val > dist->num))
		return ERR_DIST_PAT_VALUE_OUT_OF_RANGE;

	dist->idx[dist->period++] = val - 1;

	return ERR_DIST_PAT_OKAY;
}

neuraldist_error_t neuraldist_parse(neuraldist *dist,
				const char *dist_str)
{
	char *res;
	unsigned long val;
	tokeniser_t tok;
	neuraldist_error_t ret = ERR_DIST_PAT_OKAY;

	assert(dist);

	if(!dist_str) {
		unsigned int loop = 0;
		while(loop < dist->num) {
			ret = neuraldist_push(dist, ++loop);
			if(ret != ERR_DIST_PAT_OKAY)
				return ret;
		}
		return ret;
	}

	init_tokeniser(&tok, dist_str, ", ");
	while((res = do_tokenising(&tok)) != NULL) {
		if(!int_substrtoul(res, &val, ", ")) {
			ret = ERR_DIST_PAT_INVALID_SYNTAX;
			break;
		}
		ret = neuraldist_push(dist, val);
		if(ret != ERR_DIST_PAT_OKAY)
			break;
	}
	free_tokeniser(&tok);
	return ret;
}

const char *neuraldist_error_string(neuraldist_error_t err)
{
	switch(err) {
	case ERR_DIST_PAT_OKAY:
		return "no error";
	case ERR_DIST_PAT_VALUE_OUT_OF_RANGE:
		return "value out of range";
	case ERR_DIST_PAT_INVALID_SYNTAX:
		return "invalid syntax";
	case ERR_DIST_PAT_ARRAY_FULL:
		return "distribute pattern array is full";
	case ERR_DIST_PAT_INTERNAL_PROBLEM:
		return "internal error (a bug?)";
	}
	return "";
}


