/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */


#include "cryptonsc.h"

#ifndef CACERT_PATH
#error "CACERT_PATH not set"
#endif

static const char *cacert_paths[] = {
	"CA-sample.pem", "cacert.pem",
	CACERT_PATH,
	"/etc/cacert.pem", NULL};

static void ossl_do_good_seeding(void);
static SSL_CTX *ossl_setup_ssl_ctx(const cryptonsc_config *config);
static void ossl_close_ssl_ctx(SSL_CTX *ctx);
static int cryptonsc_item_init(cryptonsc_item *item, cryptonsc_thread_ctx *ctx);
static void cryptonsc_item_finish(cryptonsc_item *item);
static void cryptonsc_item_dirty_loop(cryptonsc_item *item);
static int cryptonsc_thread_ctx_init(cryptonsc_thread_ctx *ctx,
				const cryptonsc_config *config);
static void cryptonsc_thread_ctx_finish(cryptonsc_thread_ctx *ctx);
static int cryptonsc_thread_ctx_loop(cryptonsc_thread_ctx *ctx);

static void ossl_do_good_seeding(void)
{
	unsigned int loop;

	for(loop = 0; loop < 1000; loop++)
		RAND_seed(&loop, sizeof(loop));
}

static SSL_CTX *ossl_setup_ssl_ctx(const cryptonsc_config *config)
{
	FILE *fp = NULL;
	X509 *x509 = NULL;
	RSA *rsa = NULL;
	SSL_CTX *ctx = NULL;
	const char **paths;
	SSL_METHOD const *sslmethod = NULL;
#ifdef HAVE_ENGINE
	ENGINE *e = NULL;
#endif
	SSL_load_error_strings();
	SSLeay_add_ssl_algorithms();

#ifdef HAVE_ENGINE
	if(config->engine_id) {
		if((e = ENGINE_by_id(config->engine_id)) == NULL) {
			SYS_fprintf(SYS_stderr, "No such engine as \"%s\"\n",
				config->engine_id);
			return NULL;
		}
		if(!ENGINE_set_default(e, ENGINE_METHOD_ALL)) {
			SYS_fprintf(SYS_stderr, "Engine was unable to initialise\n");
			return NULL;
		}
		ENGINE_free(e);
	}
#endif
	switch(config->sslmeth) {
	case CRYPTONSC_SSLMETH_NORMAL:
		sslmethod = SSLv23_client_method(); break;
	default:
		return NULL;
	}
	ctx = SSL_CTX_new(sslmethod);
	if(!ctx) return NULL;

	if(config->cacert) {
		fp = fopen(config->cacert, "r");
		if(fp == NULL)
			return NULL;
		if(!PEM_read_X509(fp, &x509, NULL, NULL))
			return NULL;
	} else {
		SYS_fprintf(SYS_stderr, "No 'cacert' supplied, trying defaults ...");
		paths = cacert_paths;
		while(*paths && !fp) {
			fp = fopen(*paths, "r");
			if((fp == NULL) || !PEM_read_X509(fp, &x509,
						NULL, NULL)) {
				if(fp) fclose(fp);
				fp = NULL;
			}
			if(fp)
				SYS_fprintf(SYS_stderr, " '%s' found.\n", *paths);
			else
				paths++;
		}
		if(!fp) {
			SYS_fprintf(SYS_stderr, " none found\n");
			return NULL;
		}
	}
	if(fp) {
		fclose(fp);
		fp = NULL;
	}
	if(x509) {
		X509_free(x509);
		x509 = NULL;
	}
	if(!SSL_CTX_set_default_verify_paths(ctx))
		return NULL;

	if(config->cert) {
		fp = fopen(config->cert, "r");
		if(fp == NULL)
			return NULL;
		if(!PEM_read_X509(fp, &x509, NULL, NULL))
			return NULL;
		if(!SSL_CTX_use_certificate(ctx, x509))
			return NULL;
		X509_free(x509);
		x509 = NULL;

		if(!PEM_read_RSAPrivateKey(fp, &rsa, NULL, NULL))
			return NULL;
		if(!SSL_CTX_use_RSAPrivateKey(ctx, rsa))
			return NULL;
		RSA_free(rsa);
		rsa = NULL;
		fclose(fp);
		fp = NULL;
	} else
		SYS_fprintf(SYS_stderr, "no client cert provided, continuing "
			"anyway.\n");

	if(config->cipher_string)
		if(!SSL_CTX_set_cipher_list(ctx, config->cipher_string))
			return NULL;

	return ctx;
}

static void ossl_close_ssl_ctx(SSL_CTX *ctx)
{
	SSL_CTX_free(ctx);
}

static int cryptonsc_thread_ctx_init(cryptonsc_thread_ctx *ctx,
				const cryptonsc_config *config)
{
	unsigned int loop;

	SYS_zero(cryptonsc_thread_ctx, ctx);
	ctx->items = SYS_malloc(cryptonsc_item, config->list_size);
	if(!ctx->items)
		goto fail;
	SYS_zero_n(cryptonsc_item, ctx->items, config->list_size);
	ctx->size = config->list_size;
	if((ctx->sel = NEURAL_SELECTOR_new()) == NULL)
		goto fail;
	ctx->config = config;
	ctx->ssl_ctx = ossl_setup_ssl_ctx(config);
	if(!ctx->ssl_ctx) {
		openssl_err();
		goto fail;
	}
	ctx->total_completed = 0;
	ctx->total_failed = 0;
	ctx->total_max = config->total_max;
	for(loop = 0; loop < ctx->size; loop++)
	{
		ctx->items[loop].parent = ctx;
		ctx->items[loop].request =
			(const unsigned char *)config->request_string;
		ctx->items[loop].request_size = strlen(config->request_string);
		ctx->items[loop].response = SYS_malloc(unsigned char,
					config->response_size);
		if(!ctx->items[loop].response)
			goto fail;
		ctx->items[loop].response_size = config->response_size;
		ctx->items[loop].response_expected = config->response_expected;
		ctx->items[loop].conn = NULL;
		if(!(ctx->items[loop].learniterator = learniterator_new(
				config->distribution)))
			goto fail;
	}
	return 1;
fail:
	if(ctx->items) {
		for(loop = 0; loop < ctx->size; loop++) {
			if(ctx->items[loop].learniterator)
				learniterator_free(
					ctx->items[loop].learniterator);
		}
		SYS_free(cryptonsc_item, ctx->items);
		ctx->items = NULL;
	}
	if(ctx->sel) {
		NEURAL_SELECTOR_free(ctx->sel);
		ctx->sel = NULL;
	}
	if(ctx->ssl_ctx) {
		ossl_close_ssl_ctx(ctx->ssl_ctx);
		ctx->ssl_ctx = NULL;
	}
	return 0;
}

static void cryptonsc_thread_ctx_finish(cryptonsc_thread_ctx *ctx)
{
	unsigned int loop;
	for(loop = 0; loop < ctx->size; loop++) {
		SYS_free(unsigned char, ctx->items[loop].response);
		learniterator_free(ctx->items[loop].learniterator);
		if(ctx->items[loop].conn)
			NEURAL_CONNECTION_free(ctx->items[loop].conn);
	}
	SYS_free(cryptonsc_item, ctx->items);
	NEURAL_SELECTOR_free(ctx->sel);
	ossl_close_ssl_ctx(ctx->ssl_ctx);
}

static void cryptonsc_item_dirty_loop(cryptonsc_item *item)
{
	unsigned int max;
	NEURAL_BUFFER *buf;
	int handshook = SSL_is_init_finished(item->ssl);
	if(!handshook)
		SSL_do_handshake(item->ssl);
	buf = NEURAL_CONNECTION_get_send(item->conn);
	max = NEURAL_BUFFER_unused(buf);
	if(max) {
		unsigned char *ptr = NEURAL_BUFFER_write_ptr(buf);
		int tmp = BIO_read(item->bio_out, ptr, max);
		if(tmp > 0)
			NEURAL_BUFFER_wrote(buf, tmp);
	}
	buf = NEURAL_CONNECTION_get_read(item->conn);
	max = NEURAL_BUFFER_used(buf);
	if(max) {
		const unsigned char *ptr = NEURAL_BUFFER_data(buf);
		int tmp = BIO_write(item->bio_in, ptr, max);
		if(tmp > 0)
			NEURAL_BUFFER_read(buf, NULL, tmp);
	}
	if(!handshook && !SSL_is_init_finished(item->ssl))
		SSL_do_handshake(item->ssl);
}

static int cryptonsc_item_init(cryptonsc_item *item, cryptonsc_thread_ctx *ctx)
{
	const cryptonsc_config *config;
	config = item->parent->config;

	if(((item->conn = NEURAL_CONNECTION_new()) == NULL) ||
			!NEURAL_CONNECTION_create(item->conn, learniterator_next(
				item->learniterator)) ||
			!NEURAL_CONNECTION_add_to_selector(item->conn, ctx->sel)) {
		SYS_fprintf(SYS_stderr, "connect failed\n");
		if(item->conn)
			NEURAL_CONNECTION_free(item->conn);
		return 0;
	}
	item->ssl = SSL_new(item->parent->ssl_ctx);
	if(!item->ssl) {
		SYS_fprintf(SYS_stderr, "SSL_new() failed\n");
		return 0;
	}
	item->bio_in = BIO_new(BIO_s_mem());
	item->bio_out = BIO_new(BIO_s_mem());
	if(!item->bio_in || !item->bio_out) {
		SYS_fprintf(SYS_stderr, "BIO_new() failed\n");
		return 0;
	}
	if(config->session_string &&
			(config->session_string[item->total_completed %
				config->session_string_length] == 'r'))
		SSL_set_session(item->ssl, item->ssl_sess);
	SSL_set_verify_depth(item->ssl, 10);
	SSL_set_bio(item->ssl, item->bio_in, item->bio_out);
	SSL_set_connect_state(item->ssl);
	item->request_sent = 0;
	item->response_received = 0;
	item->handshake_complete = 0;
	return 1;
}

static void cryptonsc_item_finish(cryptonsc_item *item)
{
	SSL_free(item->ssl);
	item->ssl = NULL;
	item->bio_in = item->bio_out = NULL;
	NEURAL_CONNECTION_free(item->conn);
	item->conn = NULL;
}

static int cryptonsc_thread_ctx_loop(cryptonsc_thread_ctx *ctx)
{
	int tmp;
	unsigned int loop;
	cryptonsc_item *item;
	SSL_SESSION *temp_session = NULL;
	const char *session_string = ctx->config->session_string;
	unsigned int session_string_length =
			ctx->config->session_string_length;

	ctx->total_completed = 0;
	ctx->total_failed = 0;
	ctx->resumes_hit = 0;
	ctx->resumes_missed = 0;

	for(loop = 0; loop < ctx->size; loop++)
	{
		item = ctx->items + loop;

		if(item->conn && !NEURAL_CONNECTION_io(item->conn)) {
			cryptonsc_item_finish(item);
			item->total_failed++;
		}

possible_reconnect:
		if(!item->conn && !cryptonsc_item_init(item, ctx))
			return 0;
		cryptonsc_item_dirty_loop(item);
		if(!item->handshake_complete && SSL_is_init_finished(item->ssl)) {
			if(SSL_get_verify_result(item->ssl) != X509_V_OK)
				verify_result_warning();
			item->handshake_complete = 1;
			if(session_string) {
				temp_session = SSL_get1_session(item->ssl);
				if(session_string[item->total_completed %
						session_string_length] == 'r') {
					if(temp_session == item->ssl_sess)
						item->resumes_hit++;
					else
						item->resumes_missed++;
					SSL_SESSION_free(temp_session);
				} else {
					if(item->ssl_sess)
						SSL_SESSION_free(item->ssl_sess);
					item->ssl_sess = temp_session;
				}
			}
			if(ctx->config->output_sessions) {
				temp_session = SSL_get1_session(item->ssl);
				SYS_fprintf(SYS_stderr, "session-id[conn:%i]:", loop);
				SYS_fprintf(SYS_stderr, "\n");
				SSL_SESSION_free(temp_session);
			}
		}

		if(item->request_sent < item->request_size) {
			tmp = SSL_write(item->ssl, item->request + item->request_sent,
				item->request_size - item->request_sent);
			if(tmp > 0)
				item->request_sent += tmp;
		}
		if((item->request_sent == item->request_size) &&
				(item->response_received < item->response_expected)) {
			tmp = SSL_read(item->ssl, (char *)item->response,
				item->response_size);
			if(tmp > 0)
				item->response_received += tmp;
			if(item->response_received >= item->response_expected)
				SSL_shutdown(item->ssl);
		}
		if((item->request_sent == item->request_size) &&
				(item->response_received >=
				item->response_expected)) {
			if(NEURAL_BUFFER_empty(NEURAL_CONNECTION_get_send(item->conn))) {
				cryptonsc_item_finish(item);
				item->total_completed++;
				goto possible_reconnect;
			}
		}
		if(item->conn)
			cryptonsc_item_dirty_loop(item);
		ctx->total_completed += item->total_completed;
		ctx->total_failed += item->total_failed;
		ctx->resumes_hit += item->resumes_hit;
		ctx->resumes_missed += item->resumes_missed;
	}
	fflush(SYS_stderr);
	return 1;
}

#define PRINT_PERIOD_UPDATE() { \
	long span; rate = (float)-1; SYS_gettime(&exact_finish); \
	if((span = SYS_msecs_between(&exact_start, &exact_finish)) > 0) \
		rate = (float)(ctx.total_completed - last_total) * 1000 / span; \
	SYS_timecpy(&exact_start, &exact_finish); \
	SYS_fprintf(SYS_stderr, "%u seconds since starting, %u successful, " \
		"%u failed, resumes(+%u,-%u) %.2f ops/sec\n", \
		(unsigned int)(finish - start), ctx.total_completed, \
		ctx.total_failed, ctx.resumes_hit, ctx.resumes_missed, rate); \
	last_update += config.period_update; \
	last_total = ctx.total_completed; }

int main(int argc, char *argv[])
{
	cryptonsc_config config;
	cryptonsc_thread_ctx ctx;
	time_t start, finish;
	time_t last_update, last_csv;
	struct timeval exact_start, exact_finish;
	float rate;
	int select_res, toreturn = 0;
	unsigned int last_total = 0;

	cryptonsc_config_init(&config);

	ossl_do_good_seeding();

	if(!cryptonsc_config_process_command_line(&config,
			argc - 1, (const char **)argv + 1))
		return 1;

	copyright(config.nologo);

	if(!cryptonsc_thread_ctx_init(&ctx, &config)) {
		SYS_fprintf(SYS_stderr, "error setting up cryptonsc_thread_ctx");
		return(openssl_err());
	}

	time(&start);
	last_update = last_csv = start;
#ifdef DATE_OUTPUT
	SYS_fprintf(SYS_stderr, "%s", ctime(&start));
#endif
	time(&finish);
	SYS_gettime(&exact_start);

loop_start:
	if(!cryptonsc_thread_ctx_loop(&ctx)) {
		SYS_fprintf(SYS_stderr, "error in data loop");
		return(openssl_err());
	}
	if((ctx.total_max > 0) && ((ctx.total_completed +
			ctx.total_failed) >= ctx.total_max))
		goto loop_complete;
	if((config.time_max > 0) && ((unsigned long)(finish - start) >= config.time_max))
		goto loop_complete;
	select_res = NEURAL_SELECTOR_select(ctx.sel, 0, 0);
	if(select_res < 0) {
		SYS_fprintf(SYS_stderr, "error in select()");
		toreturn = 1;
		goto loop_complete;
	}
	time(&finish);
	if(config.period_update > 0) {
		if((unsigned long)(finish - last_update) >= config.period_update)
			PRINT_PERIOD_UPDATE()
	}
	if(config.csv_output && (finish > last_csv)) {
		char time_cheat[50];
		int time_cheat_len;
		time_cheat_len = sprintf(time_cheat, "%s", ctime(&last_csv));
		if(time_cheat[time_cheat_len - 1] == '\n')
			time_cheat[time_cheat_len - 1] = '\0';
		SYS_fprintf(config.csv_output, "%s,%u,%u,%u,%u\n",
			time_cheat, ctx.total_completed, ctx.total_failed,
			ctx.resumes_hit, ctx.resumes_missed);
		fflush(config.csv_output);
		last_csv = finish;
	}
	goto loop_start;
loop_complete:
	PRINT_PERIOD_UPDATE()
#ifdef DATE_OUTPUT
	SYS_fprintf(SYS_stderr, "%s", ctime(&finish));
#endif
	cryptonsc_thread_ctx_finish(&ctx);
	cryptonsc_config_finish(&config);
	return toreturn;
}


