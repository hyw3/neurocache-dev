/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_EXE

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <libgen/post.h>

#include <assert.h>
#include <errno.h>
#include <ctype.h>

static int verbose = 0;

static int pindah_desc(int src, int dest)
{
	if(dup2(src, dest) == -1)
		return 0;
	close(src);
	return 1;
}

typedef struct desc_mandor_defn mandor_defn;
struct desc_mandor_defn {
	int fd_left;
	int fd_right;
	int ltr;
	int safe_left;
	int safe_right;
};

static void mandor_defn_close_left(mandor_defn *d)
{
	close(d->safe_left);
}

static void mandor_defn_close_right(mandor_defn *d)
{
	close(d->safe_right);
}

static int mandor_defn_apply_left(mandor_defn *d)
{
	return pindah_desc(d->safe_left, d->fd_left);
}

static int mandor_defn_apply_right(mandor_defn *d)
{
	return pindah_desc(d->safe_right, d->fd_right);
}

static int mandor_defn_tengah(mandor_defn *d, NEURAL_CONNECTION *conn)
{
	NEURAL_ADDRESS *addr = NEURAL_ADDRESS_new();
	char buf[256];
	mandor_defn_close_right(d);
	if(!mandor_defn_apply_left(d)) return 0;
	if(!addr) return 0;
	if(d->ltr) return 0;
	sprintf(buf, "FD:%d:-1", d->fd_left);
	if(!NEURAL_ADDRESS_create(addr, buf, 1))
		return 0;
	if(!NEURAL_CONNECTION_create(conn, addr))
		return 0;
	NEURAL_ADDRESS_free(addr);
	return 1;
}

static void mandor_defn_print(mandor_defn *d)
{
	SYS_fprintf(SYS_stderr, "%d%s%d ", d->fd_left,
			(d->ltr ? ">" : "<"), d->fd_right);
}

static void mandor_defn_set(mandor_defn *d, int left, int right, int ltr)
{
	d->fd_left = left;
	d->fd_right = right;
	d->ltr = ltr;
}

static int mandor_defn_parse(mandor_defn *d, const char **s)
{
	int step = 0;
	int num = 0;
loop:
	if(isdigit(**s)) {
		num *= 10;
		num += (int)(**s - '0');
	} else if((**s == '>') || (**s == '<')) {
		if(step) return 0;
		d->fd_left = num;
		d->ltr = ((**s == '>') ? 1 : 0);
		step = 1;
		num = 0;
	} else if((**s == ',') || (**s == '\0')) {
		if(!step) return 0;
		d->fd_right = num;
		return 1;
	} else
		return 0;
	(*s)++;
	goto loop;
}

static int mandor_defn_build(mandor_defn *d, int *fdnext)
{
	int fds[2];
	int fdtmp;
	d->safe_left = d->safe_right = -1;
	if(pipe(fds) != 0)
		return 0;
	fdtmp = *fdnext + 2;
	if(fdtmp <= fds[0]) fdtmp = fds[0] + 1;
	if(fdtmp <= fds[1]) fdtmp = fds[1] + 1;
	if(!pindah_desc(fds[0], fdtmp) || !pindah_desc(fds[1], fdtmp + 1))
		return 0;
	if(!pindah_desc(fdtmp, *fdnext)) return 0;
	fds[0] = (*fdnext)++;
	if(!pindah_desc(fdtmp + 1, *fdnext)) return 0;
	fds[1] = (*fdnext)++;
	if(d->ltr) {
		d->safe_left = fds[1];
		d->safe_right = fds[0];
	} else {
		d->safe_left = fds[0];
		d->safe_right = fds[1];
	}
	return 1;
}

typedef struct desc_mandor_rules mandor_rules;
struct desc_mandor_rules {
#define MANDOR_RULES_SIZE 8
	mandor_defn *defns;
	unsigned int used, size;
	int safe_start, safe_next;
};

static void mandor_rules_close_left(mandor_rules *r)
{
	unsigned int idx = 0;
	while(idx < r->used)
		mandor_defn_close_left(r->defns + (idx++));
}

static void mandor_rules_close_right(mandor_rules *r)
{
	unsigned int idx = 0;
	while(idx < r->used)
		mandor_defn_close_right(r->defns + (idx++));
}

static int mandor_rules_apply_left(mandor_rules *r)
{
	unsigned int idx = 0;
	while(idx < r->used)
		if(!mandor_defn_apply_left(r->defns + (idx++)))
			return 0;
	return 1;
}

static int mandor_rules_apply_right(mandor_rules *r)
{
	unsigned int idx = 0;
	while(idx < r->used)
		if(!mandor_defn_apply_right(r->defns + (idx++)))
			return 0;
	return 1;
}

static int mandor_rules_bookroom(mandor_rules *r)
{
	if(r->used == r->size) {
		unsigned int newsize = (r->size ? (r->size * 3 / 2) : MANDOR_RULES_SIZE);
		mandor_defn *newdefns = SYS_malloc(mandor_defn, newsize);
		if(!newdefns) return 0;
		if(r->used) SYS_memcpy_n(mandor_defn, newdefns, r->defns, r->used);
		if(r->size) SYS_free(mandor_defn, r->defns);
		r->defns = newdefns;
		r->size = newsize;
	}
	return 1;
}

static int mandor_rules_set(mandor_rules *r, const char *params)
{
	r->defns = NULL;
	r->used = r->size = 0;
	if(*params == '\0') return mandor_rules_set(r, "1>0,0<1");
	if(!isdigit(*params)) return 0;
	do {
		if(!mandor_rules_bookroom(r)) return 0;
		if(!mandor_defn_parse(r->defns + r->used, &params)) return 0;
		r->used++;
	} while(*(params++) == ',');
	if(*(--params) != '\0') return 0;
	return 1;
}

static void mandor_rules_print(mandor_rules *r)
{
	unsigned int idx = 0;
	while(idx < r->used)
		mandor_defn_print(r->defns + (idx++));
	SYS_fprintf(SYS_stderr, "\n");
}

static int mandor_rules_build(mandor_rules *r, int fdsafe)
{
	unsigned int idx = 0;
	r->safe_start = r->safe_next = fdsafe;
	while(idx < r->used)
		if(!mandor_defn_build(r->defns + (idx++), &r->safe_next))
			return 0;
	return 1;
}

static int mandor_rules_build_plus(mandor_rules *r, mandor_defn *d)
{
	mandor_defn_set(d, r->safe_next++, r->safe_next++, 0);
	return mandor_defn_build(d, &r->safe_next);
}

typedef struct desc_task_args task_args;
struct desc_task_args {
#define TASK_ARGS_MAX 511
	const char *args[TASK_ARGS_MAX + 1];
	unsigned used;
};

static void task_args_init(task_args *a)
{
	a->used = 0;
}

static int task_args_add(task_args *a, const char *arg)
{
	if(a->used == TASK_ARGS_MAX) return 0;
	a->args[a->used++] = arg;
	return 1;
}

static void task_args_exec(task_args *a)
{
	char *argv[TASK_ARGS_MAX + 1];
	SYS_memcpy_n(char *, argv, a->args, a->used + 1);
	execvp(a->args[0], argv);
}

static int task_args_reasonable(task_args *a)
{
	return ((a->used > 0) && task_args_add(a, NULL));
}

static void task_args_print(task_args *a)
{
	unsigned int idx = 0;
	while(idx < a->used)
		SYS_fprintf(SYS_stderr, "%s ", a->args[idx++]);
	SYS_fprintf(SYS_stderr, "\n");
}

typedef struct desc_mandor_task mandor_task;
struct desc_mandor_task {
	task_args args;
	mandor_defn tengah_mandor;
	NEURAL_CONNECTION *tengah_conn;
	pid_t child_pid;
};

static int mandor_task_exec(mandor_task *task, mandor_rules *rules, mandor_task *left)
{
	pid_t res;
	if(!mandor_rules_build_plus(rules, &task->tengah_mandor))
		return 0;
	res = fork();
	switch(res) {
	case -1:
		return 0;
	case 0:
		break;
	default:
		task->child_pid = res;
		if(left)
			mandor_rules_close_right(rules);
		else
			mandor_rules_close_left(rules);
		if(!mandor_defn_tengah(&task->tengah_mandor, task->tengah_conn))
			return 0;
		return 1;
	}

	mandor_defn_close_left(&task->tengah_mandor);
	if(left) {
		mandor_defn_close_left(&left->tengah_mandor);
		if(!mandor_rules_apply_right(rules))
			return 1;
	} else {
		mandor_rules_close_right(rules);
		if(!mandor_rules_apply_left(rules))
			return 1;
	}
	task_args_exec(&task->args);
	exit(1);
}

static int mandor_task_init(mandor_task *task)
{
	task_args_init(&task->args);
	task->tengah_conn = NEURAL_CONNECTION_new();
	return (task->tengah_conn ? 1 : 0);
}

static int mandor_task_dimengerti(mandor_task *task)
{
	if(!task_args_reasonable(&task->args)) {
		SYS_fprintf(SYS_stderr, "Error, empty command\n");
		return 0;
	}
	return 1;
}

static int mandor_task_lahap(mandor_task *task, const char *s)
{
	if(!task_args_add(&task->args, s)) {
		SYS_fprintf(SYS_stderr, "Error, internal error\n");
		return 0;
	}
	return 1;
}

static void mandor_task_print(mandor_task *task)
{
	task_args_print(&task->args);
}

static int do_waitpid(pid_t pid)
{
	int status;
	if(waitpid(pid, &status, 0) <= 0)
		return 0;
	if(WEXITSTATUS(status) != 0)
		return 0;
	return 1;
}

static int do_waiting(mandor_task *task1, mandor_task *task2)
{
	NEURAL_SELECTOR *sel = NEURAL_SELECTOR_new();
	if(!sel) return 1;
	if((task1->child_pid != -1) && !NEURAL_CONNECTION_add_to_selector(
			task1->tengah_conn, sel))
		return 0;
	if((task2->child_pid != -1) && !NEURAL_CONNECTION_add_to_selector(
			task2->tengah_conn, sel))
		return 0;
	while(1) {
		NEURAL_SELECTOR_select(sel, 0, 0);
		if((task1->child_pid != -1) &&
				!NEURAL_CONNECTION_io(task1->tengah_conn)) {
			if(verbose)
				SYS_fprintf(SYS_stderr,
					"task1 lost contact, cleaning ...\n");
			NEURAL_CONNECTION_free(task1->tengah_conn);
			if(!do_waitpid(task1->child_pid)) {
				if(verbose)
					SYS_fprintf(SYS_stderr, "task1 failed\n");
				return 1;
			}
			if(verbose)
				SYS_fprintf(SYS_stderr, "task1 exited cleanly\n");
			task1->child_pid = -1;
		}
		if((task2->child_pid != -1) &&
				!NEURAL_CONNECTION_io(task2->tengah_conn)) {
			if(verbose)
				SYS_fprintf(SYS_stderr,
					"task2 lost contact, cleaning ...\n");
			NEURAL_CONNECTION_free(task2->tengah_conn);
			if(!do_waitpid(task2->child_pid)) {
				if(verbose)
					SYS_fprintf(SYS_stderr, "task2 failed\n");
				return 1;
			}
			task2->child_pid = -1;
			if(verbose)
				SYS_fprintf(SYS_stderr, "task2 exited cleanly\n");
		}
		if((task1->child_pid == -1) && (task2->child_pid == -1))
			return 0;
	}
}

static int display_version(void)
{
	SYS_fprintf(SYS_stderr,
		"Mandor version %s (%s / %s)\n\n"
		"Distributed as part of the NeuroCache source package\n"
		"Copyright (C) 2019-2020 Hilman P. Alisabana, Jakarta.\n\n",
                HAVE_PACKAGE_VERSION, HAVE_HYSCMTERM_UUID, HAVE_HYSCMTERM_DATE);
}

static const char *usage_msg[] = {
"",
"Usage: mandor [options] <cmd1 ...> --[...] <cmd2 ...>",
"  where 'options' are from;",
"  -<h|help|?>      (display this help)",
"  -<v|version>     (display version information)",
"  -mandor <string> (change the symbol of mandor description from '--' to <string>)",
"  -safe <num>      (when pre-allocating pipes by mandor, use descriptors >= <num>)",
"  -verbose         (display verbose messages)",
"",
"Then various pipes descriptor by mandor will be set up between two commands",
"The symbol '--' is used for the pipe, though it can be overriden by using",
"the '-mandor' flag;",
"",
"   mandor prog1 -- prog2",
"   mandor prog1 --1\\>0,0\\<1 prog2",
"   mandor prog1 \"--1>0,0<1\" prog2",
"", NULL};

static int usage(void)
{
	const char **u = usage_msg;
	while(*u)
		SYS_fprintf(SYS_stderr, "%s\n", *(u++));
	return 0;
}

static const char *CMD_VERS1   = "-v";
static const char *CMD_VERS2   = "-version";
static const char *CMD_HELP1   = "-h";
static const char *CMD_HELP2   = "-help";
static const char *CMD_HELP3   = "-?";
static const char *CMD_MANDOR  = "-mandor";
static const char *CMD_SAFE    = "-safe";
static const char *CMD_VERBOSE = "-verbose";

static const char *desc_mandor_sep = "--";
static int def_fdsafe = 50;
static int def_verbose = 0;

static int err_noarg(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, %s requires an argument\n", arg);
	usage();
	return 1;
}
static int err_badrange(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, %s given an invalid argument\n", arg);
	usage();
	return 1;
}
static int err_badswitch(const char *arg)
{
	SYS_fprintf(SYS_stderr, "Error, \"%s\" not recognised\n", arg);
	usage();
	return 1;
}

static const char *mandor_sep = NULL;
#define IS_SEP(s) (strncmp((s), mandor_sep, strlen(mandor_sep)) == 0)
#define ARG_INC {argc--;argv++;}
#define ARG_CHECK(a) \
	if(argc < 2) \
		return err_noarg(a); \
	ARG_INC

int main(int argc, char *argv[])
{
	int fdsafe;
	mandor_rules rules;
	mandor_task task1, task2;
	enum {
		PARSE_TASK1,
		PARSE_TASK2
	} state = PARSE_TASK1;
	mandor_sep = desc_mandor_sep;
	fdsafe = def_fdsafe;
	verbose = def_verbose;

	ARG_INC;
	while(argc > 0) {
		if((*argv[0] != '-') || IS_SEP(*argv)) goto done;
		if((strcmp(*argv, CMD_VERS1) == 0) ||
				(strcmp(*argv, CMD_VERS2) == 0))
			return display_version();
		if((strcmp(*argv, CMD_HELP1) == 0) ||
				(strcmp(*argv, CMD_HELP2) == 0) ||
				(strcmp(*argv, CMD_HELP3) == 0))
			return usage();
		else if(strcmp(*argv, CMD_MANDOR) == 0) {
			ARG_INC;
			if(!argc) return err_noarg(CMD_MANDOR);
			mandor_sep = *argv;
		} else if(strcmp(*argv, CMD_SAFE) == 0) {
			ARG_INC;
			if(!argc) return err_noarg(CMD_SAFE);
			fdsafe = atoi(*argv);
			if((fdsafe < 3) || (fdsafe > 512))
				return err_badrange(CMD_SAFE);
		} else if(strcmp(*argv, CMD_VERBOSE) == 0)
			verbose++;
		else
			return err_badswitch(*argv);
		ARG_INC;
	}
done:
	if(!argc) {
		SYS_fprintf(SYS_stderr, "Error, no command to execute\n");
		return 1;
	}
	if(!SYS_sigpipe_ignore()) {
#if SYS_DEBUG_LEVEL > 0
		SYS_fprintf(SYS_stderr, "Error, couldn't ignore SIGPIPE\n");
#endif
		return 1;
	}
	if(!mandor_task_init(&task1) || !mandor_task_init(&task2)) return 1;
	while(argc) {
		switch(state) {
		case PARSE_TASK1:
			if(IS_SEP(*argv)) {
				if(!mandor_task_dimengerti(&task1))
					return 1;
				if(!mandor_rules_set(&rules, *argv + strlen(mandor_sep))) {
					SYS_fprintf(SYS_stderr, "Error, invalid mandor rules\n");
					return 1;
				}
				state = PARSE_TASK2;
				break;
			}
			if(!mandor_task_lahap(&task1, *argv))
				return 1;
			break;
		case PARSE_TASK2:
			if(!mandor_task_lahap(&task2, *argv))
				return 1;
			break;
		}
		ARG_INC;
	}
	if(!mandor_task_dimengerti(&task2))
		return 1;
	if(verbose) {
		SYS_fprintf(SYS_stderr, "Info task1: ");
		mandor_task_print(&task1);
		SYS_fprintf(SYS_stderr, "Info task2: ");
		mandor_task_print(&task2);
		SYS_fprintf(SYS_stderr, "Info mandor: ");
		mandor_rules_print(&rules);
	}
	if(!mandor_rules_build(&rules, fdsafe))
		return 1;
	if(!mandor_task_exec(&task1, &rules, NULL) || !mandor_task_exec(&task2, &rules, &task1))
		return 1;
	return do_waiting(&task1, &task2);
}

