/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_EXE

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "timing.h"
#include <libgen/post.h>

#define ECHO_DEBUG_CLIENTS

#define DEF_SERVER_ADDRESS	"UNIX:/tmp/foo"
#define BUFFER_SIZE		(32*1024)
#define MAX_CONNS		512
#define DEF_UNITS		UNITS_bits

#ifdef SUPPORT_UPDATE
IMPLEMENT_UNITS()
#endif

static void usage(void)
{
	SYS_fprintf(SYS_stderr,
"Usage:   neural_echo [options ...]\n"
"where options include;\n"
"   -accept <addr>      - default='%s'\n"
"   -max <num>          - default=%d\n"
"   -errinject <num>    - default=<none>\n"
"   -dump\n", DEF_SERVER_ADDRESS, MAX_CONNS);
#ifdef SUPPORT_UPDATE
	SYS_fprintf(SYS_stderr,
"   -update <secs>      - default=<none>\n"
"   -units [k|m|g]<b|B> - default='%s'\n"
"'units' displays traffic rates as bits or bytes per second.\n"
"An optional prefix can scale to kilo, mega, or giga bits/bytes.\n",
UNITS2STR(DEF_UNITS));
#endif
	SYS_fprintf(SYS_stderr,
"'errinject' will insert 0xdeadbeef into output every\n"
"<num> times the selector logic breaks.\n");
}

static int util_parsenum(const char *s, unsigned int *num)
{
	char *endptr;
	unsigned long int val;
	val = strtoul(s, &endptr, 10);
	if((val == ULONG_MAX) || !endptr || (*endptr != '\0')) {
		SYS_fprintf(SYS_stderr, "Error, bad number '%s'\n", s);
		return 0;
	}
	*num = val;
	return 1;
}

static int err_noarg(const char *s)
{
	SYS_fprintf(SYS_stderr, "Error: missing argument for '%s'\n", s);
	usage();
	return 1;
}

static int err_unknown(const char *s)
{
	SYS_fprintf(SYS_stderr, "Error: unknown switch '%s'\n", s);
	usage();
	return 1;
}

#define ARG_INC do {argc--;argv++;} while(0)
#define ARG_CHECK(a) \
	if(argc < 2) \
		return err_noarg(a); \
	ARG_INC

static unsigned int my_transfer(NEURAL_BUFFER *dest, NEURAL_BUFFER *src, int dump);

int main(int argc, char *argv[])
{
	int tmp;
	unsigned int loop = 0;
	unsigned conns_used = 0;
	NEURAL_CONNECTION *conn[MAX_CONNS];
	const char *str_addr = DEF_SERVER_ADDRESS;
	unsigned int num_conns = MAX_CONNS;
	unsigned int errinject = 0;
	int dump = 0;
	NEURAL_ADDRESS *addr;
	NEURAL_LISTENER *listener;
	NEURAL_SELECTOR *sel;
#ifdef SUPPORT_UPDATE
	unsigned int update = 0;
	UNITS units = DEF_UNITS;
	time_t tt1 = 0, tt2;
	struct timeval tv1, tv2;
	struct rusage ru1, ru2;
	unsigned int traffic = 0;
#endif

	ARG_INC;
	while(argc) {
		if(strcmp(*argv, "-accept") == 0) {
			ARG_CHECK("-accept");
			str_addr = *argv;
		} else if(strcmp(*argv, "-max") == 0) {
			ARG_CHECK("-max");
			if(!util_parsenum(*argv, &num_conns))
				return 1;
			if(!num_conns || (num_conns > MAX_CONNS)) {
				SYS_fprintf(SYS_stderr, "Error, '%d' is out of bounds "
					"for -max\n", num_conns);
				return 1;
			}
		} else if(strcmp(*argv, "-dump") == 0)
			dump = 1;
		else if(strcmp(*argv, "-errinject") == 0) {
			ARG_CHECK("-errinject");
			if(!util_parsenum(*argv, &errinject))
				return 1;
#ifdef SUPPORT_UPDATE
		} else if(strcmp(*argv, "-update") == 0) {
			ARG_CHECK("-update");
			if(!util_parsenum(*argv, &update))
				return 1;
		} else if(strcmp(*argv, "-units") == 0) {
			ARG_CHECK("-units");
			if(!util_parseunits(*argv, &units))
				return 1;
#endif
		} else
			return err_unknown(*argv);
		ARG_INC;
	}
	SYS_sigpipe_ignore();
	addr = NEURAL_ADDRESS_new();
	listener = NEURAL_LISTENER_new();
	sel = NEURAL_SELECTOR_new();
	if(!addr || !listener || !sel) abort();
	while(loop < num_conns)
		if((conn[loop++] = NEURAL_CONNECTION_new()) == NULL)
			abort();
	if(!NEURAL_ADDRESS_create(addr, str_addr, BUFFER_SIZE)) abort();
	if(!NEURAL_LISTENER_create(listener, addr)) abort();
	if(!NEURAL_LISTENER_add_to_selector(listener, sel)) abort();
#ifdef SUPPORT_UPDATE
	if(update) {
		tt1 = time(NULL);
		SYS_gettime(&tv1);
		getrusage(RUSAGE_SELF, &ru1);
		SYS_fprintf(SYS_stderr,
"\n"
"Note, '-update' statistics have accurate timing but the traffic measurements\n"
"are based on transfers between user-space fifo buffers. As such, they should\n"
"only be considered accurate \"on average\". Also, the traffic measured is\n"
"two-way, identical traffic is passing in both directions so you can consider\n"
"each direction to be half the advertised throughput value.\n"
"\n");
	}
#endif
reselect:
	tmp = NEURAL_SELECTOR_select(sel, 0, 0);
	if(tmp <= 0) {
		SYS_fprintf(SYS_stderr, "Error, NEURAL_SELECTOR_select() returned 0\n");
		return 1;
	}
#ifdef SUPPORT_UPDATE
	if(update && ((tt2 = time(NULL)) >= (time_t)(tt1 + update))) {
		unsigned long msecs, muser, msys;
		double rate;
		SYS_gettime(&tv2);
		getrusage(RUSAGE_SELF, &ru2);
		msecs = SYS_msecs_between(&tv1, &tv2);
		muser = SYS_msecs_between(&ru1.ru_utime, &ru2.ru_utime);
		msys = SYS_msecs_between(&ru1.ru_stime, &ru2.ru_stime);
		rate = util_tounits(traffic, units);
		rate = 2000.0 * rate / (double)msecs;
		SYS_fprintf(SYS_stdout, "Update: %ld msecs elapsed, %.2f %s/s, "
			"%.1f%% user, %.1f%% kernel\n", msecs, rate,
			UNITS2STR(units), (100.0 * muser)/((float)msecs),
			(100.0 * msys)/((float)msecs));
		tt1 = tt2;
		SYS_timecpy(&tv1, &tv2);
		SYS_memcpy(struct rusage, &ru1, &ru2);
		traffic = 0;
	}
#endif
	while((conns_used < num_conns) && NEURAL_CONNECTION_accept(conn[conns_used],
						listener)) {
		if(!NEURAL_CONNECTION_add_to_selector(conn[conns_used], sel))
			abort();
		conns_used++;
#ifdef ECHO_DEBUG_CLIENTS
		SYS_fprintf(SYS_stderr, "ECHO: added a conn (now have %d)\n",
			conns_used);
#endif
		if(conns_used == num_conns)
			NEURAL_LISTENER_del_from_selector(listener);
	}
	for(loop = 0; loop < conns_used; ) {
		if(!NEURAL_CONNECTION_io(conn[loop])) {
			NEURAL_CONNECTION_reset(conn[loop]);
			conns_used--;
#ifdef ECHO_DEBUG_CLIENTS
			SYS_fprintf(SYS_stderr, "ECHO: removed a conn (now have %d)\n",
				conns_used);
#endif
			if((conns_used + 1) == num_conns)
				if(!NEURAL_LISTENER_add_to_selector(listener, sel))
					abort();
			if(loop < conns_used) {
				NEURAL_CONNECTION *ptmp = conn[loop];
				conn[loop] = conn[conns_used];
				conn[conns_used] = ptmp;
			}
		} else {
			static unsigned int inject = 0;
			static const unsigned char deadbeef[] =
				{ 0xde, 0xad, 0xbe, 0xef };
			NEURAL_BUFFER *buf_send = NEURAL_CONNECTION_get_send(conn[loop]);
			NEURAL_BUFFER *buf_read = NEURAL_CONNECTION_get_read(conn[loop]);
			if(NEURAL_BUFFER_unused(buf_send) >= sizeof(deadbeef)) {
				if(errinject && (++inject == errinject)) {
					NEURAL_BUFFER_write(buf_send, deadbeef,
						sizeof(deadbeef));
					inject = 0;
				}
				traffic += my_transfer(buf_send, buf_read, dump);
			}
			loop++;
		}
	}
	if(NEURAL_LISTENER_finished(listener)) {
		NEURAL_LISTENER_del_from_selector(listener);
		if(!conns_used)
			return 0;
	}
	goto reselect;
}

static void bindump(const unsigned char *data, unsigned int len)
{
#define LINEWIDTH 16
	unsigned int tot = 0, pos = 0;
	while(len--) {
		if(!pos)
			SYS_fprintf(SYS_stdout, "%04d: ", tot);
		SYS_fprintf(SYS_stdout, "0x%02x ", *(data++));
		if(++pos == LINEWIDTH) {
			SYS_fprintf(SYS_stdout, "\n");
			pos = 0;
		}
		tot++;
	}
	if(pos)
		SYS_fprintf(SYS_stdout, "\n");
}

static unsigned int my_transfer(NEURAL_BUFFER *dest, NEURAL_BUFFER *src, int dump)
{
	const unsigned char *sptr;
	unsigned int len;
	if(!dump) return NEURAL_BUFFER_transfer(dest, src, 0);
	len = NEURAL_BUFFER_used(src);
	if(len > NEURAL_BUFFER_unused(dest)) len = NEURAL_BUFFER_unused(dest);
	if(!len) return 0;
	sptr = NEURAL_BUFFER_data(src);
	SYS_fprintf(SYS_stderr, "transferring data (%d bytes):\n", len);
	bindump(sptr, len);
	if((NEURAL_BUFFER_write(dest, sptr, len) != len) ||
			(NEURAL_BUFFER_read(src, NULL, len) != len)) {
		SYS_fprintf(SYS_stderr, "Error, internal bug!\n");
		abort();
	}
	return len;
}

