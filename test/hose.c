/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_EXE

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <libgen/post.h>

#define DEF_SERVER_ADDRESS	"UNIX:/tmp/foo"
#define BUFFER_SIZE		(32*1024)
#define DEF_CHUNK_SIZE		2051
#define MIN_CHUNK_SIZE		10
#define DEF_CHUNK_LAG		0
#define DEF_PING_NUM		0
#define DEF_NUM_CONNS		1

static void usage(void)
{
	SYS_fprintf(SYS_stderr,
"Usage:   neural_hose [options ...]\n"
"where options include;\n"
"   -connect <addr>   - default='%s'\n"
"   -num <num>        - default=%d\n"
"   -size <num>       - default=%d\n"
"   -repeat <num>     - default=%d\n"
"   -lag <num>        - default=%d\n"
"   -quiet\n",
DEF_SERVER_ADDRESS, DEF_NUM_CONNS, DEF_CHUNK_SIZE, DEF_PING_NUM, DEF_CHUNK_LAG);
}

static int util_parsenum(const char *s, unsigned int *num)
{
	char *endptr;
	unsigned long int val;
	val = strtoul(s, &endptr, 10);
	if((val == ULONG_MAX) || !endptr || (*endptr != '\0')) {
		SYS_fprintf(SYS_stderr, "Error, bad number '%s'\n", s);
		return 0;
	}
	*num = val;
	return 1;
}

static int err_noarg(const char *s)
{
	SYS_fprintf(SYS_stderr, "Error: missing argument for '%s'\n", s);
	usage();
	return 1;
}

static int err_unknown(const char *s)
{
	SYS_fprintf(SYS_stderr, "Error: unknown switch '%s'\n", s);
	usage();
	return 1;
}

#define ARG_INC do {argc--;argv++;} while(0)
#define ARG_CHECK(a) \
	if(argc < 2) \
		return err_noarg(a); \
	ARG_INC

typedef struct st_pingctx {
	int connected, id, done, quiet;
	NEURAL_CONNECTION *conn;
	unsigned int num_read, num_written, repeat, size, lag;
} pingctx;

static int pingctx_io(pingctx *ctx);

static pingctx *pingctx_new(const NEURAL_ADDRESS *addr, NEURAL_SELECTOR *sel, int id,
			unsigned int repeat, unsigned int size,
			unsigned int lag, unsigned int quiet)
{
	pingctx *ret = SYS_malloc(pingctx, 1);
	if(!ret) goto err;
	ret->conn = NEURAL_CONNECTION_new();
	if(!ret->conn) goto err;
	if(!NEURAL_CONNECTION_create(ret->conn, addr)) goto err;
	if(!NEURAL_CONNECTION_add_to_selector(ret->conn, sel)) goto err;
	ret->connected = 0;
	ret->id = id;
	ret->done = 0;
	ret->num_read = 0;
	ret->num_written = 0;
	ret->repeat = repeat;
	ret->size = size;
	ret->lag = lag;
	ret->quiet = quiet;
	if(!pingctx_io(ret)) goto err;
	return ret;
err:
	abort();
	if(ret) {
		if(ret->conn) NEURAL_CONNECTION_free(ret->conn);
		SYS_free(pingctx, ret);
	}
	return NULL;
}

static void pingctx_free(pingctx *ctx)
{
	NEURAL_CONNECTION_free(ctx->conn);
	SYS_free(pingctx, ctx);
}

static unsigned int uint32_read(const unsigned char **p)
{
	unsigned int tmp = *((*p)++);
	tmp = (tmp << 8) | *((*p)++);
	tmp = (tmp << 8) | *((*p)++);
	tmp = (tmp << 8) | *((*p)++);
	return tmp;
}

static void uint32_write(unsigned char **p, unsigned int val)
{
	*((*p)++) = (val >> 24) & 0xFF;
	*((*p)++) = (val >> 16) & 0xFF;
	*((*p)++) = (val >> 8) & 0xFF;
	*((*p)++) = val & 0xFF;
}

static int pingctx_io(pingctx *ctx)
{
	const unsigned char *cdata;
	unsigned int num, seed, loop, base, mult, total, used;
	unsigned char *p;
	if(ctx->done) return 1;
	if(!NEURAL_CONNECTION_io(ctx->conn)) {
		if(!ctx->connected)
			SYS_fprintf(SYS_stderr, "(%d) Connection failed\n", ctx->id);
		else
			SYS_fprintf(SYS_stderr, "(%d) Disconnection\n", ctx->id);
		return 0;
	}
	if(!ctx->connected) {
		if(!NEURAL_CONNECTION_is_established(ctx->conn))
			return 1;
		ctx->connected = 1;
		goto write_ping;
	}

	total = NEURAL_BUFFER_used(NEURAL_CONNECTION_get_read(ctx->conn));
	cdata = NEURAL_BUFFER_data(NEURAL_CONNECTION_get_read(ctx->conn));
	used = 0;
	while((total - used) >= ctx->size) {
		ctx->num_read++;
		num = uint32_read(&cdata);
		seed = uint32_read(&cdata);
		if(num != ctx->num_read) {
			SYS_fprintf(SYS_stderr, "(%d) Read error: bad chunk header\n", ctx->id);
			return 0;
		}
		srand(seed);
		loop = ctx->size - 8;
		base = (int)(65536.0 * rand()/(RAND_MAX+1.0));
		mult = 1 + (int)(65536.0 * rand()/(RAND_MAX+1.0));
		do {
			base *= mult;
			base += mult;
			if(*(cdata++) != ((base >> 24) ^ (base & 0xFF))) {
				SYS_fprintf(SYS_stderr, "(%d) Read error: bad match at "
					"offset %d\n", ctx->id, ctx->size - loop);
				return 0;
			}
		} while(--loop);
		used += ctx->size;
		if(!ctx->quiet)
			SYS_fprintf(SYS_stdout, "(%d) Packet %d read ok\n",
					ctx->id, ctx->num_read);
		if(ctx->num_read == ctx->repeat) {
			ctx->done = 1;
			NEURAL_CONNECTION_reset(ctx->conn);
			return 1;
		}
	}
	if(used && NEURAL_BUFFER_read(NEURAL_CONNECTION_get_read(ctx->conn),
					NULL, used) != used) {
		SYS_fprintf(SYS_stderr, "(%d) Read error\n", ctx->id);
		return 0;
	}

write_ping:
	total = NEURAL_BUFFER_unused(NEURAL_CONNECTION_get_send(ctx->conn));
	p = NEURAL_BUFFER_write_ptr(NEURAL_CONNECTION_get_send(ctx->conn));
	used = 0;
	while((total - used) >= ctx->size) {
		if(ctx->num_written == ctx->repeat) break;
		if(ctx->num_read + ctx->lag < ctx->num_written) break;
		ctx->num_written++;
		seed = ctx->num_written + time(NULL);
		seed &= 0xFFFF;
		uint32_write(&p, ctx->num_written);
		uint32_write(&p, seed);
		srand(seed);
		loop = ctx->size - 8;
		base = (int)(65536.0 * rand()/(RAND_MAX+1.0));
		mult = 1 + (int)(65536.0 * rand()/(RAND_MAX+1.0));
		do {
			base *= mult;
			base += mult;
			*(p++) = (base >> 24) ^ (base & 0xFF);
		} while(--loop);
		used += ctx->size;
	}
	if(used)
		NEURAL_BUFFER_wrote(NEURAL_CONNECTION_get_send(ctx->conn), used);
	return 1;
}

int main(int argc, char *argv[])
{
	int tmp, ret = 1;
	unsigned int loop, done;
	pingctx **ctx;
	const char *str_addr = DEF_SERVER_ADDRESS;
	unsigned int repeat = DEF_PING_NUM;
	unsigned int size = DEF_CHUNK_SIZE;
	unsigned int lag = DEF_CHUNK_LAG;
	unsigned int num_conns = DEF_NUM_CONNS;
	int quiet = 0;
	NEURAL_ADDRESS *addr = NEURAL_ADDRESS_new();
	NEURAL_SELECTOR *sel = NEURAL_SELECTOR_new();
	if(!addr || !sel) abort();
	ARG_INC;
	while(argc) {
		if(strcmp(*argv, "-connect") == 0) {
			ARG_CHECK("-connect");
			str_addr = *argv;
		} else if(strcmp(*argv, "-num") == 0) {
			ARG_CHECK("-num");
			if(!util_parsenum(*argv, &num_conns))
				return 1;
		} else if(strcmp(*argv, "-repeat") == 0) {
			ARG_CHECK("-repeat");
			if(!util_parsenum(*argv, &repeat))
				return 1;
		} else if(strcmp(*argv, "-lag") == 0) {
			ARG_CHECK("-lag");
			if(!util_parsenum(*argv, &lag))
				return 1;
		} else if(strcmp(*argv, "-size") == 0) {
			ARG_CHECK("-size");
			if(!util_parsenum(*argv, &size))
				return 1;
			if(!size || (size < MIN_CHUNK_SIZE) ||
						(size > BUFFER_SIZE)) {
				SYS_fprintf(SYS_stderr, "Error, '%d' is "
					"out of range\n", size);
				return 1;
			}
		} else if(strcmp(*argv, "-quiet") == 0) {
			quiet = 1;
		} else
			return err_unknown(*argv);
		ARG_INC;
	}
	if((ctx = SYS_malloc(pingctx*, num_conns)) == NULL) abort();
	if(!NEURAL_ADDRESS_create(addr, str_addr, BUFFER_SIZE)) abort();
	for(loop = 0; loop < num_conns; loop++)
		if((ctx[loop] = pingctx_new(addr, sel, loop, repeat,
				size, lag, quiet)) == NULL)
			abort();
	do {
		if((tmp = NEURAL_SELECTOR_select(sel, 0, 0)) <= 0) {
			SYS_fprintf(SYS_stderr, "Error, NEURAL_SELECTOR_select() "
				"returned <= 0\n");
			goto err;
		}
		done = 0;
		for(loop = 0; loop < num_conns; loop++) {
			if(!pingctx_io(ctx[loop]))
				goto err;
			if(ctx[loop]->done)
				done++;
		}
	} while((done < num_conns) || NEURAL_SELECTOR_num_objects(sel));
	ret = 0;
err:
	for(loop = 0; loop < num_conns; loop++)
		pingctx_free(ctx[loop]);
	SYS_free(pingctx*, ctx);
	NEURAL_SELECTOR_free(sel);
	NEURAL_ADDRESS_free(addr);
	return ret;
}
