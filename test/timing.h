/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_PRIVATE_TIMING_H
#define HEADER_PRIVATE_TIMING_H

#if defined(HAVE_GETTIMEOFDAY) && defined(HAVE_GETRUSAGE)
#define SUPPORT_UPDATE
#endif

typedef unsigned char UNITS;

#define IMPLEMENT_UNITS() \
		static const char *UNITS_str[] = \
		{ "b", "Kb", "Mb", "Gb", "B", "KB", "MB", "GB" }; \
		static int util_parseunits(const char *s, UNITS *u) \
		{ \
			const char *foo = s; \
			*u = UNITS_bits; \
			switch(strlen(s)) { \
			case 2: \
				switch(*foo) { \
				case 'k': *u |= UNITS_kilo; break; \
				case 'm': *u |= UNITS_mega; break; \
				case 'g': *u |= UNITS_giga; break; \
				default: goto err; \
				} \
				foo++; \
			case 1: \
				switch(*foo) { \
				case 'b': *u |= UNITS_bits; break; \
				case 'B': *u |= UNITS_bytes; break; \
				default: goto err; \
				} \
				break; \
			default: \
				goto err; \
			} \
			return 1; \
		err: \
			SYS_fprintf(SYS_stderr, "Error, bad unit '%s'\n", s); \
			return 0; \
		} \
		static double util_tounits(unsigned long traffic, UNITS units) \
		{ \
			double ret = traffic; \
			if(!(units & UNITS_bytes)) ret *= 8; \
			switch(units & UNITS_mask) { \
			case UNITS_giga: ret /= 1024; \
			case UNITS_mega: ret /= 1024; \
			case UNITS_kilo: ret /= 1024; \
			case 0: break; \
			default: abort(); \
			} \
			return ret; \
		}

#define UNITS_bits	(UNITS)0
#define UNITS_bytes	(UNITS)4
#define UNITS_kilo	(UNITS)1
#define UNITS_mega	(UNITS)2
#define UNITS_giga	(UNITS)3
#define UNITS_mask	(UNITS)3

#define UNITS2STR(u)		UNITS_str[(u)]

#endif
