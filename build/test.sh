#!/bin/sh

BAILOUT=no
BAILREASON=""
THISDIR=`pwd`
NSC_SERVER_PROG="$THISDIR/ncsess-server/nsc_server"
NSC_SERVER_UNIX="$THISDIR/unix.nsc_server"
NSC_SERVER_PID="$THISDIR/pid.nsc_server"
NSC_CLIENT_PROG="$THISDIR/ncsess-client/nsc_client"
NSC_CLIENT_UNIX="$THISDIR/unix.nsc_client"
NSC_CLIENT_PID="$THISDIR/pid.nsc_client"
NSC_TEST="$THISDIR/test/nsc_test -timeout 30 -timevar 10"

NSC_SERVER="$NSC_SERVER_PROG -listen UNIX:$NSC_SERVER_UNIX -pidfile $NSC_SERVER_PID -daemon"
NSC_CLIENT="$NSC_CLIENT_PROG -listen UNIX:$NSC_CLIENT_UNIX -pidfile $NSC_CLIENT_PID -daemon -server UNIX:$NSC_SERVER_UNIX"

cleanup() {
	if [ -f "$NSC_SERVER_PID" ]; then
		kill `cat $NSC_SERVER_PID` || echo "couldn't kill 'nsc_server'!"
	fi
	if [ -f "$NSC_CLIENT_PID" ]; then
		kill `cat $NSC_CLIENT_PID` || echo "couldn't kill 'nsc_client'!"
	fi
	rm -f $NSC_SERVER_PID $NSC_SERVER_UNIX
	rm -f $NSC_CLIENT_PID $NSC_CLIENT_UNIX
}

bang() {
	echo "Bailing out:$BAILREASON"
	cleanup
	exit 1
}

# run_test $1 $2 $3
# $1: number of operations
# $2: "server" or "client" (target address)
# $3: "temporary" or "persistent"  (whether to use -persistent)
run_test() {
	text="$1 random operations"
	cmd="$NSC_TEST -ops $1 -connect UNIX:"
	if [ "$2" = "server" ]; then
		text="$text direct to"
		cmd="$cmd$NSC_SERVER_UNIX"
	else
		text="$text through"
		cmd="$cmd$NSC_CLIENT_UNIX"
	fi
	text="$text nsc_$2 ($3 connections) ... "
	if [ "$3" = "persistent" ]; then
		cmd="$cmd -persistent"
	fi
	printf "%s" "$text"
	$cmd 1> /dev/null 2> /dev/null && echo "SUCCESS" && return 0
	echo "FAILED"
	return 1
}

if [ ! -x "$NSC_SERVER_PROG" ]; then
	echo "Either you haven't compiled NeuroCache source or you are executing"
	echo "from the wrong directory. Please run this script from the top-level"
	echo "directory, ie;"
	echo "   ./build/test.sh"
	BAILOUT=yes
	BAILREASON="$BAILREASON ($NSC_SERVER_PROG not found)"
fi

if [ -f "$NSC_SERVER_PID" ]; then
	BAILOUT=yes
	BAILREASON="$BAILREASON (pid.nsc_server exists)"
fi

if [ -S "$NSC_SERVER_UNIX" ]; then
	BAILOUT=yes
	BAILREASON="$BAILREASON (unix.nsc_server exists)"
fi

if [ "x$BAILOUT" != "xno" ]; then
	bang
fi

printf "Starting nsc_server daemon on %s ... " "$NSC_SERVER_UNIX"
$NSC_SERVER 1> /dev/null 2> /dev/null || (echo "FAILED" && exit 1) || exit 1
echo "SUCCESS"
printf "Starting nsc_client daemon on %s ... " "$NSC_CLIENT_UNIX"
$NSC_CLIENT 1> /dev/null 2> /dev/null || (echo "FAILED" && exit 1) || exit 1
echo "SUCCESS"

echo ""

sleep 1

run_test 8000 server temporary
run_test 8000 server persistent

run_test 8000 client temporary
run_test 8000 client persistent

cleanup

