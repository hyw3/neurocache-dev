/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include <libgen/post.h>

#include <assert.h>

struct st_NEURAL_CONNECTION {
	const NEURAL_CONNECTION_vtable *vt;
	void *vt_data;
	size_t vt_data_size;
	const NEURAL_CONNECTION_vtable *reset;
	NEURAL_SELECTOR *sel;
	NEURAL_SELECTOR_TOKEN sel_token;
};

int neural_connection_set_vtable(NEURAL_CONNECTION *a, const NEURAL_CONNECTION_vtable *vtable)
{
	if(a->vt) {
		if(a->vt->pre_close)
			a->vt->pre_close(a);
		if(a->sel) NEURAL_CONNECTION_del_from_selector(a);
		a->vt->on_reset(a);
		a->reset = a->vt;
		a->vt = NULL;
	}
	if(a->reset && (a->reset != vtable)) {
		a->reset->on_destroy(a);
		a->reset = NULL;
		SYS_zero_n(unsigned char, a->vt_data, a->vt_data_size);
	}
	if(vtable->vtdata_size > a->vt_data_size) {
		assert(a->reset == NULL);
		if(a->vt_data)
			SYS_free(void, a->vt_data);
		a->vt_data = SYS_malloc(unsigned char, vtable->vtdata_size);
		if(!a->vt_data) {
			a->vt_data_size = 0;
			return 0;
		}
		a->vt_data_size = vtable->vtdata_size;
		SYS_zero_n(unsigned char, a->vt_data, vtable->vtdata_size);
	}
	if(vtable->on_create(a)) {
		a->vt = vtable;
		return 1;
	}
	return 0;
}

const NEURAL_CONNECTION_vtable *neural_connection_get_vtable(const NEURAL_CONNECTION *conn)
{
	return conn->vt;
}

void *neural_connection_get_vtdata(const NEURAL_CONNECTION *conn)
{
	return conn->vt_data;
}

void neural_connection_pre_select(NEURAL_CONNECTION *conn)
{
	assert((conn->sel != NULL) && (conn->vt != NULL));
	conn->vt->pre_select(conn, conn->sel, conn->sel_token);
}

void neural_connection_post_select(NEURAL_CONNECTION *conn)
{
	assert((conn->sel != NULL) && (conn->vt != NULL));
	conn->vt->post_select(conn, conn->sel, conn->sel_token);
}

NEURAL_SELECTOR *neural_connection_get_selector(const NEURAL_CONNECTION *c,
					NEURAL_SELECTOR_TOKEN *t)
{
	if(c->sel && t) *t = c->sel_token;
	return c->sel;
}

void neural_connection_set_selector_raw(NEURAL_CONNECTION *c, NEURAL_SELECTOR *sel,
					NEURAL_SELECTOR_TOKEN token)
{
	c->sel = sel;
	c->sel_token = token;
}

NEURAL_CONNECTION *NEURAL_CONNECTION_new(void)
{
	NEURAL_CONNECTION *conn = SYS_malloc(NEURAL_CONNECTION, 1);
	if(conn) {
		conn->vt = NULL;
		conn->vt_data = NULL;
		conn->vt_data_size = 0;
		conn->reset = NULL;
		conn->sel = NULL;
		conn->sel_token = NULL;
	}
	return conn;
}

void NEURAL_CONNECTION_free(NEURAL_CONNECTION *conn)
{
	if(conn->vt && conn->vt->pre_close) conn->vt->pre_close(conn);
	if(conn->sel) NEURAL_CONNECTION_del_from_selector(conn);
	if(conn->vt) conn->vt->on_destroy(conn);
	else if(conn->reset) conn->reset->on_destroy(conn);
	if(conn->vt_data) SYS_free(void, conn->vt_data);
	SYS_free(NEURAL_CONNECTION, conn);
}

void NEURAL_CONNECTION_reset(NEURAL_CONNECTION *conn)
{
	if(conn->vt && conn->vt->pre_close) conn->vt->pre_close(conn);
	if(conn->vt) {
		if(conn->sel) NEURAL_CONNECTION_del_from_selector(conn);
		conn->vt->on_reset(conn);
		conn->reset = conn->vt;
		conn->vt = NULL;
	}
}

int NEURAL_CONNECTION_create(NEURAL_CONNECTION *conn, const NEURAL_ADDRESS *addr)
{
	const NEURAL_CONNECTION_vtable *vtable;
	if(conn->vt || !NEURAL_ADDRESS_can_connect(addr))
		return 0;
	if((vtable = neural_address_get_connection(addr)) == NULL)
		return 0;
	if(!neural_connection_set_vtable(conn, vtable) ||
			!vtable->connect(conn, addr)) {
		NEURAL_CONNECTION_reset(conn);
		return 0;
	}
	return 1;
}

int NEURAL_CONNECTION_accept(NEURAL_CONNECTION *conn, NEURAL_LISTENER *list)
{
	const NEURAL_CONNECTION_vtable *vtable;
	if(conn->vt) return 0;
	if((vtable = neural_listener_pre_accept(list)) == NULL)
		return 0;
	if(!neural_connection_set_vtable(conn, vtable) ||
			!vtable->accept(conn, list)) {
		NEURAL_CONNECTION_reset(conn);
		return 0;
	}
	return 1;
}

int NEURAL_CONNECTION_set_size(NEURAL_CONNECTION *conn, unsigned int size)
{
	if(!neural_check_buffer_size(size))
		return 0;
	if(conn->vt) return conn->vt->set_size(conn, size);
	return 0;
}

NEURAL_BUFFER *NEURAL_CONNECTION_get_read(NEURAL_CONNECTION *conn)
{
	if(conn->vt) return conn->vt->get_read(conn);
	return NULL;
}

NEURAL_BUFFER *NEURAL_CONNECTION_get_send(NEURAL_CONNECTION *conn)
{
	if(conn->vt) return conn->vt->get_send(conn);
	return NULL;
}

const NEURAL_BUFFER *NEURAL_CONNECTION_get_read_c(const NEURAL_CONNECTION *conn)
{
	if(conn->vt) return conn->vt->get_read(conn);
	return NULL;
}

const NEURAL_BUFFER *NEURAL_CONNECTION_get_send_c(const NEURAL_CONNECTION *conn)
{
	if(conn->vt) return conn->vt->get_send(conn);
	return NULL;
}

int NEURAL_CONNECTION_io(NEURAL_CONNECTION *conn)
{
	if(conn->vt && conn->sel)
		return conn->vt->do_io(conn);
	return 0;
}

int NEURAL_CONNECTION_is_established(const NEURAL_CONNECTION *conn)
{
	if(conn->vt) return conn->vt->is_established(conn);
	return 0;
}

int NEURAL_CONNECTION_add_to_selector(NEURAL_CONNECTION *conn,
				NEURAL_SELECTOR *sel)
{
	if(conn->sel || !conn->vt || !conn->vt->pre_selector_add(conn, sel))
		return 0;
	if((conn->sel_token = neural_selector_add_connection(sel, conn)) ==
				NEURAL_SELECTOR_TOKEN_NULL) {
		conn->vt->post_selector_del(conn, sel);
		return 0;
	}
	conn->sel = sel;
	if(conn->vt->post_selector_add && !conn->vt->post_selector_add(conn,
				sel, conn->sel_token)) {
		NEURAL_CONNECTION_del_from_selector(conn);
		return 0;
	}
	return 1;
}

void NEURAL_CONNECTION_del_from_selector(NEURAL_CONNECTION *conn)
{
	if(conn->vt && conn->sel) {
		NEURAL_SELECTOR *sel = conn->sel;
		if(conn->vt->pre_selector_del)
			conn->vt->pre_selector_del(conn, sel, conn->sel_token);
		neural_selector_del_connection(conn->sel, conn, conn->sel_token);
		conn->sel = NULL;
		conn->sel_token = NULL;
		conn->vt->post_selector_del(conn, sel);
	}
}

