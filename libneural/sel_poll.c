/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include "ctrl_fd.h"
#include <libgen/post.h>

#include <assert.h>

#ifndef HAVE_POLL

const NEURAL_SELECTOR_vtable *sel_fdpoll(void)
{
	return NULL;
}

NEURAL_SELECTOR *NEURAL_SELECTOR_new_fdpoll(void)
{
	return NULL;
}

#else

typedef struct st_sel_obj {
	union {
		NEURAL_CONNECTION *conn;
		NEURAL_LISTENER *listener;
	} obj;
	unsigned char what;
        /* This looks like this:
         *   0 = unused
         *   1 = conn
         *   2 = listener
         * In particular we use token/idx conversion on the neural
         * selector implementations so that we can invoke this in a way
         * they callback to us whilst processing their hooks and use
         * the following logic for pre/post neural select.
         */
        unsigned int idx_init, idx_total;
} sel_obj;

typedef struct st_sel_ctx {
	struct pollfd *pollfds;
	unsigned int pfds_used, pfds_size;
	sel_obj *obj_table;
	unsigned int obj_used, obj_size;
        /* Ensure that we only get callbacks for tokens of the object we're
         * hooking during both pre and post neural select */
	NEURAL_SELECTOR_TOKEN hook_current;
} sel_ctx;

#define OBJ_TABLE_START		32
#define POLLFD_TABLE_START	4
/* Use uintptr_t to cast to long pointer instead of unsigned int */
#define TOKENIFY(idx) (NEURAL_SELECTOR_TOKEN)((uintptr_t)idx | 0x8000)
#define IDXIFY(tok) ((uintptr_t)tok & 0x7FFF)

static void obj_table_zilch(sel_obj *items, unsigned int num)
{
	while(num--)
		(items++)->what = 0;
}

static int obj_table_init(sel_ctx *ctx)
{
	ctx->obj_table = SYS_malloc(sel_obj, OBJ_TABLE_START);
	if(!ctx->obj_table) return 0;
	ctx->pollfds = SYS_malloc(struct pollfd, POLLFD_TABLE_START);
	if(!ctx->pollfds) {
		SYS_free(sel_obj, ctx->obj_table);
		return 0;
	}
	obj_table_zilch(ctx->obj_table, OBJ_TABLE_START);
	ctx->obj_used = 0;
	ctx->obj_size = OBJ_TABLE_START;
	ctx->pfds_used = 0;
	ctx->pfds_size = POLLFD_TABLE_START;
	return 1;
}

static void obj_table_finish(sel_ctx *ctx)
{
	if(ctx->obj_used)
		SYS_fprintf(SYS_stderr, "Warning, selector destruction leaves "
				"dangling objects\n");
	SYS_free(sel_obj, ctx->obj_table);
	SYS_free(struct pollfd, ctx->pollfds);
}

static void obj_table_reset(sel_ctx *ctx)
{
	if(ctx->obj_used)
		SYS_fprintf(SYS_stderr, "Warning, selector reset leaves "
				"dangling objects\n");
	ctx->obj_used = 0;
	ctx->pfds_used = 0;
}

static int pollfds_expand(sel_ctx *ctx)
{
	struct pollfd *newitems;
	unsigned int newsize;
	if(ctx->pfds_used < ctx->pfds_size)
		return 1;
	newsize = ctx->pfds_size * 3 / 2;
	newitems = SYS_malloc(struct pollfd, newsize);
	if(!newitems) return 0;
	if(ctx->pfds_used)
		SYS_memcpy_n(struct pollfd, newitems, ctx->pollfds,
				ctx->pfds_used);
	SYS_free(struct pollfd, ctx->pollfds);
	ctx->pollfds = newitems;
	ctx->pfds_size = newsize;
	return 1;
}

static int obj_table_add(sel_ctx *ctx)
{
	int loop = 0;
	if(ctx->obj_used == ctx->obj_size) {
		unsigned int newsize = ctx->obj_size * 3 / 2;
		sel_obj *newitems = SYS_malloc(sel_obj, newsize);
		if(!newitems) return -1;
		obj_table_zilch(newitems, newsize);
		if(ctx->obj_used)
			SYS_memcpy_n(sel_obj, newitems, ctx->obj_table,
					ctx->obj_used);
		SYS_free(sel_obj, ctx->obj_table);
		ctx->obj_table = newitems;
		ctx->obj_size = newsize;
	}
	while(ctx->obj_table[loop].what != 0)
		loop++;
	ctx->obj_used++;
	return loop;
}

static NEURAL_SELECTOR_TOKEN obj_table_add_listener(sel_ctx *ctx,
                                                    NEURAL_LISTENER *listener)
{
	int loc = obj_table_add(ctx);
	if(loc < 0) return NEURAL_SELECTOR_TOKEN_NULL;
	ctx->obj_table[loc].what = 2;
	ctx->obj_table[loc].obj.listener = listener;
	return TOKENIFY(loc);
}

static NEURAL_SELECTOR_TOKEN obj_table_add_connection(sel_ctx *ctx,
                                                      NEURAL_CONNECTION *conn)
{
	int loc = obj_table_add(ctx);
	if(loc < 0) return NEURAL_SELECTOR_TOKEN_NULL;
	ctx->obj_table[loc].what = 1;
	ctx->obj_table[loc].obj.conn = conn;
	return TOKENIFY(loc);
}

static void obj_table_del(sel_ctx *ctx, NEURAL_SELECTOR_TOKEN tok)
{
	unsigned int idx = IDXIFY(tok);
	assert(idx < ctx->obj_size);
	assert(ctx->obj_table[idx].what != 0);
	assert(ctx->obj_used > 0);
	ctx->obj_table[idx].what = 0;
	ctx->obj_used--;
}

static void obj_table_pre_select(sel_ctx *ctx);
static void obj_table_post_select(sel_ctx *ctx);
static int sel_on_create(NEURAL_SELECTOR *);
static void sel_on_destroy(NEURAL_SELECTOR *);
static void sel_on_reset(NEURAL_SELECTOR *);
static NEURAL_SELECTOR_TYPE sel_get_type(const NEURAL_SELECTOR *);
static int sel_select(NEURAL_SELECTOR *, unsigned long usec_timeout, int use_timeout);
static unsigned int sel_num_objects(const NEURAL_SELECTOR *);
static NEURAL_SELECTOR_TOKEN sel_add_listener(NEURAL_SELECTOR *, NEURAL_LISTENER *);
static NEURAL_SELECTOR_TOKEN sel_add_connection(NEURAL_SELECTOR *, NEURAL_CONNECTION *);
static void sel_del_listener(NEURAL_SELECTOR *, NEURAL_LISTENER *, NEURAL_SELECTOR_TOKEN);
static void sel_del_connection(NEURAL_SELECTOR *, NEURAL_CONNECTION *, NEURAL_SELECTOR_TOKEN);
static int sel_ctrl(NEURAL_SELECTOR *, int, void *);
static const NEURAL_SELECTOR_vtable sel_fdpoll_vtable = {
	sizeof(sel_ctx),
	sel_on_create,
	sel_on_destroy,
	sel_on_reset,
	NULL,
	sel_get_type,
	sel_select,
	sel_num_objects,
	sel_add_listener,
	sel_add_connection,
	sel_del_listener,
	sel_del_connection,
	sel_ctrl
};

const NEURAL_SELECTOR_vtable *sel_fdpoll(void)
{
	return &sel_fdpoll_vtable;
}

NEURAL_SELECTOR *NEURAL_SELECTOR_new_fdpoll(void)
{
	return neural_selector_new(&sel_fdpoll_vtable);
}

static int sel_on_create(NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	if(!obj_table_init(ctx)) return 0;
	return 1;
}

static void sel_on_destroy(NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	obj_table_finish(ctx);
}

static void sel_on_reset(NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	obj_table_reset(ctx);
}

static NEURAL_SELECTOR_TYPE sel_get_type(const NEURAL_SELECTOR *sel)
{
	return NEURAL_SELECTOR_TYPE_FDPOLL;
}

static int sel_select(NEURAL_SELECTOR *sel, unsigned long usec_timeout,
                      int use_timeout)
{
	int res;
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	obj_table_pre_select(ctx);
	res = poll(ctx->pollfds, ctx->pfds_used, use_timeout ?
                   (int)(usec_timeout / 1000) : -1);
	if(res > 0) obj_table_post_select(ctx);
	return res;
}

static unsigned int sel_num_objects(const NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	return ctx->obj_used;
}

static NEURAL_SELECTOR_TOKEN sel_add_listener(NEURAL_SELECTOR *sel,
                                              NEURAL_LISTENER *listener)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
        return obj_table_add_listener(ctx, listener);
}

static NEURAL_SELECTOR_TOKEN sel_add_connection(NEURAL_SELECTOR *sel,
				                NEURAL_CONNECTION *conn)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
        return obj_table_add_connection(ctx, conn);
}

static void sel_del_listener(NEURAL_SELECTOR *sel, NEURAL_LISTENER *listener,
                             NEURAL_SELECTOR_TOKEN token)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
        obj_table_del(ctx, token);
}

static void sel_del_connection(NEURAL_SELECTOR *sel, NEURAL_CONNECTION *conn,
                               NEURAL_SELECTOR_TOKEN token)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
        obj_table_del(ctx, token);
}

static void obj_table_pre_select(sel_ctx *ctx)
{
	unsigned int loop = 0;
	sel_obj *item = ctx->obj_table;
	ctx->pfds_used = 0;
	while(loop < ctx->obj_size) {
                item->idx_init = ctx->pfds_used;
		item->idx_total = 0;
		ctx->hook_current = TOKENIFY(loop);
		switch(item->what) {
		case 1:
			neural_connection_pre_select(item->obj.conn);
			break;
		case 2:
			neural_listener_pre_select(item->obj.listener);
			break;
		default:
			break;
		}
                if(item->idx_total) {
			assert(item->idx_init + item->idx_total == ctx->pfds_used);
		}
		item++;
		loop++;
	}
}

static void obj_table_post_select(sel_ctx *ctx)
{
	unsigned int loop = 0;
	sel_obj *item = ctx->obj_table;
	while(loop < ctx->obj_size) {
              if(item->idx_total) {
                    ctx->hook_current = TOKENIFY(loop);
	         switch(item->what) {
		        case 1:
			       neural_connection_post_select(item->obj.conn);
			       break;
		        case 2:
			       neural_listener_post_select(item->obj.listener);
			       break;
		        default:
			       break;
	          }
               }
	       item++;
	       loop++;
	}
}

static void sel_fd_set(NEURAL_SELECTOR *sel, NEURAL_SELECTOR_TOKEN token,
                       int fd, unsigned char flags)
{
	struct pollfd *pfd;
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	unsigned int idx = IDXIFY(token);
	sel_obj *obj = ctx->obj_table + idx;
	assert(token == ctx->hook_current);
	assert(idx < ctx->obj_size);
	assert((obj->what == 1) || (obj->what == 2));
	if(!pollfds_expand(ctx)) {
		SYS_fprintf(SYS_stderr, "Warning, expansion for fd_set failed\n");
		return;
	}
	assert(obj->idx_init + obj->idx_total == ctx->pfds_used);
	pfd = ctx->pollfds + (obj->idx_init + obj->idx_total++);
	pfd->fd = fd;
	pfd->events = ((flags & SELECTOR_FLAG_READ) ? POLLIN : 0) |
		((flags & SELECTOR_FLAG_SEND) ? POLLOUT : 0) |
		((flags & SELECTOR_FLAG_EXCEPT) ?
			POLLERR | POLLHUP | POLLNVAL : 0);
	ctx->pfds_used++;
}

static unsigned char sel_fd_test(const NEURAL_SELECTOR *sel,
                                 NEURAL_SELECTOR_TOKEN token, int fd)
{
	unsigned int loop = 0;
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	unsigned int idx = IDXIFY(token);
	sel_obj *obj = ctx->obj_table + idx;
	assert(token == ctx->hook_current);
	assert(idx < ctx->obj_size);
	assert((obj->what == 1) || (obj->what == 2));
	assert(!obj->idx_total ||
		((obj->idx_init + obj->idx_total) <= ctx->pfds_used));
	while(loop < obj->idx_total) {
		struct pollfd *pfd = ctx->pollfds +
			(obj->idx_init + loop++);
		if(pfd->fd == fd) {
			unsigned char flags = 0;
			if(pfd->revents & POLLIN)
				flags |= SELECTOR_FLAG_READ;
			if(pfd->revents & POLLOUT)
				flags |= SELECTOR_FLAG_SEND;
			if(!flags && (pfd->revents & (POLLERR|POLLHUP|POLLNVAL)))
				flags = SELECTOR_FLAG_EXCEPT;
			return flags;
		}
	}
	assert(NULL == "sel_fd_test had no collision!");
	return 0;
}

static int sel_ctrl(NEURAL_SELECTOR *sel, int cmd, void *p)
{
	switch(cmd) {
	case NEURAL_FD_CTRL_FDSET:
		{
		NEURAL_FD_FDSET *args = p;
                sel_fd_set(sel, args->token, args->fd, args->flags);
		}
		break;
	case NEURAL_FD_CTRL_FDTEST:
		{
		NEURAL_FD_FDTEST *args = p;
                args->flags = sel_fd_test(sel, args->token, args->fd);
		}
		break;
	default:
		abort();
		return 0;
	}
	return 1;
}

#endif
