/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include "ctrl_fd.h"
#include <libgen/post.h>

#include <errno.h>

static int addr_on_create(NEURAL_ADDRESS *addr);
static void addr_on_destroy(NEURAL_ADDRESS *addr);
static int addr_parse(NEURAL_ADDRESS *addr, const char *addr_string);
static int addr_can_connect(const NEURAL_ADDRESS *addr);
static int addr_can_listen(const NEURAL_ADDRESS *addr);
static const NEURAL_LISTENER_vtable *addr_create_listener(const NEURAL_ADDRESS *addr);
static const NEURAL_CONNECTION_vtable *addr_create_connection(const NEURAL_ADDRESS *addr);
static const char *addr_prefixes[] = {"FD:", NULL};
typedef struct st_addr_ctx {
	int fd_read;
	int fd_send;
} addr_ctx;
extern NEURAL_ADDRESS_vtable builtin_fd_addr_vtable;
NEURAL_ADDRESS_vtable builtin_fd_addr_vtable = {
	"proto_fd",
	sizeof(addr_ctx),
	addr_prefixes,
	addr_on_create,
	addr_on_destroy,
	addr_on_destroy,
	NULL,
	addr_parse,
	addr_can_connect,
	addr_can_listen,
	addr_create_listener,
	addr_create_connection,
	NULL
};

static int list_on_create(NEURAL_LISTENER *l);
static void list_on_destroy(NEURAL_LISTENER *l);
static int list_listen(NEURAL_LISTENER *l, const NEURAL_ADDRESS *addr);
static const NEURAL_CONNECTION_vtable *list_pre_accept(NEURAL_LISTENER *l);
static int list_finished(const NEURAL_LISTENER *l);
static int list_pre_selector_add(NEURAL_LISTENER *, NEURAL_SELECTOR *);
static void list_post_selector_del(NEURAL_LISTENER *, NEURAL_SELECTOR *);
static void list_pre_select(NEURAL_LISTENER *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
static void list_post_select(NEURAL_LISTENER *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
typedef struct st_list_ctx {
	int accepted;
	int fd_read;
	int fd_send;
} list_ctx;

static const NEURAL_LISTENER_vtable list_vtable = {
	sizeof(list_ctx),
	list_on_create,
	list_on_destroy,
	list_on_destroy,
	NULL,
	list_listen,
	list_pre_accept,
	list_finished,
	list_pre_selector_add,
	NULL,
	NULL,
	list_post_selector_del,
	list_pre_select,
	list_post_select,
	NULL,
	NULL
};

static int conn_on_create(NEURAL_CONNECTION *conn);
static void conn_on_destroy(NEURAL_CONNECTION *conn);
static void conn_on_reset(NEURAL_CONNECTION *conn);
static int conn_connect(NEURAL_CONNECTION *conn, const NEURAL_ADDRESS *addr);
static int conn_accept(NEURAL_CONNECTION *conn, const NEURAL_LISTENER *l);
static int conn_set_size(NEURAL_CONNECTION *conn, unsigned int size);
static NEURAL_BUFFER *conn_get_read(const NEURAL_CONNECTION *conn);
static NEURAL_BUFFER *conn_get_send(const NEURAL_CONNECTION *conn);
static int conn_is_established(const NEURAL_CONNECTION *conn);
static int conn_pre_selector_add(NEURAL_CONNECTION *, NEURAL_SELECTOR *);
static void conn_post_selector_del(NEURAL_CONNECTION *, NEURAL_SELECTOR *);
static void conn_pre_select(NEURAL_CONNECTION *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
static void conn_post_select(NEURAL_CONNECTION *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
static int conn_do_io(NEURAL_CONNECTION *);

typedef struct st_conn_ctx {
	int fd_read;
	int fd_send;
	unsigned char flags;
	NEURAL_BUFFER *b_read;
	NEURAL_BUFFER *b_send;
} conn_ctx;
static const NEURAL_CONNECTION_vtable conn_vtable = {
	sizeof(conn_ctx),
	conn_on_create,
	conn_on_destroy,
	conn_on_reset,
	NULL,
	conn_connect,
	conn_accept,
	conn_set_size,
	conn_get_read,
	conn_get_send,
	conn_is_established,
	conn_pre_selector_add,
	NULL,
	NULL,
	conn_post_selector_del,
	conn_pre_select,
	conn_post_select,
	conn_do_io
};

static int addr_on_create(NEURAL_ADDRESS *addr)
{
	addr_ctx *ctx = neural_address_get_vtdata(addr);
	ctx->fd_read = ctx->fd_send = -1;
	return 1;
}

static void addr_on_destroy(NEURAL_ADDRESS *addr)
{
	addr_ctx *ctx = neural_address_get_vtdata(addr);
	ctx->fd_read = ctx->fd_send = -1;
}

static int addr_parse(NEURAL_ADDRESS *addr, const char *addr_string)
{
	char *tmp_ptr;
	addr_ctx *ctx;
	long conv_val;

	tmp_ptr = strchr(addr_string, ':');
	if(!tmp_ptr) return 0;
	ctx = neural_address_get_vtdata(addr);
	addr_string = tmp_ptr + 1;
	conv_val = strtol(addr_string, &tmp_ptr, 10);
	if(!tmp_ptr || (tmp_ptr == addr_string)) return 0;
	if((conv_val < -1) && (errno == EINVAL)) return 0;
	if(conv_val > 65535) return 0;
	switch(*tmp_ptr) {
	case '\0':
		if(conv_val < 0) return 0;
		ctx->fd_read = ctx->fd_send = (int)conv_val;
		return 1;
	case ':':
		ctx->fd_read = (int)conv_val;
		break;
	default:
		return 0;
	}
	addr_string = tmp_ptr + 1;
	conv_val = strtol(addr_string, &tmp_ptr, 10);
	if(!tmp_ptr || (tmp_ptr == addr_string)) return 0;
	if((conv_val < -1) && (errno == EINVAL)) return 0;
	if(conv_val > 65535) return 0;
	if(*tmp_ptr != '\0') return 0;
	if((ctx->fd_read < 0) && (conv_val < 0)) return 0;
	ctx->fd_send = (int)conv_val;
	return 1;
}

static int addr_can_connect(const NEURAL_ADDRESS *addr)
{
	return 1;
}

static int addr_can_listen(const NEURAL_ADDRESS *addr)
{
	return 1;
}

static const NEURAL_LISTENER_vtable *addr_create_listener(const NEURAL_ADDRESS *addr)
{
	return &list_vtable;
}

static const NEURAL_CONNECTION_vtable *addr_create_connection(const NEURAL_ADDRESS *addr)
{
	return &conn_vtable;
}

static int list_on_create(NEURAL_LISTENER *l)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	ctx->fd_read = ctx->fd_send = -1;
	return 1;
}

static void list_on_destroy(NEURAL_LISTENER *l)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	ctx->fd_read = ctx->fd_send = -1;
}

static int list_listen(NEURAL_LISTENER *l, const NEURAL_ADDRESS *addr)
{
	addr_ctx *ctx_addr = neural_address_get_vtdata(addr);
	list_ctx *ctx_list = neural_listener_get_vtdata(l);
	ctx_list->fd_read = ctx_addr->fd_read;
	ctx_list->fd_send = ctx_addr->fd_send;
	ctx_list->accepted = 0;
	return 1;
}

static const NEURAL_CONNECTION_vtable *list_pre_accept(NEURAL_LISTENER *l)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	if(ctx->accepted == 1) {
		ctx->accepted = 2;
		return &conn_vtable;
	}
	return NULL;
}

static int list_finished(const NEURAL_LISTENER *l)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	return ctx->accepted;
}

static int list_pre_selector_add(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel)
{
	switch(neural_selector_get_type(sel)) {
	case NEURAL_SELECTOR_TYPE_FDSELECT:
	case NEURAL_SELECTOR_TYPE_FDPOLL:
		return 1;
	case NEURAL_SELECTOR_TYPE_DYNAMIC:
		return neural_selector_dynamic_set(sel, NEURAL_SELECTOR_VT_DEFAULT());
	default:
		break;
	}
	return 0;
}

static void list_post_selector_del(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel)
{
	/* */
}

static void list_pre_select(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN tok)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	if(!ctx->accepted)
		neural_selector_fd_set(sel, tok, ctx->fd_read, SELECTOR_FLAG_READ);
}

static void list_post_select(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN tok)
{
	unsigned char flags;
	list_ctx *ctx = neural_listener_get_vtdata(l);
	if(ctx->accepted) return;
	neural_selector_fd_test(&flags, sel, tok, ctx->fd_read);
	if(flags & SELECTOR_FLAG_READ) ctx->accepted = 1;
}

static int conn_ctx_setup(conn_ctx *ctx_conn, int fd_read, int fd_send,
				unsigned int buf_size)
{
	if((fd_read != -1) && !NEURAL_BUFFER_set_size(ctx_conn->b_read, buf_size))
		return 0;
	if((fd_send != -1) && !NEURAL_BUFFER_set_size(ctx_conn->b_send, buf_size))
		return 0;
	ctx_conn->fd_read = fd_read;
	ctx_conn->fd_send = fd_send;
	return 1;
}

static int conn_on_create(NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	if(!ctx->b_read) ctx->b_read = NEURAL_BUFFER_new();
	if(!ctx->b_send) ctx->b_send = NEURAL_BUFFER_new();
	if(!ctx->b_read || !ctx->b_send) return 0;
	ctx->fd_read = -1;
	ctx->fd_send = -1;
	return 1;
}

static void conn_on_destroy(NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	neural_fd_close(&ctx->fd_read);
	neural_fd_close(&ctx->fd_send);
	NEURAL_BUFFER_free(ctx->b_read);
	NEURAL_BUFFER_free(ctx->b_send);
}

static void conn_on_reset(NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	neural_fd_close(&ctx->fd_read);
	neural_fd_close(&ctx->fd_send);
	NEURAL_BUFFER_reset(ctx->b_read);
	NEURAL_BUFFER_reset(ctx->b_send);
	ctx->flags = 0;
}

static int conn_connect(NEURAL_CONNECTION *conn, const NEURAL_ADDRESS *addr)
{
	const addr_ctx *ctx_addr = neural_address_get_vtdata(addr);
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	if((ctx_addr->fd_read != -1) && !neural_fd_make_non_blocking(ctx_addr->fd_read, 1))
		return 0;
	if((ctx_addr->fd_send != -1) && !neural_fd_make_non_blocking(ctx_addr->fd_send, 1))
		return 0;
	if(!conn_ctx_setup(ctx_conn, ctx_addr->fd_read, ctx_addr->fd_send,
				NEURAL_ADDRESS_get_def_buffer_size(addr)))
		return 0;
	return 1;
}

static int conn_accept(NEURAL_CONNECTION *conn, const NEURAL_LISTENER *l)
{
	list_ctx *ctx_list = neural_listener_get_vtdata(l);
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	if(ctx_list->accepted != 2)
		return 0;
	if((ctx_list->fd_read != -1) && !neural_fd_make_non_blocking(ctx_list->fd_read, 1))
		return 0;
	if((ctx_list->fd_send != -1) && !neural_fd_make_non_blocking(ctx_list->fd_send, 1))
		return 0;
	if(!conn_ctx_setup(ctx_conn, ctx_list->fd_read, ctx_list->fd_send,
				neural_listener_get_def_buffer_size(l)))
		return 0;
	ctx_list->accepted = 2;
	return 1;
}

static int conn_set_size(NEURAL_CONNECTION *conn, unsigned int size)
{
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	if((ctx_conn->fd_read != -1) && !NEURAL_BUFFER_set_size(ctx_conn->b_read, size))
		return 0;
	if((ctx_conn->fd_send != -1) && !NEURAL_BUFFER_set_size(ctx_conn->b_send, size))
		return 0;
	return 1;
}

static NEURAL_BUFFER *conn_get_read(const NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	return ctx_conn->b_read;
}

static NEURAL_BUFFER *conn_get_send(const NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	return ctx_conn->b_send;
}

static int conn_is_established(const NEURAL_CONNECTION *conn)
{
	return 1;
}

static int conn_pre_selector_add(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel)
{
	switch(neural_selector_get_type(sel)) {
	case NEURAL_SELECTOR_TYPE_FDSELECT:
	case NEURAL_SELECTOR_TYPE_FDPOLL:
		return 1;
	case NEURAL_SELECTOR_TYPE_DYNAMIC:
		return neural_selector_dynamic_set(sel, NEURAL_SELECTOR_VT_DEFAULT());
	default:
		break;
	}
	return 0;
}

static void conn_post_selector_del(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	ctx->flags = 0;
}

static void conn_pre_select(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN token)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	unsigned char rflag =
		(NEURAL_BUFFER_notfull(ctx->b_read) ?  SELECTOR_FLAG_READ : 0);
	unsigned char sflag =
		(NEURAL_BUFFER_notempty(ctx->b_send) ?  SELECTOR_FLAG_SEND : 0);
	if(ctx->fd_read == ctx->fd_send) {
		if(ctx->fd_read != -1)
			neural_selector_fd_set(sel, token, ctx->fd_read,
					rflag | sflag | SELECTOR_FLAG_EXCEPT);
	} else {
		if(ctx->fd_read != -1)
			neural_selector_fd_set(sel, token, ctx->fd_read,
					rflag | SELECTOR_FLAG_EXCEPT);
		if(ctx->fd_send != -1)
			neural_selector_fd_set(sel, token, ctx->fd_send,
					sflag | SELECTOR_FLAG_EXCEPT);
	}
}

static void conn_post_select(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN token)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	if(ctx->fd_read == ctx->fd_send) {
		if(ctx->fd_read != -1)
			neural_selector_fd_test(&ctx->flags, sel, token, ctx->fd_read);
	} else {
		ctx->flags = 0;
		if(ctx->fd_read != -1)
			neural_selector_fd_test(&ctx->flags, sel, token, ctx->fd_read);
		if(ctx->fd_send != -1)
			neural_selector_fd_test(&ctx->flags, sel, token, ctx->fd_send);
	}
}

static int conn_do_io(NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	if(ctx->flags & SELECTOR_FLAG_EXCEPT) return 0;
	if(ctx->flags & SELECTOR_FLAG_READ) {
		int io_ret = neural_fd_buffer_from_fd(ctx->b_read, ctx->fd_read, 0);
		if(io_ret <= 0)
			return 0;
	}
	if(ctx->flags & SELECTOR_FLAG_SEND) {
		int io_ret = neural_fd_buffer_to_fd(ctx->b_send, ctx->fd_send, 0);
		if(io_ret <= 0)
			return 0;
	}
	return 1;
}

