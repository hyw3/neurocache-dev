/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include "ctrl_fd.h"
#include <libgen/post.h>

#include <assert.h>

#ifndef HAVE_SELECT

const NEURAL_SELECTOR_vtable *sel_fdselect(void)
{
	return NULL;
}

NEURAL_SELECTOR *NEURAL_SELECTOR_new_fdselect(void)
{
	return NULL;
}

#else

typedef struct st_sel_item {
	fd_set reads;
	fd_set sends;
	fd_set excepts;
	int max;
} sel_item;

typedef struct st_sel_obj {
	union {
		NEURAL_CONNECTION *conn;
		NEURAL_LISTENER *listener;
	} obj;
	unsigned char what;
} sel_obj;

typedef struct st_sel_ctx {
	sel_item last_selected;
	sel_item to_select;
	sel_obj *obj_table;
	unsigned int obj_used, obj_size;
} sel_ctx;

#define OBJ_TABLE_START	32
/* Use uintptr_t to cast to long pointer instead of unsigned int */
#define TOKENIFY(idx) (NEURAL_SELECTOR_TOKEN)((uintptr_t)idx | 0x8000)
#define IDXIFY(tok) ((uintptr_t)tok & 0x7FFF)

static void obj_table_zilch(sel_obj *items, unsigned int num)
{
	while(num--)
		(items++)->what = 0;
}

static int obj_table_init(sel_ctx *ctx)
{
	ctx->obj_table = SYS_malloc(sel_obj, OBJ_TABLE_START);
	if(!ctx->obj_table) return 0;
	obj_table_zilch(ctx->obj_table, OBJ_TABLE_START);
	ctx->obj_used = 0;
	ctx->obj_size = OBJ_TABLE_START;
	return 1;
}

static void obj_table_finish(sel_ctx *ctx)
{
	SYS_free(sel_obj, ctx->obj_table);
}

static void obj_table_reset(sel_ctx *ctx)
{
	ctx->obj_used = 0;
}

static int obj_table_add(sel_ctx *ctx)
{
	int loop = 0;
	if(ctx->obj_used == ctx->obj_size) {
		unsigned int newsize = ctx->obj_size * 3 / 2;
		sel_obj *newitems = SYS_malloc(sel_obj, newsize);
		if(!newitems) return -1;
		obj_table_zilch(newitems, newsize);
		if(ctx->obj_used)
			SYS_memcpy_n(sel_obj, newitems, ctx->obj_table,
					ctx->obj_used);
		SYS_free(sel_obj, ctx->obj_table);
		ctx->obj_table = newitems;
		ctx->obj_size = newsize;
	}
	while(ctx->obj_table[loop].what != 0)
		loop++;
	ctx->obj_used++;
	return loop;
}

static NEURAL_SELECTOR_TOKEN obj_table_add_listener(sel_ctx *ctx,
					NEURAL_LISTENER *listener)
{
	int loc = obj_table_add(ctx);
	if(loc < 0) return NEURAL_SELECTOR_TOKEN_NULL;
	ctx->obj_table[loc].what = 2;
	ctx->obj_table[loc].obj.listener = listener;
        return TOKENIFY(loc);
}

static NEURAL_SELECTOR_TOKEN obj_table_add_connection(sel_ctx *ctx,
					NEURAL_CONNECTION *conn)
{
	int loc = obj_table_add(ctx);
	if(loc < 0) return NEURAL_SELECTOR_TOKEN_NULL;
	ctx->obj_table[loc].what = 1;
	ctx->obj_table[loc].obj.conn = conn;
        return TOKENIFY(loc);
}

static void obj_table_del(sel_ctx *ctx, NEURAL_SELECTOR_TOKEN tok)
{
	unsigned int idx = IDXIFY(tok);
	assert(idx < ctx->obj_size);
	assert(ctx->obj_table[idx].what != 0);
	assert(ctx->obj_used > 0);
	ctx->obj_table[idx].what = 0;
	ctx->obj_used--;
}

static void obj_table_pre_select(sel_ctx *ctx)
{
	unsigned int loop = 0;
	sel_obj *item = ctx->obj_table;
	while(loop < ctx->obj_size) {
		switch(item->what) {
		case 1:
			neural_connection_pre_select(item->obj.conn);
			break;
		case 2:
			neural_listener_pre_select(item->obj.listener);
			break;
		default:
			break;
		}
		item++;
		loop++;
	}
}

static void obj_table_post_select(sel_ctx *ctx)
{
	unsigned int loop = 0;
	sel_obj *item = ctx->obj_table;
	while(loop < ctx->obj_size) {
		switch(item->what) {
		case 1:
			neural_connection_post_select(item->obj.conn);
			break;
		case 2:
			neural_listener_post_select(item->obj.listener);
			break;
		default:
			break;
		}
		item++;
		loop++;
	}
}

static int sel_on_create(NEURAL_SELECTOR *);
static void sel_on_destroy(NEURAL_SELECTOR *);
static void sel_on_reset(NEURAL_SELECTOR *);
static NEURAL_SELECTOR_TYPE sel_get_type(const NEURAL_SELECTOR *);
static int sel_select(NEURAL_SELECTOR *, unsigned long usec_timeout, int use_timeout);
static unsigned int sel_num_objects(const NEURAL_SELECTOR *);
static NEURAL_SELECTOR_TOKEN sel_add_listener(NEURAL_SELECTOR *, NEURAL_LISTENER *);
static NEURAL_SELECTOR_TOKEN sel_add_connection(NEURAL_SELECTOR *, NEURAL_CONNECTION *);
static void sel_del_listener(NEURAL_SELECTOR *, NEURAL_LISTENER *, NEURAL_SELECTOR_TOKEN);
static void sel_del_connection(NEURAL_SELECTOR *, NEURAL_CONNECTION *, NEURAL_SELECTOR_TOKEN);
static int sel_ctrl(NEURAL_SELECTOR *, int, void *);
static const NEURAL_SELECTOR_vtable sel_fdselect_vtable = {
	sizeof(sel_ctx),
	sel_on_create,
	sel_on_destroy,
	sel_on_reset,
	NULL,
	sel_get_type,
	sel_select,
	sel_num_objects,
	sel_add_listener,
	sel_add_connection,
	sel_del_listener,
	sel_del_connection,
	sel_ctrl
};

const NEURAL_SELECTOR_vtable *sel_fdselect(void)
{
	return &sel_fdselect_vtable;
}

NEURAL_SELECTOR *NEURAL_SELECTOR_new_fdselect(void)
{
	return neural_selector_new(&sel_fdselect_vtable);
}

#ifndef WIN32
#define FD_SET2(a,b) FD_SET((a),(b))
#define FD_CLR2(a,b) FD_CLR((a),(b))
#else
#define FD_SET2(a,b) FD_SET((SOCKET)(a),(b))
#define FD_CLR2(a,b) FD_CLR((SOCKET)(a),(b))
#endif

static void neural_selector_item_init(sel_item *item)
{
	FD_ZERO(&item->reads);
	FD_ZERO(&item->sends);
	FD_ZERO(&item->excepts);
	item->max = 0;
}

static void neural_selector_item_flip(sel_item *to,
				sel_item *from)
{
	SYS_memcpy(fd_set, &to->reads, &from->reads);
	SYS_memcpy(fd_set, &to->sends, &from->sends);
	SYS_memcpy(fd_set, &to->excepts, &from->excepts);
	to->max = from->max;
	neural_selector_item_init(from);
}

static int neural_selector_item_select(sel_item *item,
		unsigned long usec_timeout, int use_timeout)
{
	struct timeval timeout;
	if(use_timeout) {
		timeout.tv_sec = usec_timeout / 1000000;
		timeout.tv_usec = usec_timeout % 1000000;
	}
	return select(item->max, &item->reads, &item->sends, &item->excepts,
			(use_timeout ? &timeout : NULL));
}

static int sel_on_create(NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	if(!obj_table_init(ctx)) return 0;
	neural_selector_item_init(&ctx->last_selected);
	neural_selector_item_init(&ctx->to_select);
	return 1;
}

static void sel_on_destroy(NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	obj_table_finish(ctx);
}

static void sel_on_reset(NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	obj_table_reset(ctx);
	neural_selector_item_init(&ctx->last_selected);
	neural_selector_item_init(&ctx->to_select);
}

static NEURAL_SELECTOR_TYPE sel_get_type(const NEURAL_SELECTOR *sel)
{
	return NEURAL_SELECTOR_TYPE_FDSELECT;
}

static int sel_select(NEURAL_SELECTOR *sel, unsigned long usec_timeout,
			int use_timeout)
{
	int res;
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	obj_table_pre_select(ctx);
	neural_selector_item_flip(&ctx->last_selected, &ctx->to_select);
	res = neural_selector_item_select(&ctx->last_selected, usec_timeout,
				use_timeout);
	if(res > 0) obj_table_post_select(ctx);
	return res;
}

static unsigned int sel_num_objects(const NEURAL_SELECTOR *sel)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	return ctx->obj_used;
}

static NEURAL_SELECTOR_TOKEN sel_add_listener(NEURAL_SELECTOR *sel,
				NEURAL_LISTENER *listener)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	return obj_table_add_listener(ctx, listener);
}

static NEURAL_SELECTOR_TOKEN sel_add_connection(NEURAL_SELECTOR *sel,
				NEURAL_CONNECTION *conn)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	return obj_table_add_connection(ctx, conn);
}

static void sel_del_listener(NEURAL_SELECTOR *sel, NEURAL_LISTENER *listener,
				NEURAL_SELECTOR_TOKEN token)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
        obj_table_del(ctx, token);
}

static void sel_del_connection(NEURAL_SELECTOR *sel, NEURAL_CONNECTION *conn,
				NEURAL_SELECTOR_TOKEN token)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
        obj_table_del(ctx, token);
}

static void sel_fd_set(NEURAL_SELECTOR *sel, NEURAL_SELECTOR_TOKEN token,
				int fd, unsigned char flags)
{
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	if(flags & SELECTOR_FLAG_READ)
		FD_SET2(fd, &ctx->to_select.reads);
	if(flags & SELECTOR_FLAG_SEND)
		FD_SET2(fd, &ctx->to_select.sends);
	if(flags & SELECTOR_FLAG_EXCEPT)
		FD_SET2(fd, &ctx->to_select.excepts);
	ctx->to_select.max = ((ctx->to_select.max <= (fd + 1)) ?
				(fd + 1) : ctx->to_select.max);
}

static unsigned char sel_fd_test(const NEURAL_SELECTOR *sel,
                                 NEURAL_SELECTOR_TOKEN token, int fd)
{
	unsigned char flags = 0;
	sel_ctx *ctx = neural_selector_get_vtdata(sel);
	if(FD_ISSET(fd, &ctx->last_selected.reads))
		flags |= SELECTOR_FLAG_READ;
	if(FD_ISSET(fd, &ctx->last_selected.sends))
		flags |= SELECTOR_FLAG_SEND;
	if(FD_ISSET(fd, &ctx->last_selected.excepts))
		flags |= SELECTOR_FLAG_EXCEPT;
	return flags;
}

static int sel_ctrl(NEURAL_SELECTOR *sel, int cmd, void *p)
{
	switch(cmd) {
	case NEURAL_FD_CTRL_FDSET:
		{
		NEURAL_FD_FDSET *args = p;
		sel_fd_set(sel, args->token, args->fd, args->flags);
		}
		break;
	case NEURAL_FD_CTRL_FDTEST:
		{
		NEURAL_FD_FDTEST *args = p;
		args->flags = sel_fd_test(sel, args->token, args->fd);
		}
		break;
	default:
		abort();
		return 0;
	}
	return 1;
}

#endif
