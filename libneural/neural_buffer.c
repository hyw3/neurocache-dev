/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include <libgen/post.h>

#include <assert.h>

struct st_NEURAL_BUFFER {
	unsigned char *data;
	unsigned int used, size;
};

NEURAL_BUFFER *NEURAL_BUFFER_new(void)
{
	NEURAL_BUFFER *b = SYS_malloc(NEURAL_BUFFER, 1);
	if(b) {
		b->data = NULL;
		b->used = b->size = 0;
	}
	return b;
}

void NEURAL_BUFFER_free(NEURAL_BUFFER *b)
{
	if(b->data) SYS_free(unsigned char, b->data);
	SYS_free(NEURAL_BUFFER, b);
}

void NEURAL_BUFFER_reset(NEURAL_BUFFER *b)
{
	b->used = 0;
}

int NEURAL_BUFFER_set_size(NEURAL_BUFFER *buf, unsigned int size)
{
	unsigned char *next;
	if(size == buf->size)
		return 1;
	if(!neural_check_buffer_size(size)) {
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Error, NEURAL_BUFFER_set_size() called with too "
				"large a size\n");
#endif
		return 0;
	}
	next = SYS_realloc(unsigned char, buf->data, size);
	if(size && !next)
		return 0;
	buf->data = next;
	buf->size = size;
	buf->used = 0;
	return 1;
}

int NEURAL_BUFFER_empty(const NEURAL_BUFFER *buf)
{
	return (buf->used == 0);
}

int NEURAL_BUFFER_full(const NEURAL_BUFFER *buf)
{
	return (buf->used == buf->size);
}

int NEURAL_BUFFER_notempty(const NEURAL_BUFFER *buf)
{
	return (buf->used > 0);
}

int NEURAL_BUFFER_notfull(const NEURAL_BUFFER *buf)
{
	return (buf->used < buf->size);
}

unsigned int NEURAL_BUFFER_used(const NEURAL_BUFFER *buf)
{
	return buf->used;
}

unsigned int NEURAL_BUFFER_unused(const NEURAL_BUFFER *buf)
{
	return (buf->size - buf->used);
}

const unsigned char *NEURAL_BUFFER_data(const NEURAL_BUFFER *buf)
{
	return buf->data;
}

unsigned int NEURAL_BUFFER_size(const NEURAL_BUFFER *buf)
{
	return buf->size;
}

unsigned int NEURAL_BUFFER_write(NEURAL_BUFFER *buf, const unsigned char *ptr,
		                unsigned int size)
{
	unsigned int towrite = NEURAL_BUFFER_unused(buf);
	if(towrite > size)
		towrite = size;
	if(towrite == 0)
		return 0;
	SYS_memcpy_n(unsigned char, buf->data + buf->used, ptr, towrite);
	buf->used += towrite;
	return towrite;
}

unsigned int NEURAL_BUFFER_read(NEURAL_BUFFER *buf, unsigned char *ptr,
		                unsigned int size)
{
	unsigned int toread = NEURAL_BUFFER_used(buf);
	if(toread > size)
		toread = size;
	if(toread == 0)
		return 0;
	if(ptr)
		SYS_memcpy_n(unsigned char, ptr, buf->data, toread);
	buf->used -= toread;
	if(buf->used > 0)
		SYS_memmove_n(unsigned char, buf->data,
				buf->data + toread, buf->used);
	return toread;
}

unsigned int NEURAL_BUFFER_transfer(NEURAL_BUFFER *dest, NEURAL_BUFFER *src,
				unsigned int max)
{
	unsigned int tmp = NEURAL_BUFFER_unused(dest);
	if(!max || (max > tmp)) max = tmp;
	if(!max) return 0;
	tmp = NEURAL_BUFFER_read(src, NEURAL_BUFFER_write_ptr(dest), max);
	NEURAL_BUFFER_wrote(dest, tmp);
	return tmp;
}

unsigned char *NEURAL_BUFFER_write_ptr(NEURAL_BUFFER *buf)
{
	return (buf->data + buf->used);
}

void NEURAL_BUFFER_wrote(NEURAL_BUFFER *buf, unsigned int size)
{
	assert(size <= NEURAL_BUFFER_unused(buf));
	buf->used += size;
}
