/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include <libgen/post.h>

#include <errno.h>

int neural_fd_make_non_blocking(int fd, int non_blocking)
{
#ifdef WIN32
	u_long dummy = 1;
	if(ioctlsocket(fd, FIONBIO, &dummy) != 0)
		return 0;
	return 1;
#else
	int flags;

	if(((flags = fcntl(fd, F_GETFL, 0)) < 0) ||
			(fcntl(fd, F_SETFL, (non_blocking ?
			(flags | O_NONBLOCK) : (flags & ~O_NONBLOCK))) < 0)) {
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Error, couldn't make socket non-blocking.\n");
#endif
		return 0;
	}
	return 1;
#endif
}

int neural_fd_buffer_to_fd(NEURAL_BUFFER *buf, int fd, unsigned int max_send)
{
	ssize_t ret;
	unsigned int buf_used = NEURAL_BUFFER_used(buf);

	if((max_send == 0) || (max_send > buf_used))
		max_send = buf_used;
	if(!max_send)
		return 0;
#ifdef WIN32
	ret = send(fd, NEURAL_BUFFER_data(buf), max_send, 0);
#else
	ret = write(fd, NEURAL_BUFFER_data(buf), max_send);
#endif
#if 0
	ret = send(fd, NEURAL_BUFFER_data(buf), max_send,
		MSG_DONTWAIT | MSG_NOSIGNAL);
#endif
	if(ret < 0) {
		switch(errno) {
		case EAGAIN:
		case EINTR:
			return 0;
		default:
			break;
		}
		return -1;
	}
	if(ret > 0) {
		unsigned int uret = (unsigned int)ret;
		NEURAL_BUFFER_read(buf, NULL, uret);
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Debug: net.c (fd=%d) sent %lu bytes\n",
			fd, (unsigned long)uret);
#endif
	}
	return ret;
}

int neural_fd_buffer_from_fd(NEURAL_BUFFER *buf, int fd, unsigned int max_read)
{
	ssize_t ret;
	unsigned int buf_avail = NEURAL_BUFFER_unused(buf);
	if((max_read == 0) || (max_read > buf_avail))
		max_read = buf_avail;
	if(!max_read)
		return 0;
#ifdef WIN32
	ret = recv(fd, NEURAL_BUFFER_write_ptr(buf), max_read, 0);
#else
	ret = read(fd, NEURAL_BUFFER_write_ptr(buf), max_read);
#endif
#if 0
	ret = recv(fd, NEURAL_BUFFER_write_ptr(buf), max_read, MSG_NOSIGNAL);
#endif
	if(ret < 0) {
		switch(errno) {
		case EINTR:
		case EAGAIN:
			return 0;
		default:
			break;
		}
		return -1;
	}
	if(ret > 0) {
		unsigned int uret = (unsigned int)ret;
		NEURAL_BUFFER_wrote(buf, uret);
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Debug: net.c (fd=%d) received %lu bytes\n",
			fd, (unsigned long)uret);
#endif
	}
	return ret;
}

void neural_fd_close(int *fd)
{
	if(*fd > -1)
#ifdef WIN32
		closesocket(*fd);
#else
		close(*fd);
#endif
	*fd = -1;
}
