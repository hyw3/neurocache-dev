/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include <libgen/post.h>

#include <assert.h>

struct st_NEURAL_LISTENER {
	const NEURAL_LISTENER_vtable *vt;
	void *vt_data;
	size_t vt_data_size;
	const NEURAL_LISTENER_vtable *reset;
	unsigned int def_buffer_size;
	NEURAL_SELECTOR *sel;
	NEURAL_SELECTOR_TOKEN sel_token;
};

unsigned int neural_listener_get_def_buffer_size(const NEURAL_LISTENER *l)
{
	return l->def_buffer_size;
}

int neural_listener_set_def_buffer_size(NEURAL_LISTENER *l, unsigned int def_buffer_size)
{
	if(!neural_check_buffer_size(def_buffer_size)) return 0;
	l->def_buffer_size = def_buffer_size;
	return 1;
}

int neural_listener_set_vtable(NEURAL_LISTENER *a, const NEURAL_LISTENER_vtable *vtable)
{
	if(a->vt) {
		if(a->vt->pre_close)
			a->vt->pre_close(a);
		if(a->sel) NEURAL_LISTENER_del_from_selector(a);
		a->vt->on_reset(a);
		a->reset = a->vt;
		a->vt = NULL;
	}
	if(a->reset && (a->reset != vtable)) {
		a->reset->on_destroy(a);
		a->reset = NULL;
		SYS_zero_n(unsigned char, a->vt_data, a->vt_data_size);
	}
	if(vtable->vtdata_size > a->vt_data_size) {
		assert(a->reset == NULL);
		if(a->vt_data)
			SYS_free(void, a->vt_data);
		a->vt_data = SYS_malloc(unsigned char, vtable->vtdata_size);
		if(!a->vt_data) {
			a->vt_data_size = 0;
			return 0;
		}
		a->vt_data_size = vtable->vtdata_size;
		SYS_zero_n(unsigned char, a->vt_data, vtable->vtdata_size);
	}
	if(vtable->on_create(a)) {
		a->vt = vtable;
		return 1;
	}
	return 0;
}

const NEURAL_LISTENER_vtable *neural_listener_get_vtable(const NEURAL_LISTENER *l)
{
	return l->vt;
}

void *neural_listener_get_vtdata(const NEURAL_LISTENER *l)
{
	return l->vt_data;
}

const NEURAL_CONNECTION_vtable *neural_listener_pre_accept(NEURAL_LISTENER *l)
{
	if(l->vt) return l->vt->pre_accept(l);
	return NULL;
}

void neural_listener_pre_select(NEURAL_LISTENER *l)
{
	assert((l->sel != NULL) && (l->vt != NULL));
	l->vt->pre_select(l, l->sel, l->sel_token);
}

void neural_listener_post_select(NEURAL_LISTENER *l)
{
	assert((l->sel != NULL) && (l->vt != NULL));
	l->vt->post_select(l, l->sel, l->sel_token);
}

NEURAL_SELECTOR *neural_listener_get_selector(const NEURAL_LISTENER *l,
					NEURAL_SELECTOR_TOKEN *t)
{
	if(l->sel && t) *t = l->sel_token;
	return l->sel;
}

void neural_listener_set_selector_raw(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel,
					NEURAL_SELECTOR_TOKEN token)
{
	l->sel = sel;
	l->sel_token = token;
}

NEURAL_LISTENER *NEURAL_LISTENER_new(void)
{
	NEURAL_LISTENER *l = SYS_malloc(NEURAL_LISTENER, 1);
	if(l) {
		l->vt = NULL;
		l->vt_data = NULL;
		l->vt_data_size = 0;
		l->reset = NULL;
		l->def_buffer_size = 0;
		l->sel = NULL;
		l->sel_token = NULL;
	}
	return l;
}

void NEURAL_LISTENER_free(NEURAL_LISTENER *list)
{
	if(list->vt && list->vt->pre_close) list->vt->pre_close(list);
	if(list->sel) NEURAL_LISTENER_del_from_selector(list);
	if(list->vt) list->vt->on_destroy(list);
	else if(list->reset) list->reset->on_destroy(list);
	if(list->vt_data) SYS_free(void, list->vt_data);
	SYS_free(NEURAL_LISTENER, list);
}

void NEURAL_LISTENER_reset(NEURAL_LISTENER *list)
{
	if(list->vt && list->vt->pre_close) list->vt->pre_close(list);
	if(list->sel) NEURAL_LISTENER_del_from_selector(list);
	if(list->vt) {
		list->vt->on_reset(list);
		list->reset = list->vt;
		list->vt = NULL;
	}
}

int NEURAL_LISTENER_create(NEURAL_LISTENER *list, const NEURAL_ADDRESS *addr)
{
	const NEURAL_LISTENER_vtable *vtable;
	if(list->vt) return 0;
	vtable = neural_address_get_listener(addr);
	if(!neural_listener_set_vtable(list, vtable) ||
			!neural_listener_set_def_buffer_size(list,
				NEURAL_ADDRESS_get_def_buffer_size(addr)) ||
			!vtable->listen(list, addr)) {
		NEURAL_LISTENER_reset(list);
		return 0;
	}
	return 1;
}

int NEURAL_LISTENER_add_to_selector(NEURAL_LISTENER *list,
				NEURAL_SELECTOR *sel)
{
	if(!list->vt || list->sel || !list->vt->pre_selector_add(list, sel))
		return 0;
	if((list->sel_token = neural_selector_add_listener(sel, list)) ==
				NEURAL_SELECTOR_TOKEN_NULL) {
		list->vt->post_selector_del(list, sel);
		return 0;
	}
	list->sel = sel;
	if(list->vt->post_selector_add && !list->vt->post_selector_add(list,
				sel, list->sel_token)) {
		NEURAL_LISTENER_del_from_selector(list);
		return 0;
	}
	return 1;
}

void NEURAL_LISTENER_del_from_selector(NEURAL_LISTENER *list)
{
	if(list->vt && list->sel) {
		NEURAL_SELECTOR *sel = list->sel;
		if(list->vt->pre_selector_del)
			list->vt->pre_selector_del(list, sel, list->sel_token);
		neural_selector_del_listener(list->sel, list, list->sel_token);
		list->sel = NULL;
		list->sel_token = NULL;
		list->vt->post_selector_del(list, sel);
	}
}

int NEURAL_LISTENER_finished(const NEURAL_LISTENER *list)
{
	if(list->vt) return list->vt->finished(list);
	return 0;
}

int NEURAL_LISTENER_set_fs_owner(NEURAL_LISTENER *list,
				const char *ownername,
				const char *groupname)
{
	if(list->vt && list->vt->set_fs_owner)
		return list->vt->set_fs_owner(list, ownername, groupname);
	return 0;
}

int NEURAL_LISTENER_set_fs_perms(NEURAL_LISTENER *list,
				const char *octal_string)
{
	if(list->vt && list->vt->set_fs_perms)
		return list->vt->set_fs_perms(list, octal_string);
	return 0;
}
