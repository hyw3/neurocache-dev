/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include <libgen/post.h>

#include <assert.h>

struct st_NEURAL_SELECTOR {
	const NEURAL_SELECTOR_vtable *vt;
	void *vt_data;
	size_t vt_data_size;
	const NEURAL_SELECTOR_vtable *reset;
};

static int dyn_on_create(NEURAL_SELECTOR *s) { return 1; }
static void dyn_on_destroy(NEURAL_SELECTOR *s) { }
static void dyn_on_reset(NEURAL_SELECTOR *s) { }
static NEURAL_SELECTOR_TYPE dyn_get_type(const NEURAL_SELECTOR *s) {
	return NEURAL_SELECTOR_TYPE_DYNAMIC; }
static int dyn_select(NEURAL_SELECTOR *s, unsigned long x, int y) { return -1; }
static unsigned int dyn_num_objects(const NEURAL_SELECTOR *s) { return 0; }
static const NEURAL_SELECTOR_vtable vtable_dyn = {
	0,
	dyn_on_create,
	dyn_on_destroy,
	dyn_on_reset,
	NULL,
	dyn_get_type,
	dyn_select,
	dyn_num_objects,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

NEURAL_SELECTOR_TOKEN neural_selector_add_listener(NEURAL_SELECTOR *s, NEURAL_LISTENER *l)
{
	if(s->vt) return s->vt->add_listener(s, l);
	return NEURAL_SELECTOR_TOKEN_NULL;
}

NEURAL_SELECTOR_TOKEN neural_selector_add_connection(NEURAL_SELECTOR *s, NEURAL_CONNECTION *c)
{
	if(s->vt) return s->vt->add_connection(s, c);
	return NEURAL_SELECTOR_TOKEN_NULL;
}

void neural_selector_del_listener(NEURAL_SELECTOR *s, NEURAL_LISTENER *l, NEURAL_SELECTOR_TOKEN k)
{
	if(s->vt) s->vt->del_listener(s, l, k);
}

void neural_selector_del_connection(NEURAL_SELECTOR *s, NEURAL_CONNECTION *c, NEURAL_SELECTOR_TOKEN k)
{
	if(s->vt) s->vt->del_connection(s, c, k);
}

NEURAL_SELECTOR *neural_selector_new(const NEURAL_SELECTOR_vtable *vtable)
{
	NEURAL_SELECTOR *sel = SYS_malloc(NEURAL_SELECTOR, 1);
	if(!sel) goto err;
	if(vtable->vtdata_size) {
		sel->vt_data = SYS_malloc(unsigned char, vtable->vtdata_size);
		if(!sel->vt_data) goto err;
	} else
		sel->vt_data = NULL;
	sel->vt = vtable;
	sel->vt_data_size = vtable->vtdata_size;
	sel->reset = NULL;
	SYS_zero_n(unsigned char, sel->vt_data, vtable->vtdata_size);
	if(!vtable->on_create(sel)) goto err;
	return sel;
err:
	if(sel) {
		if(sel->vt_data) SYS_free(void, sel->vt_data);
		SYS_free(NEURAL_SELECTOR, sel);
	}
	return NULL;
}

const NEURAL_SELECTOR_vtable *neural_selector_get_vtable(const NEURAL_SELECTOR *sel)
{
	return sel->vt;
}

void *neural_selector_get_vtdata(const NEURAL_SELECTOR *sel)
{
	return sel->vt_data;
}

NEURAL_SELECTOR_TYPE neural_selector_get_type(const NEURAL_SELECTOR *sel)
{
	if(!sel->vt) return NEURAL_SELECTOR_TYPE_ERROR;
	return sel->vt->get_type(sel);
}

int neural_selector_ctrl(NEURAL_SELECTOR *sel, int cmd, void *p)
{
	if(sel->vt && sel->vt->ctrl)
		return sel->vt->ctrl(sel, cmd, p);
	return 0;
}

int neural_selector_dynamic_set(NEURAL_SELECTOR *s, const NEURAL_SELECTOR_vtable *vt) {
	assert(s->vt == &vtable_dyn);
	assert(s->vt_data == NULL);
	assert(s->vt_data_size == 0);
	assert(s->reset == NULL);
	if(s->vt != &vtable_dyn) return 0;
	if(vt->vtdata_size) {
		s->vt_data = SYS_malloc(unsigned char, vt->vtdata_size);
		if(!s->vt_data) return 0;
	}
	SYS_zero_n(unsigned char, s->vt_data, vt->vtdata_size);
	s->vt = vt;
	s->vt_data_size = vt->vtdata_size;
	if(!vt->on_create(s)) {
		SYS_free(void, s->vt_data);
		s->vt = &vtable_dyn;
		s->vt_data_size = 0;
		return 0;
	}
	return 1;
}

NEURAL_SELECTOR *NEURAL_SELECTOR_new(void)
{
	return neural_selector_new(&vtable_dyn);
}

void NEURAL_SELECTOR_free(NEURAL_SELECTOR *sel)
{
	assert(sel->vt);
	if(sel->vt->pre_close) sel->vt->pre_close(sel);
	sel->vt->on_destroy(sel);
	if(sel->vt_data) SYS_free(void, sel->vt_data);
	SYS_free(NEURAL_SELECTOR, sel);
}

void NEURAL_SELECTOR_reset(NEURAL_SELECTOR *sel)
{
	assert(sel->vt);
	if(sel->vt->pre_close) sel->vt->pre_close(sel);
	sel->vt->on_reset(sel);
}

int NEURAL_SELECTOR_select(NEURAL_SELECTOR *sel, unsigned long usec_timeout,
			int use_timeout)
{
	assert(sel->vt);
	return sel->vt->select(sel, usec_timeout, use_timeout);
}

unsigned int NEURAL_SELECTOR_num_objects(const NEURAL_SELECTOR *sel)
{
	assert(sel->vt);
	return sel->vt->num_objects(sel);
}
