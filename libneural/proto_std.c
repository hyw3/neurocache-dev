/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include "ctrl_fd.h"
#include <libgen/post.h>

#include <assert.h>

static int addr_on_create(NEURAL_ADDRESS *addr);
static void addr_on_destroy(NEURAL_ADDRESS *addr);
static int addr_parse(NEURAL_ADDRESS *addr, const char *addr_string);
static int addr_can_connect(const NEURAL_ADDRESS *addr);
static int addr_can_listen(const NEURAL_ADDRESS *addr);
static const NEURAL_LISTENER_vtable *addr_create_listener(const NEURAL_ADDRESS *addr);
static const NEURAL_CONNECTION_vtable *addr_create_connection(const NEURAL_ADDRESS *addr);
static const char *addr_prefixes[] = {"IP:", "IPv4:", "UNIX:", NULL};
extern NEURAL_ADDRESS_vtable builtin_sock_addr_vtable;
extern NEURAL_ADDRESS_vtable builtin_fd_addr_vtable;
NEURAL_ADDRESS_vtable builtin_sock_addr_vtable = {
	"proto_std",
	sizeof(neural_sockaddr),
	addr_prefixes,
	addr_on_create,
	addr_on_destroy,
	addr_on_destroy,
	NULL,
	addr_parse,
	addr_can_connect,
	addr_can_listen,
	addr_create_listener,
	addr_create_connection,
	&builtin_fd_addr_vtable
};

static int list_on_create(NEURAL_LISTENER *);
static void list_on_destroy(NEURAL_LISTENER *);
static int list_listen(NEURAL_LISTENER *, const NEURAL_ADDRESS *);
static const NEURAL_CONNECTION_vtable *list_pre_accept(NEURAL_LISTENER *);
static int list_finished(const NEURAL_LISTENER *);
static int list_pre_selector_add(NEURAL_LISTENER *, NEURAL_SELECTOR *);
static void list_post_selector_del(NEURAL_LISTENER *, NEURAL_SELECTOR *);
static void list_pre_select(NEURAL_LISTENER *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
static void list_post_select(NEURAL_LISTENER *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
static int list_set_fs_owner(NEURAL_LISTENER *l, const char *ownername,
				const char *groupname);
static int list_set_fs_perms(NEURAL_LISTENER *l, const char *octal_string);

typedef struct st_list_ctx {
	int fd, caught;
	neural_sockaddr_type type;
} list_ctx;

static const NEURAL_LISTENER_vtable list_vtable = {
	sizeof(list_ctx),
	list_on_create,
	list_on_destroy,
	list_on_destroy,
	NULL,
	list_listen,
	list_pre_accept,
	list_finished,
	list_pre_selector_add,
	NULL,
	NULL,
	list_post_selector_del,
	list_pre_select,
	list_post_select,
	list_set_fs_owner,
	list_set_fs_perms
};

static int conn_on_create(NEURAL_CONNECTION *);
static void conn_on_destroy(NEURAL_CONNECTION *);
static void conn_on_reset(NEURAL_CONNECTION *);
static int conn_connect(NEURAL_CONNECTION *, const NEURAL_ADDRESS *);
static int conn_accept(NEURAL_CONNECTION *, const NEURAL_LISTENER *);
static int conn_set_size(NEURAL_CONNECTION *, unsigned int);
static NEURAL_BUFFER *conn_get_read(const NEURAL_CONNECTION *);
static NEURAL_BUFFER *conn_get_send(const NEURAL_CONNECTION *);
static int conn_is_established(const NEURAL_CONNECTION *);
static int conn_pre_selector_add(NEURAL_CONNECTION *, NEURAL_SELECTOR *);
static void conn_post_selector_del(NEURAL_CONNECTION *, NEURAL_SELECTOR *);
static void conn_pre_select(NEURAL_CONNECTION *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
static void conn_post_select(NEURAL_CONNECTION *, NEURAL_SELECTOR *, NEURAL_SELECTOR_TOKEN);
static int conn_do_io(NEURAL_CONNECTION *);

typedef struct st_conn_ctx {
	int fd, established;
	unsigned char flags;
	NEURAL_BUFFER *b_read;
	NEURAL_BUFFER *b_send;
} conn_ctx;

static const NEURAL_CONNECTION_vtable conn_vtable = {
	sizeof(conn_ctx),
	conn_on_create,
	conn_on_destroy,
	conn_on_reset,
	NULL,
	conn_connect,
	conn_accept,
	conn_set_size,
	conn_get_read,
	conn_get_send,
	conn_is_established,
	conn_pre_selector_add,
	NULL,
	NULL,
	conn_post_selector_del,
	conn_pre_select,
	conn_post_select,
	conn_do_io
};

static int gb_use_nagle = 0;

void NEURAL_config_set_nagle(int enabled)
{
	gb_use_nagle = enabled;
}

int NEURAL_CONNECTION_create_pair(NEURAL_CONNECTION *conn1,
				NEURAL_CONNECTION *conn2,
				unsigned int def_buffer_size)
{
	int fds[2];
	conn_ctx *ctx1, *ctx2;
	if(neural_connection_get_vtable(conn1) || neural_connection_get_vtable(conn2))
		return 0;
	if(!neural_connection_set_vtable(conn1, &conn_vtable) ||
			!neural_connection_set_vtable(conn2, &conn_vtable))
		return 0;
	if(!NEURAL_CONNECTION_set_size(conn1, def_buffer_size) ||
			!NEURAL_CONNECTION_set_size(conn2, def_buffer_size))
		return 0;
	if(!neural_sock_create_unix_pair(fds))
		return 0;
	if(!neural_fd_make_non_blocking(fds[0], 1) ||
			!neural_fd_make_non_blocking(fds[1], 1) ||
			!neural_sock_set_nagle(fds[0], gb_use_nagle, neural_sockaddr_type_unix) ||
			!neural_sock_set_nagle(fds[1], gb_use_nagle, neural_sockaddr_type_unix)) {
		neural_fd_close(fds);
		neural_fd_close(fds + 1);
		return 0;
	}
	ctx1 = neural_connection_get_vtdata(conn1);
	ctx2 = neural_connection_get_vtdata(conn2);
	ctx1->fd = fds[0];
	ctx2->fd = fds[1];
	ctx1->established = 1;
	ctx2->established = 1;
	ctx1->flags = ctx2->flags = 0;
	return 1;
}

static int addr_on_create(NEURAL_ADDRESS *addr)
{
	return 1;
}

static void addr_on_destroy(NEURAL_ADDRESS *addr)
{
}

static int addr_parse(NEURAL_ADDRESS *addr, const char *addr_string)
{
	char *tmp_ptr;
	neural_sockaddr *ctx;
	int len;

	tmp_ptr = strchr(addr_string, ':');
	if(!tmp_ptr) return 0;
	len = (tmp_ptr - addr_string);
	if(len < 1) return 0;
	tmp_ptr++;
	ctx = neural_address_get_vtdata(addr);
	if(((len == 4) && (strncmp(addr_string, "IPv4", 4) == 0)) ||
			((len == 2) && (strncmp(addr_string, "IP", 2) == 0))) {
		if(!neural_sock_sockaddr_from_ipv4(ctx, tmp_ptr))
			return 0;
	} else if((len == 4) && (strncmp(addr_string, "UNIX", 4) == 0)) {
		if(!neural_sock_sockaddr_from_unix(ctx, tmp_ptr))
			return 0;
	} else
		return 0;
	return 1;
}

static int addr_can_connect(const NEURAL_ADDRESS *addr)
{
	neural_sockaddr *ctx = neural_address_get_vtdata(addr);
	return ((ctx->caps & NEURAL_ADDRESS_CAN_CONNECT) ? 1 : 0);
}

static int addr_can_listen(const NEURAL_ADDRESS *addr)
{
	neural_sockaddr *ctx = neural_address_get_vtdata(addr);
	return ((ctx->caps & NEURAL_ADDRESS_CAN_LISTEN) ? 1 : 0);
}

static const NEURAL_LISTENER_vtable *addr_create_listener(const NEURAL_ADDRESS *addr)
{
	return &list_vtable;
}

static const NEURAL_CONNECTION_vtable *addr_create_connection(const NEURAL_ADDRESS *addr)
{
	return &conn_vtable;
}

static int list_on_create(NEURAL_LISTENER *l)
{
	return 1;
}

static void list_on_destroy(NEURAL_LISTENER *l)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	neural_fd_close(&ctx->fd);
	ctx->caught = 0;
}

static int list_listen(NEURAL_LISTENER *l, const NEURAL_ADDRESS *addr)
{
	neural_sockaddr *ctx_addr = neural_address_get_vtdata(addr);
	list_ctx *ctx_listener = neural_listener_get_vtdata(l);
	ctx_listener->fd = -1;
	if(!neural_sock_create_socket(&ctx_listener->fd, ctx_addr) ||
			!neural_fd_make_non_blocking(ctx_listener->fd, 1) ||
			!neural_sock_listen(ctx_listener->fd, ctx_addr)) {
		neural_fd_close(&ctx_listener->fd);
		return 0;
	}
	ctx_listener->type = ctx_addr->type;
	return 1;
}

static const NEURAL_CONNECTION_vtable *list_pre_accept(NEURAL_LISTENER *l)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	if(ctx->caught) {
		return &conn_vtable;
	}
	return NULL;
}

static int list_finished(const NEURAL_LISTENER *l)
{
	return 0;
}

static int list_pre_selector_add(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel)
{
	switch(neural_selector_get_type(sel)) {
	case NEURAL_SELECTOR_TYPE_FDSELECT:
	case NEURAL_SELECTOR_TYPE_FDPOLL:
		return 1;
	case NEURAL_SELECTOR_TYPE_DYNAMIC:
		return neural_selector_dynamic_set(sel, NEURAL_SELECTOR_VT_DEFAULT());
	default:
		break;
	}
	return 0;
}

static void list_post_selector_del(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	ctx->caught = 0;
}

static void list_pre_select(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN tok)
{
	list_ctx *ctx = neural_listener_get_vtdata(l);
	if(!ctx->caught)
		neural_selector_fd_set(sel, tok, ctx->fd, SELECTOR_FLAG_READ);
}

static void list_post_select(NEURAL_LISTENER *l, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN tok)
{
	unsigned char flags;
	list_ctx *ctx = neural_listener_get_vtdata(l);
	neural_selector_fd_test(&flags, sel, tok, ctx->fd);
	if(flags & SELECTOR_FLAG_READ) {
		assert(!ctx->caught);
		ctx->caught = 1;
	}
}

static int list_set_fs_owner(NEURAL_LISTENER *l, const char *ownername,
				const char *groupname)
{
	neural_sockaddr sa;
	list_ctx *ctx = neural_listener_get_vtdata(l);
	if(ctx->type != neural_sockaddr_type_unix) return 0;
	if(!neural_sockaddr_get(&sa, ctx->fd)) return 0;
	return neural_sockaddr_chown(&sa, ownername, groupname);
}

static int list_set_fs_perms(NEURAL_LISTENER *l, const char *octal_string)
{
	neural_sockaddr sa;
	list_ctx *ctx = neural_listener_get_vtdata(l);
	if(ctx->type != neural_sockaddr_type_unix) return 0;
	if(!neural_sockaddr_get(&sa, ctx->fd)) return 0;
	return neural_sockaddr_chmod(&sa, octal_string);
}

static int conn_ctx_setup(conn_ctx *ctx_conn, int fd, int established,
				unsigned int buf_size)
{
	if(!NEURAL_BUFFER_set_size(ctx_conn->b_read, buf_size) ||
			!NEURAL_BUFFER_set_size(ctx_conn->b_send, buf_size))
		return 0;
	ctx_conn->fd = fd;
	ctx_conn->established = established;
	return 1;
}

static int conn_on_create(NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	if(!ctx->b_read) ctx->b_read = NEURAL_BUFFER_new();
	if(!ctx->b_send) ctx->b_send = NEURAL_BUFFER_new();
	if(!ctx->b_read || !ctx->b_send) return 0;
	ctx->fd = -1;
	return 1;
}

static void conn_on_destroy(NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	neural_fd_close(&ctx->fd);
	NEURAL_BUFFER_free(ctx->b_read);
	NEURAL_BUFFER_free(ctx->b_send);
}

static void conn_on_reset(NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	neural_fd_close(&ctx->fd);
	NEURAL_BUFFER_reset(ctx->b_read);
	NEURAL_BUFFER_reset(ctx->b_send);
	ctx->flags = 0;
	ctx->established = 0;
}

static int conn_connect(NEURAL_CONNECTION *conn, const NEURAL_ADDRESS *addr)
{
	int fd = -1, established;
	const neural_sockaddr *ctx_addr = neural_address_get_vtdata(addr);
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	if(!neural_sock_create_socket(&fd, ctx_addr) ||
			!neural_fd_make_non_blocking(fd, 1) ||
			!neural_sock_connect(fd, ctx_addr, &established) ||
			!neural_sock_set_nagle(fd, gb_use_nagle, ctx_addr->type) ||
			!conn_ctx_setup(ctx_conn, fd, established,
				NEURAL_ADDRESS_get_def_buffer_size(addr)))
		goto err;
	return 1;
err:
	neural_fd_close(&fd);
	return 0;
}

static int conn_accept(NEURAL_CONNECTION *conn, const NEURAL_LISTENER *l)
{
	int fd = -1;
	list_ctx *ctx_list = neural_listener_get_vtdata(l);
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	assert(ctx_list->caught);
	if(!neural_sock_accept(ctx_list->fd, &fd)) {
#if 0
		switch(errno) {
		case EAGAIN:
#if EAGAIN != EWOULDBLOCK
		case EWOULDBLOCK:
#endif
			ctx_list->caught = 0;
			break;
		default:
			break;
		}
#endif
		goto err;
	}
	ctx_list->caught = 0;
	if(!neural_fd_make_non_blocking(fd, 1) ||
			!neural_sock_set_nagle(fd, gb_use_nagle, ctx_list->type) ||
			!conn_ctx_setup(ctx_conn, fd, 1,
				neural_listener_get_def_buffer_size(l)))
		goto err;
	return 1;
err:
	neural_fd_close(&fd);
	return 0;
}

static int conn_set_size(NEURAL_CONNECTION *conn, unsigned int size)
{
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	if(!NEURAL_BUFFER_set_size(ctx_conn->b_read, size) ||
			!NEURAL_BUFFER_set_size(ctx_conn->b_send, size))
		return 0;
	return 1;
}

static NEURAL_BUFFER *conn_get_read(const NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	return ctx_conn->b_read;
}

static NEURAL_BUFFER *conn_get_send(const NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	return ctx_conn->b_send;
}

static int conn_is_established(const NEURAL_CONNECTION *conn)
{
	conn_ctx *ctx_conn = neural_connection_get_vtdata(conn);
	return ctx_conn->established;
}

static int conn_pre_selector_add(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel)
{
	switch(neural_selector_get_type(sel)) {
	case NEURAL_SELECTOR_TYPE_FDSELECT:
	case NEURAL_SELECTOR_TYPE_FDPOLL:
		return 1;
	case NEURAL_SELECTOR_TYPE_DYNAMIC:
		return neural_selector_dynamic_set(sel, NEURAL_SELECTOR_VT_DEFAULT());
	default:
		break;
	}
	return 0;
}

static void conn_post_selector_del(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	ctx->flags = 0;
}

static void conn_pre_select(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN token)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	neural_selector_fd_set(sel, token, ctx->fd,
		(ctx->established && NEURAL_BUFFER_notfull(ctx->b_read) ?
			SELECTOR_FLAG_READ : 0) |
		(!ctx->established || NEURAL_BUFFER_notempty(ctx->b_send) ?
			SELECTOR_FLAG_SEND : 0) |
		SELECTOR_FLAG_EXCEPT);
}

static void conn_post_select(NEURAL_CONNECTION *conn, NEURAL_SELECTOR *sel,
			NEURAL_SELECTOR_TOKEN token)
{
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	neural_selector_fd_test(&ctx->flags, sel, token, ctx->fd);
}

static int conn_do_io(NEURAL_CONNECTION *conn)
{
	int nb = 0;
	conn_ctx *ctx = neural_connection_get_vtdata(conn);
	if(ctx->flags & SELECTOR_FLAG_EXCEPT) return 0;
	if(!ctx->established) {
		if(!(ctx->flags & SELECTOR_FLAG_SEND))
			return 1;
		if(!neural_sock_is_connected(ctx->fd))
			return 0;
		ctx->established = 1;
		nb = 1;
	}
	if(ctx->flags & SELECTOR_FLAG_READ) {
		int io_ret = neural_fd_buffer_from_fd(ctx->b_read, ctx->fd, 0);
		if(io_ret <= 0)
			return 0;
	}
	if(ctx->flags & SELECTOR_FLAG_SEND) {
		int io_ret = neural_fd_buffer_to_fd(ctx->b_send, ctx->fd, 0);
		if((io_ret < 0) || (!io_ret && !nb))
			return 0;
	}
	ctx->flags = 0;
	return 1;
}

