/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include <libgen/post.h>

#include <assert.h>

extern NEURAL_ADDRESS_vtable builtin_sock_addr_vtable;

const NEURAL_ADDRESS_vtable *NEURAL_ADDRESS_vtable_builtins(void)
{
	return &builtin_sock_addr_vtable;
}

void NEURAL_ADDRESS_vtable_link(NEURAL_ADDRESS_vtable *vt)
{
	NEURAL_ADDRESS_vtable *i, *next;
	do {
		next = vt->next;
		i = &builtin_sock_addr_vtable;
conflict_loop:
		if(strcmp(i->unique_name, vt->unique_name) == 0)
			continue;
		if(i->next) {
			i = i->next;
			goto conflict_loop;
		}
		i->next = vt;
		vt->next = NULL;
	} while((vt = next) != NULL);
}

struct st_NEURAL_ADDRESS {
	const NEURAL_ADDRESS_vtable *vt;
	void *vt_data;
	size_t vt_data_size;
	const NEURAL_ADDRESS_vtable *reset;
	unsigned int def_buffer_size;
};

int neural_address_set_vtable(NEURAL_ADDRESS *a, const NEURAL_ADDRESS_vtable *vtable)
{
	if(a->vt) {
		if(a->vt->pre_close)
			a->vt->pre_close(a);
		a->vt->on_reset(a);
		a->reset = a->vt;
		a->vt = NULL;
	}
	if(a->reset && (a->reset != vtable)) {
		a->reset->on_destroy(a);
		a->reset = NULL;
		SYS_zero_n(unsigned char, a->vt_data, a->vt_data_size);
	}
	if(vtable->vtdata_size > a->vt_data_size) {
		assert(a->reset == NULL);
		if(a->vt_data)
			SYS_free(void, a->vt_data);
		a->vt_data = SYS_malloc(unsigned char, vtable->vtdata_size);
		if(!a->vt_data) {
			a->vt_data_size = 0;
			return 0;
		}
		a->vt_data_size = vtable->vtdata_size;
		SYS_zero_n(unsigned char, a->vt_data, vtable->vtdata_size);
	}
	if(vtable->on_create(a)) {
		a->vt = vtable;
		return 1;
	}
	return 0;
}

const NEURAL_ADDRESS_vtable *neural_address_get_vtable(const NEURAL_ADDRESS *addr)
{
	return addr->vt;
}

void *neural_address_get_vtdata(const NEURAL_ADDRESS *addr)
{
	return addr->vt_data;
}

const NEURAL_LISTENER_vtable *neural_address_get_listener(const NEURAL_ADDRESS *addr)
{
	if(addr->vt) return addr->vt->create_listener(addr);
	return NULL;
}

const NEURAL_CONNECTION_vtable *neural_address_get_connection(const NEURAL_ADDRESS *addr)
{
	if(addr->vt) return addr->vt->create_connection(addr);
	return NULL;
}

NEURAL_ADDRESS *NEURAL_ADDRESS_new(void)
{
	NEURAL_ADDRESS *a = SYS_malloc(NEURAL_ADDRESS, 1);
	if(a) {
		a->vt = NULL;
		a->vt_data = NULL;
		a->vt_data_size = 0;
		a->reset = NULL;
		a->def_buffer_size = 0;
	}
	return a;
}

void NEURAL_ADDRESS_free(NEURAL_ADDRESS *a)
{
	if(a->vt) {
		if(a->vt->pre_close)
			a->vt->pre_close(a);
		a->vt->on_destroy(a);
	} else if(a->reset)
		a->reset->on_destroy(a);
	if(a->vt_data) SYS_free(void, a->vt_data);
	SYS_free(NEURAL_ADDRESS, a);
}

void NEURAL_ADDRESS_reset(NEURAL_ADDRESS *a)
{
	if(a->vt) {
		if(a->vt->pre_close)
			a->vt->pre_close(a);
		a->vt->on_reset(a);
		a->reset = a->vt;
		a->vt = NULL;
	}
}

unsigned int NEURAL_ADDRESS_get_def_buffer_size(const NEURAL_ADDRESS *addr)
{
	return addr->def_buffer_size;
}

int NEURAL_ADDRESS_set_def_buffer_size(NEURAL_ADDRESS *addr,
			unsigned int def_buffer_size)
{
	if(!neural_check_buffer_size(def_buffer_size)) return 0;
	addr->def_buffer_size = def_buffer_size;
	return 1;
}

int NEURAL_ADDRESS_create(NEURAL_ADDRESS *addr, const char *addr_string,
			unsigned int def_buffer_size)
{
	unsigned int len;
	const NEURAL_ADDRESS_vtable *vtable = NEURAL_ADDRESS_vtable_builtins();
	if(addr->vt) return 0;
	if(!NEURAL_ADDRESS_set_def_buffer_size(addr, def_buffer_size))
		return 0;
	len = strlen(addr_string);
	if((len < 2) || (len > NEURAL_ADDRESS_MAX_STR_LEN))
		return 0;
	while(vtable) {
		const char **pre = vtable->prefixes;
		while(*pre) {
			unsigned int pre_len = strlen(*pre);
			if((pre_len <= len) && (strncmp(*pre, addr_string,
							pre_len) == 0))
				goto done;
			pre++;
		}
		vtable = vtable->next;
	}
done:
	if(!vtable)
		return 0;
	if(!neural_address_set_vtable(addr, vtable) || !vtable->parse(addr, addr_string)) {
		NEURAL_ADDRESS_reset(addr);
		return 0;
	}
	return 1;
}

int NEURAL_ADDRESS_can_connect(const NEURAL_ADDRESS *addr)
{
	if(addr->vt) return addr->vt->can_connect(addr);
	return 0;
}

int NEURAL_ADDRESS_can_listen(const NEURAL_ADDRESS *addr)
{
	if(addr->vt) return addr->vt->can_listen(addr);
	return 0;
}

