/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_PRIVATE_CTRL_FD_H
#define HEADER_PRIVATE_CTRL_FD_H

#ifndef HEADER_LIBGEN_PRE_H
	#error "Must include libgen/pre.h prior to ctrl_fd.h"
#endif

#define SELECTOR_FLAG_READ	0x01
#define SELECTOR_FLAG_SEND	0x02
#define SELECTOR_FLAG_EXCEPT	0x04

typedef enum {
	NEURAL_FD_CTRL_FDSET = NEURAL_SELECTOR_CTRL_FD,
	NEURAL_FD_CTRL_FDTEST
} NEURAL_FD_CTRL_TYPE;

typedef struct st_neural_fd_fdset {
	NEURAL_SELECTOR_TOKEN token;
	int fd;
	unsigned char flags;
} NEURAL_FD_FDSET;

typedef struct st_neural_fd_fdtest {
	unsigned char flags;
	NEURAL_SELECTOR_TOKEN token;
	int fd;
} NEURAL_FD_FDTEST;

#define neural_selector_fd_set(_sel, _tok, _fd, _flags) \
	do { \
		NEURAL_FD_FDSET args; \
		args.token = (_tok); \
		args.fd = (_fd); \
		args.flags = (_flags); \
		neural_selector_ctrl((_sel), NEURAL_FD_CTRL_FDSET, &args); \
	} while(0)
#define neural_selector_fd_test(_flags, _sel, _tok, _fd) \
	do { \
		NEURAL_FD_FDTEST args; \
		args.token = (_tok); \
		args.fd = (_fd); \
		neural_selector_ctrl((_sel), NEURAL_FD_CTRL_FDTEST, &args); \
		*(_flags) = args.flags; \
	} while(0)

#endif /* !defined(HEADER_PRIVATE_CTRL_FD_H) */
