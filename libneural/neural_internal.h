/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#ifndef HEADER_PRIVATE_NEURAL_INTERNAL_H
#define HEADER_PRIVATE_NEURAL_INTERNAL_H

#ifndef HEADER_LIBGEN_PRE_H
	#error "Must include libgen/pre.h prior to libneural/neural.h"
#endif

#include <libneural/neural_build.h>

#ifndef socklen_t
#define socklen_t int
#endif

#define NEURAL_ADDRESS_CAN_LISTEN	(unsigned char)0x01
#define NEURAL_ADDRESS_CAN_CONNECT	(unsigned char)0x02

#define NEURAL_LISTENER_BACKLOG     511
#define NEURAL_ADDRESS_MAX_STR_LEN  255

typedef enum {
	neural_sockaddr_type_ip,
	neural_sockaddr_type_unix
} neural_sockaddr_type;

typedef struct st_neural_sockaddr {
	union {
		struct sockaddr_in val_in;
#ifndef WIN32
		struct sockaddr_un val_un;
#endif
	} val;
	neural_sockaddr_type type;
	unsigned char caps;
} neural_sockaddr;

int neural_fd_make_non_blocking(int fd, int non_blocking);
int neural_fd_buffer_to_fd(NEURAL_BUFFER *buf, int fd, unsigned int max_send);
int neural_fd_buffer_from_fd(NEURAL_BUFFER *buf, int fd, unsigned int max_read);
void neural_fd_close(int *fd);

int neural_sock_set_nagle(int fd, int use_nagle, neural_sockaddr_type type);
int neural_sock_sockaddr_from_ipv4(neural_sockaddr *addr, const char *start_ptr);
int neural_sock_sockaddr_from_unix(neural_sockaddr *addr, const char *start_ptr);
int neural_sock_create_socket(int *fd, const neural_sockaddr *addr);
int neural_sock_create_unix_pair(int sv[2]);
int neural_sock_connect(int fd, const neural_sockaddr *addr, int *established);
int neural_sock_listen(int fd, const neural_sockaddr *addr);
int neural_sock_accept(int listen_fd, int *conn);
int neural_sock_is_connected(int fd);
int neural_sockaddr_get(neural_sockaddr *addr, int fd);
int neural_sockaddr_chown(const neural_sockaddr *addr, const char *username,
			const char *groupname);
int neural_sockaddr_chmod(const neural_sockaddr *addr, const char *octal_string);

#ifdef HAVE_SELECT
#ifdef HAVE_POLL
#ifdef PREFER_POLL
#define NEURAL_SELECTOR_VT_DEFAULT		sel_fdpoll
#else
#define NEURAL_SELECTOR_VT_DEFAULT		sel_fdselect
#endif
#else
#define NEURAL_SELECTOR_VT_DEFAULT		sel_fdselect
#endif
#else
#ifdef HAVE_POLL
#define NEURAL_SELECTOR_VT_DEFAULT		sel_fdpoll
#else
#error "Neither HAVE_SELECT nor HAVE_POLL are defined"
#endif
#endif

NEURAL_SELECTOR_TOKEN neural_selector_add_listener(NEURAL_SELECTOR *, NEURAL_LISTENER *);
NEURAL_SELECTOR_TOKEN neural_selector_add_connection(NEURAL_SELECTOR *, NEURAL_CONNECTION *);
void neural_selector_del_listener(NEURAL_SELECTOR *, NEURAL_LISTENER *, NEURAL_SELECTOR_TOKEN);
void neural_selector_del_connection(NEURAL_SELECTOR *, NEURAL_CONNECTION *, NEURAL_SELECTOR_TOKEN);

unsigned int neural_listener_get_def_buffer_size(const NEURAL_LISTENER *l);
int neural_listener_set_def_buffer_size(NEURAL_LISTENER *l, unsigned int def_buffer_size);

#define NEURAL_BUFFER_MAX_SIZE  32768
#define neural_check_buffer_size(sz) (((sz) > NEURAL_BUFFER_MAX_SIZE) ? 0 : 1)

#endif /* !defined(HEADER_PRIVATE_NEURAL_INTERNAL_H) */
