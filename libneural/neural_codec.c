/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include <libgen/post.h>

int NEURAL_decode_uint32(const unsigned char **bin, unsigned int *bin_len,
		unsigned long *val)
{
	if(*bin_len < 4)
		return 0;
	*val =  (unsigned long)(*((*bin)++)) << 24;
	*val += (unsigned long)(*((*bin)++)) << 16;
	*val += (unsigned long)(*((*bin)++)) << 8;
	*val += (unsigned long)(*((*bin)++));
	*bin_len -= 4;
	return 1;
}

int NEURAL_decode_uint16(const unsigned char **bin, unsigned int *bin_len,
		unsigned int *val)
{
	if(*bin_len < 2)
		return 0;
	*val = (unsigned long)*((*bin)++) << 8;
	*val += (unsigned long)*((*bin)++);
	*bin_len -= 2;
	return 1;
}

int NEURAL_decode_char(const unsigned char **bin, unsigned int *bin_len,
		unsigned char *c)
{
	if(*bin_len < 1)
		return 0;
	*c = *((*bin)++);
	*bin_len -= 1;
	return 1;
}

int NEURAL_decode_bin(const unsigned char **bin, unsigned int *bin_len,
		unsigned char *val, unsigned int val_len)
{
	if(*bin_len < val_len)
		return 0;
	if(val_len == 0)
		return 1;
	SYS_memcpy_n(unsigned char, val, *bin, val_len);
	*bin += val_len;
	*bin_len -= val_len;
	return 1;
}

int NEURAL_encode_uint32(unsigned char **bin, unsigned int *cnt,
		const unsigned long val)
{
	if(*cnt < 4) {
#if SYS_DEBUG_LEVEL > 3
		if(SYS_stderr) SYS_fprintf(SYS_stderr, "encode_uint32: overflow\n");
#endif
		return 0;
	}
	*((*bin)++) = (unsigned char)((val >> 24) & 0x0FF);
	*((*bin)++) = (unsigned char)((val >> 16) & 0x0FF);
	*((*bin)++) = (unsigned char)((val >> 8) & 0x0FF);
	*((*bin)++) = (unsigned char)(val & 0x0FF);
	*cnt -= 4;
	return 1;
}

int NEURAL_encode_uint16(unsigned char **bin, unsigned int *cnt,
		const unsigned int val)
{
	if(*cnt < 2) {
#if SYS_DEBUG_LEVEL > 3
		if(SYS_stderr) SYS_fprintf(SYS_stderr, "encode_uint16: overflow\n");
#endif
		return 0;
	}
	*((*bin)++) = (unsigned char)((val >> 8) & 0x0FF);
	*((*bin)++) = (unsigned char)(val & 0x0FF);
	*cnt -= 2;
	return 1;
}

int NEURAL_encode_char(unsigned char **bin, unsigned int *cnt,
		const unsigned char c)
{
	if(*cnt < 1) {
#if SYS_DEBUG_LEVEL > 3
		if(SYS_stderr) SYS_fprintf(SYS_stderr, "encode_char: overflow\n");
#endif
		return 0;
	}
	*((*bin)++) = c;
	*cnt -= 1;
	return 1;
}

int NEURAL_encode_bin(unsigned char **bin, unsigned int *cnt,
		const unsigned char *data, const unsigned int len)
{
	if(*cnt < len) {
#if SYS_DEBUG_LEVEL > 3
		if(SYS_stderr) SYS_fprintf(SYS_stderr, "encode_bin: overflow\n");
#endif
		return 0;
	}
	SYS_memcpy_n(unsigned char, *bin, data, len);
	*bin += len;
	*cnt -= len;
	return 1;
}

