/* NeuroCache - Neuro-based Session Caching (NSC)
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 *
 * This file is part of neurocache program written by Hilman P. Alisabana
 * <alisabana@hyang.org> and licensed under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#define SYS_GENERATING_LIB

#include <libgen/pre.h>
#include <libneural/neural.h>
#include "neural_internal.h"
#include <libgen/post.h>

#include <errno.h>

static int int_always_one = 1;
static int sol_tcp = -1;
static int int_sockaddr_size(const neural_sockaddr *addr)
{
	switch(addr->type) {
	case neural_sockaddr_type_ip:
		return sizeof(struct sockaddr_in);
#ifndef WIN32
	case neural_sockaddr_type_unix:
		return sizeof(struct sockaddr_un);
#endif
	default:
		break;
	}
	abort();
}

static int int_sock_set_reuse(int fd, const neural_sockaddr *addr)
{
	int reuseVal = 1;
	if(addr->type != neural_sockaddr_type_ip)
		return 1;
	if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
			(char *)(&reuseVal), sizeof(reuseVal)) != 0)
		return 0;
	return 1;
}

static int int_sock_bind(int fd, const neural_sockaddr *addr)
{
	socklen_t addr_size = int_sockaddr_size(addr);
	neural_sockaddr tmp;

	if(addr->type == neural_sockaddr_type_unix)
		unlink(addr->val.val_un.sun_path);
	SYS_memcpy(neural_sockaddr, &tmp, addr);
	if(bind(fd, (struct sockaddr *)&tmp, addr_size) != 0)
		return 0;
	return 1;
}

int neural_sock_set_nagle(int fd, int use_nagle, neural_sockaddr_type type)
{
#ifndef WIN32
	if(use_nagle || (type != neural_sockaddr_type_ip))
		return 1;

	if(sol_tcp == -1) {
		struct protoent *p = getprotobyname("tcp");
		if(!p) {
#if SYS_DEBUG_LEVEL > 1
			SYS_fprintf(SYS_stderr, "Error, couldn't obtain SOL_TCP\n");
#endif
			return 0;
		}
		sol_tcp = p->p_proto;
	}

	if(setsockopt(fd, sol_tcp, TCP_NODELAY, &int_always_one,
			sizeof(int_always_one)) != 0) {
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Error, couldn't disable Nagle algorithm\n");
#endif
		return 0;
	}
#endif
	return 1;
}

int neural_sock_sockaddr_from_ipv4(neural_sockaddr *addr, const char *start_ptr)
{
	char *tmp_ptr;
	char *fini_ptr;
	struct hostent *ip_lookup;
	unsigned long in_ip_piece;
	unsigned char in_ip[4];
	int no_ip = 0;

	addr->caps = 0;
	if(strlen(start_ptr) < 1) return 0;
	if(((fini_ptr = strstr(start_ptr, ":")) == NULL) ||
			(start_ptr == fini_ptr)) {
		no_ip = 1;
		if(fini_ptr)
			start_ptr++;
		goto ipv4_port;
	}
	tmp_ptr = SYS_malloc(char, (int)(fini_ptr - start_ptr) + 1);
	if(!tmp_ptr)
		return 0;
	SYS_memcpy_n(char, tmp_ptr, start_ptr,
		(int)(fini_ptr - start_ptr));
	tmp_ptr[(int)(fini_ptr - start_ptr)] = '\0';
	ip_lookup = gethostbyname(tmp_ptr);
	SYS_free(char, tmp_ptr);
	if(!ip_lookup)
		return 0;
	SYS_memcpy_n(char, (char *)in_ip, ip_lookup->h_addr_list[0], 4);
	start_ptr = fini_ptr + 1;
	addr->caps |= NEURAL_ADDRESS_CAN_CONNECT;

ipv4_port:
	if(strlen(start_ptr) < 1)
		return 0;
	in_ip_piece = strtoul(start_ptr, &fini_ptr, 10);
	if((in_ip_piece > 65535) || (*fini_ptr != '\0'))
		return 0;
	addr->val.val_in.sin_family = AF_INET;
	if(no_ip)
		addr->val.val_in.sin_addr.s_addr = INADDR_ANY;
	else
		SYS_memcpy_n(unsigned char,
			(unsigned char *)&addr->val.val_in.sin_addr.s_addr, in_ip, 4);
	addr->val.val_in.sin_port = htons((unsigned short)in_ip_piece);
	addr->caps |= NEURAL_ADDRESS_CAN_LISTEN;
	addr->type = neural_sockaddr_type_ip;
	return 1;
}

#ifndef WIN32
int neural_sock_sockaddr_from_unix(neural_sockaddr *addr, const char *start_ptr)
{
	struct sockaddr_un un_addr;

	un_addr.sun_family = AF_UNIX;
	SYS_strncpy(un_addr.sun_path, start_ptr, UNIX_PATH_MAX);
	SYS_zero(neural_sockaddr, addr);
	SYS_memcpy(struct sockaddr_un, &addr->val.val_un, &un_addr);
	addr->type = neural_sockaddr_type_unix;
	addr->caps = NEURAL_ADDRESS_CAN_LISTEN | NEURAL_ADDRESS_CAN_CONNECT;
	return 1;
}
#endif

int neural_sock_create_socket(int *fd, const neural_sockaddr *addr)
{
	int tmp_fd = -1;
	switch(addr->type) {
	case neural_sockaddr_type_ip:
		tmp_fd = socket(PF_INET, SOCK_STREAM, 0);
		break;
#ifndef WIN32
	case neural_sockaddr_type_unix:
		tmp_fd = socket(PF_UNIX, SOCK_STREAM, 0);
		break;
#endif
	default:
		abort();
	}
	if(tmp_fd < 0) {
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Error, can't create socket\n\n");
#endif
		return 0;
	}
	*fd = tmp_fd;
	return 1;
}

#ifndef WIN32
int neural_sock_create_unix_pair(int sv[2])
{
	if(socketpair(PF_UNIX, SOCK_STREAM, 0, sv) != 0) {
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Error, can't create socketpair\n\n");
#endif
		return 0;
	}
	return 1;
}
#endif

int neural_sock_connect(int fd, const neural_sockaddr *addr,
			int *established)
{
	socklen_t addr_size = int_sockaddr_size(addr);
	neural_sockaddr tmp;

	SYS_memcpy(neural_sockaddr, &tmp, addr);
	if(connect(fd, (struct sockaddr *)&tmp, addr_size) != 0) {
#ifdef WIN32
		if(WSAGetLastError() != WSAEWOULDBLOCK)
#else
		if(errno != EINPROGRESS)
#endif
		{
#if SYS_DEBUG_LEVEL > 1
			SYS_fprintf(SYS_stderr, "Error, couldn't connect\n\n");
#endif
			return 0;
		}
		*established = 0;
	} else
		*established = 1;
	return 1;
}

int neural_sock_listen(int fd, const neural_sockaddr *addr)
{
	if(!int_sock_set_reuse(fd, addr) || !int_sock_bind(fd, addr))
		return 0;
	if(listen(fd, NEURAL_LISTENER_BACKLOG) != 0)
		return 0;
	return 1;
}

int neural_sock_accept(int listen_fd, int *conn)
{
	if((*conn = accept(listen_fd, NULL, NULL)) == -1) {
#if SYS_DEBUG_LEVEL > 1
		SYS_fprintf(SYS_stderr, "Error, accept failed\n\n");
#endif
		return 0;
	}
	return 1;
}

int neural_sock_is_connected(int fd)
{
	int t;
	socklen_t t_len = sizeof(t);
	if((getsockopt(fd, SOL_SOCKET, SO_ERROR, &t,
			(unsigned int *)&t_len) != 0) || (t != 0))
		return 0;
	return 1;
}

#if !defined(WIN32) && defined(HAVE_GETSOCKNAME) && defined(HAVE_STRTOUL) && \
	defined(HAVE_CHOWN) && defined(HAVE_CHMOD) && defined(HAVE_GETPWNAM)
#define NEURAL_SOCKADDR_UNIX
#endif

int neural_sockaddr_get(neural_sockaddr *addr, int fd)
{
#ifdef NEURAL_SOCKADDR_UNIX
	socklen_t pathlen = sizeof(addr->val.val_un);
	if(getsockname(fd, (struct sockaddr *)&addr->val.val_un,
				(void*)&pathlen) != 0)
		return 0;
	addr->type = neural_sockaddr_type_unix;
	addr->caps = 0;
	return 1;
#else
	return 0;
#endif
}

int neural_sockaddr_chown(const neural_sockaddr *addr, const char *username,
			const char *groupname)
{
#ifdef NEURAL_SOCKADDR_UNIX
	struct passwd *p = (username ? getpwnam(username) : NULL);
	uid_t uid = (p ? p->pw_uid : (uid_t)-1);
	gid_t gid = (p ? p->pw_gid : (uid_t)-1);
#if defined(HAVE_GETGRNAM)
	struct group *g = (groupname ? getgrnam(groupname) : NULL);
	if(groupname && !g) return 0;
	if(g) gid = g->gr_gid;
#endif
	if(username && !p) return 0;
	if(addr->type != neural_sockaddr_type_unix) return 0;
	if(chown(addr->val.val_un.sun_path, uid, gid) != 0) return 0;
	return 1;
#else
	return 0;
#endif
}
int neural_sockaddr_chmod(const neural_sockaddr *addr, const char *octal_string)
{
#ifdef NEURAL_SOCKADDR_UNIX
	unsigned long n;
	char *endptr;
	if(addr->type != neural_sockaddr_type_unix) return 0;
	n = strtol(octal_string, &endptr, 8);
	if ((endptr == octal_string) || (*endptr != '\0') || (n == ULONG_MAX))
		return 0;
	if (chmod(addr->val.val_un.sun_path, n) != 0) return 0;
	return 1;
#else
	return 0;
#endif
}
